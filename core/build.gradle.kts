/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

plugins {
    application
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":lib"))
    implementation(project(":nextcloud"))
    implementation(project(":openproject"))
    implementation(project(":openshift"))
    implementation(project(":authorization"))
    implementation(project(":multiservice"))
    implementation(project(":iog"))
    implementation(project(":keycloak-actions"))

    // Database ORM Framework
    implementation("org.jetbrains.exposed", "exposed-core", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-dao", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-jdbc", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-java-time", exposedVersion)

    // Database driver implementation
    implementation("org.postgresql", "postgresql", postgresVersion)

    // Rest framework
    implementation("io.javalin", "javalin", javalinVersion)
    implementation("io.javalin", "javalin-openapi", javalinVersion)

    // OpenApi Document Generator
    implementation("io.swagger.core.v3", "swagger-core", "2.0.9")
    implementation("org.webjars", "swagger-ui", "3.24.3")

    // Redis driver
    implementation("redis.clients", "jedis", "3.2.0")

    // Im memory database
    testImplementation("com.h2database", "h2", "1.4.200")

    // Mock webserver
    testImplementation("com.squareup.okhttp3", "mockwebserver", "4.7.2")
}

application {
    applicationName = "feather"
    mainClass.set("dev.maximilian.feather.MainKt")
}

distributions {
    main {
        distributionBaseName.set("feather")
    }
}

tasks.processResources {
    filter(
        org.apache.tools.ant.filters.ReplaceTokens::class,
        "tokens" to mapOf("version" to project.version)
    )
}

kotlin {
    explicitApi()
}
