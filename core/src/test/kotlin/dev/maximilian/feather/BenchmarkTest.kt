/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather

import dev.maximilian.feather.util.Benchmark
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class BenchmarkTest {
    @Test
    fun `count is correct`() {
        val benchmark = Benchmark()
        executeSimpleTest(benchmark, 10)
        executeSimpleTest(benchmark, 5)
        executeSimpleTest(benchmark, 5)
        assertEquals(3, benchmark.timeEntries["lala"]!!.count)
    }

    @Test
    fun `sum is correct`() {
        val benchmark = Benchmark()
        executeSimpleTest(benchmark, 50)
        executeSimpleTest(benchmark, 100)
        executeSimpleTest(benchmark, 20)
        val sumTime = benchmark.timeEntries["lala"]!!.sumTimeMs
        assertTrue(sumTime in 170..200)
    }

    @Test
    fun `minTime is correct`() {
        val benchmark = Benchmark()
        executeSimpleTest(benchmark, 10)
        executeSimpleTest(benchmark, 500)
        executeSimpleTest(benchmark, 300)
        val minTime = benchmark.timeEntries["lala"]!!.minTimeMs
        assertTrue(minTime in 10..80)
    }

    @Test
    fun `maxTime is correct`() {
        val benchmark = Benchmark()
        executeSimpleTest(benchmark, 10)
        executeSimpleTest(benchmark, 100)
        executeSimpleTest(benchmark, 50)
        val minTime = benchmark.timeEntries["lala"]!!.maxTimeMs
        assertTrue(minTime in 100..130)
    }

    @Test
    fun `lastDuration is correct`() {
        val benchmark = Benchmark()
        executeSimpleTest(benchmark, 100)
        executeSimpleTest(benchmark, 500)
        executeSimpleTest(benchmark, 300)
        val minTime = benchmark.timeEntries["lala"]!!.lastDuration
        assertTrue(minTime in 300..330)
    }

    @Test
    fun `reentrant use calculates correct lastTime`() {
        val benchmark = Benchmark()
        val uuid1 = benchmark.start()
        val uuid2 = benchmark.start()
        Thread.sleep(100)
        benchmark.stop(uuid1, "lala")
        benchmark.stop(uuid2, "lala")
        val minTime = benchmark.timeEntries["lala"]!!.lastDuration
        assertTrue(minTime in 100..130)
    }

    @Test
    fun `reentrant counts correctly`() {
        val benchmark = Benchmark()
        val uuid1 = benchmark.start()
        val uuid2 = benchmark.start()
        Thread.sleep(10)
        benchmark.stop(uuid1, "lala")
        benchmark.stop(uuid2, "lala")
        assertEquals(2, benchmark.timeEntries["lala"]!!.count)
    }

    @Test
    fun `execute log report works`() {
        val benchmark = Benchmark()
        executeSimpleTest(benchmark, 10)
        benchmark.logReport()
    }

    private fun executeSimpleTest(benchmark: Benchmark, duration: Long) {
        val uuid = benchmark.start()
        Thread.sleep(duration)
        benchmark.stop(uuid, "lala")
    }
}
