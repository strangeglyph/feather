/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.settings.OPRoleNames
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.iog.testframework.TestCredentialProvider
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.openproject.OpenProjectGroup
import kong.unirest.GenericType
import kong.unirest.HttpRequest
import kong.unirest.HttpResponse
import kong.unirest.RequestBodyEntity
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CreateTrialGroupsTest {

    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val apiTestUtilities = ApiTestUtilities()

    init {
        apiTestUtilities.startIogPlugin(true, backgroundJobManager)
        apiTestUtilities.createStandardGroups()
        runBlocking {
            apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        }
    }

    private val credentials = TestCredentialProvider.credentialProvider

    @Test
    fun `Create trial groups with repair by standard user returns FORBIDDEN (403)`() {
        apiTestUtilities.loginWithStandardUser()
        val response = createTrialByApi(true).asEmpty()
        apiTestUtilities.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `Create trial groups without repair by standard user returns FORBIDDEN (403)`() {
        apiTestUtilities.loginWithStandardUser()
        val response = createTrialByApi(false).asEmpty()
        apiTestUtilities.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `Create trial groups creates correct trial-groups, assignes OP roles and adds group to NC`() {
        apiTestUtilities.createRGTest()
        apiTestUtilities.removeTrialGroups()
        // test only
        val response = createTrialByApi(false).asObject<BackgroundJobWithStrings>()
        assertEquals(HttpStatus.CREATED_201, response.status)
        val job = response.body
        assertNotNull(job)
        assertEquals("ADMIN", job.permission)
        assertEquals("started", job.status)

        // wait until job is completed (otherwise the parallel execution breaks the test)

        do {
            val jobWorking = backgroundJobManager.getJobStatus(job.id)
            checkNotNull(jobWorking)
        } while (jobWorking?.completed == null)

        // repair now
        val response2 = createTrialByApi(true).asObject<BackgroundJobWithStrings>()
        assertEquals(HttpStatus.CREATED_201, response2.status)
        val job2 = response2.body
        assertNotNull(job2)
        // assertEquals(Permission.ADMIN, job.permission)
        assertEquals("ADMIN", job2.permission)
        assertEquals("started", job2.status)

        // wait until job is completed (otherwise the parallel execution breaks the test)

        do {
            val jobWorking2 = backgroundJobManager.getJobStatus(job2.id)
            checkNotNull(jobWorking2)
        } while (jobWorking2?.completed == null)

        assertEquals(HttpStatus.CREATED_201, response2.status)

        val t = credentials.getGroupByName("rg-test-trial")
        val rg1Links: List<Pair<String, String>>
        val gsLinks: List<Pair<String, String>>
        val apLinks: List<Pair<String, String>>
        runBlocking {
            val op = apiTestUtilities.scenario!!.openProject!!
            val memberships = op.getMemberships(op.getRoles())
            rg1Links = memberships.filter { it.project.value?.name == "RG Test" && it.principal.value is OpenProjectGroup }
                .map { Pair((it.principal.value as OpenProjectGroup).name, it.roles.map { role -> role.name }.joinToString()) }
            gsLinks = memberships.filter { it.project.value?.name == "Geschäftsstelle" && it.principal.value is OpenProjectGroup }
                .map { Pair((it.principal.value as OpenProjectGroup).name, it.roles.map { role -> role.name }.joinToString()) }
            apLinks = memberships.filter { it.project.value?.name == "AP SprecherInnen" && it.principal.value is OpenProjectGroup }
                .map { Pair((it.principal.value as OpenProjectGroup).name, it.roles.map { role -> role.name }.joinToString()) }
        }

        assertNotNull(t, "Die Gruppe rg-test-trial wurde nicht in LDAP erstellt.")
        runBlocking {
            assertTrue(
                apiTestUtilities.scenario!!.openProject!!.getGroups()
                    .any { it.name == "rg-test-trial" },
                "Es wurde die rg-test-trial-Gruppe nicht in OP angelegt."
            )
            assertTrue(
                apiTestUtilities.scenario!!.openProject!!.getGroups()
                    .any { it.name == OPNameConfig(mutableMapOf()).trialMemberGroupName },
                "Es wurde die TrialMembers nicht in OP angelegt."
            )
        }
        val onc = OPNameConfig(mutableMapOf())
        assertEquals(
            listOf(
                Pair(onc.interestedPeopleGroupName, OPRoleNames.INTERESTED),
                Pair(onc.iogMembersGroupName, OPRoleNames.READER),
                Pair("rg-test-admin", OPRoleNames.PROJECT_ADMIN),
                Pair("rg-test-interested", OPRoleNames.READER),
                Pair("rg-test-member", OPRoleNames.MEMBER),
                Pair("rg-test-trial", OPRoleNames.MEMBER),
            ),
            rg1Links.sortedBy { it.first }
        )
        assertEquals(
            listOf(
                Pair(onc.interestedPeopleGroupName, OPRoleNames.INTERESTED),
                Pair(onc.iogMembersGroupName, OPRoleNames.INTERESTED),
                Pair("geschaeftsstelle", OPRoleNames.MEMBER),
                Pair("geschaeftsstelle-admin", OPRoleNames.PROJECT_ADMIN),
            ),
            gsLinks.sortedBy { it.first }
        )
        assertEquals(
            listOf(
                Pair(onc.interestedPeopleGroupName, OPRoleNames.INTERESTED),
                Pair(onc.iogMembersGroupName, OPRoleNames.READER),
                Pair("ap-sprecherinnen", OPRoleNames.MEMBER),
                Pair("ap-sprecherinnen-admin", OPRoleNames.PROJECT_ADMIN)
            ),
            apLinks.sortedBy { it.first }
        )

        val ncGroups = apiTestUtilities.scenario!!.ncUtilities.nextcloud.findGroupFolder(NextcloudFolders.GroupShare)?.groups?.mapNotNull { it.key } ?: emptyList()

        assertTrue(
            ncGroups.contains("rg-test-trial"),
            "NC Groupfolder enthält rg-test-trial nicht."
        )

        apiTestUtilities.removeRGTest()
    }

    private fun createTrialByApi(autorepair: Boolean): RequestBodyEntity =
        apiTestUtilities
            .restConnection
            .post("${apiTestUtilities.scenario!!.basePath}/bindings/iog/chapter/trials")
            .body(autorepair)

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> =
        this.asObject(object : GenericType<B>() {})
}
