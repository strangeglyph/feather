/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

/*import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.github.sardine.SardineFactory
import dev.maximilian.feather.Group
import dev.maximilian.feather.getEnv
import dev.maximilian.feather.multiservice.InputInternalPublicFolder
import dev.maximilian.feather.multiservice.MultiServiceIogPluginConstants
import dev.maximilian.feather.nextcloud.NextcloudFactory
import dev.maximilian.feather.nextcloud.groupfolders.IGroupFoldersAPI
import dev.maximilian.feather.nextcloud.ocs.IGroupAPI
import dev.maximilian.feather.nextcloud.ocs.MockShareOCS
import dev.maximilian.feather.nextcloud.webdav.MockWebdavAPI
import dev.maximilian.feather.nextcloud.webdavAPI.WebdavAPI
import dev.maximilian.feather.nextcloud.webdavAPI.webdavIntern.IWebdav
import kong.unirest.Config
import kong.unirest.UnirestInstance
import kong.unirest.jackson.JacksonObjectMapper
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import java.net.URLEncoder

class InputInternalPublicFolderTest {
    companion object {
        private val mockNextcloud = MockShareOCS()

        val baseUrl = getEnv("NEXTCLOUD_BASE_URL", "http://127.0.0.1:8082")
        val user = getEnv("NEXTCLOUD_USER", "ncadmin")
        val password = getEnv("NEXTCLOUD_PWD", "ncadminsecret")
        val rest = UnirestInstance(
            Config().setObjectMapper(
                JacksonObjectMapper(
                    jacksonObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
                )
            )
                .addDefaultHeader("OCS-APIRequest", "true")
                .addDefaultHeader("Accept", "application/json")
                .setDefaultBasicAuth(user, password)
        )

        private val webdavURL = getEnv("NEXTCLOUD_WEBDAV", baseUrl + "/remote.php/dav/files/" + user + "/")

        private val mockWebdavAPI = MockWebdavAPI(webdavURL)
        val innerWebdavAPI: IWebdav = if (baseUrl == "http://127.0.0.1:7000")
            mockWebdavAPI
        else
            NextcloudFactory(mutableMapOf<String, String>())
                .createWebdav(user, password)
        val nc = Nextcloud(webdavURL, innerWebdavAPI, rest, baseUrl)
        // private val shareAPI = ShareAPI(baseUrl, rest)

        @BeforeAll
        @JvmStatic
        fun setup() {
            mockNextcloud.start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            mockNextcloud.stop()
        }
    }
    @Test
    fun `Test createSubFolderExist`() {
        loadMockScenario()

        val webdavAPI: WebdavAPI = nc.getWebdavAPI()

        if (baseUrl != "http://127.0.0.1:7000") {
            val groupAPI: IGroupAPI = nc.getGroupAPI()

            val ok = groupAPI.addGroup("testgroup")
            checkNotNull(ok)
            check(ok)

            val ok2 = groupAPI.addGroup(MultiServiceIogPluginConstants.INTERESTED_PEOPLE_LDAP_GROUP_NAME)
            checkNotNull(ok2)
            check(ok2)
        }

        val folderName = "Testprojekt mit Leerzeichen"

        if (!webdavAPI.listFiles("").contains("IOG"))
            webdavAPI.createFolder("IOG")

        if (!webdavAPI.listFiles("IOG").contains("Projekte"))
            webdavAPI.createFolder("IOG/Projekte")

        webdavAPI.listFiles("IOG/Projekte").filter {
            it != "Projekte"
        }.forEach {
            webdavAPI.delete("IOG/Projekte/$it")
        }
        val q = webdavAPI.listFiles("IOG/Projekte")
        assert(q.size == 1)

        webdavAPI.createFolder("IOG/Projekte/$folderName")
        val baseFolders = webdavAPI.listFiles("/IOG/Projekte")
        check(baseFolders.count() == 2)
        check(baseFolders.contains(folderName))

        val folderCreator = InputInternalPublicFolder(nc, "IOG/Projekte/" + folderName, folderName, null)
        folderCreator.createSubfolders()

        val innerBaseFolders = innerWebdavAPI.list(webdavURL + "IOG/Projekte", false)
        check(innerBaseFolders.count() == 2)
        check(innerBaseFolders[1].Name == folderName)
        check(innerBaseFolders[1].Directory)

        val subFolderEntries = webdavAPI.listFiles("IOG/Projekte/$folderName")

        check(subFolderEntries.count() == 5)
        check(subFolderEntries.contains("Eingang"))
        check(subFolderEntries.contains("Gruppenintern"))
        check(subFolderEntries.contains("Öffentlich"))
        check(subFolderEntries.contains("Readme.md"))

        val url = webdavURL + "IOG/Projekte/" + URLEncoder.encode(folderName, "utf-8").replace("+", "%20")
        val innerSubFolderEntries = innerWebdavAPI.list(url, false)
        check(innerSubFolderEntries.count() == 5)
        check(innerSubFolderEntries.first { it.Name == "Eingang" }.Directory)
        check(!innerSubFolderEntries.first { it.Name == "Readme.md" }.Directory)

        webdavAPI.delete("IOG/Projekte/$folderName")

        val folderName2 = "Testprojekt (Sonderzeichen)"

        if (!webdavAPI.listFiles("IOG").contains("PR+FR"))
            webdavAPI.createFolder("IOG/PR+FR")

        webdavAPI.listFiles("IOG/PR+FR").filter {
            it != "PR+FR"
        }.forEach {
            webdavAPI.delete("IOG/PR+FR/$it")
        }
        val q2 = webdavAPI.listFiles("IOG/PR+FR")
        assert(q2.size == 1)

        webdavAPI.createFolder("IOG/PR+FR/$folderName2")
        val baseFolders2 = webdavAPI.listFiles("/IOG/PR+FR")
        check(baseFolders2.contains(folderName2))

        val folderCreator2 = InputInternalPublicFolder(nc, "IOG/PR+FR/" + folderName2, folderName2, null)
        folderCreator2.createSubfolders()

        val subFolderEntries2 = webdavAPI.listFiles("IOG/PR+FR/$folderName2")

        check(subFolderEntries2.count() == 5)
        check(subFolderEntries2.contains("Eingang"))
        check(subFolderEntries2.contains("Gruppenintern"))
        check(subFolderEntries2.contains("Öffentlich"))
        check(subFolderEntries2.contains("Readme.md"))

        webdavAPI.delete("IOG/PR+FR/$folderName2")

        if (baseUrl != "http://127.0.0.1:7000") {
            val groupFoldersAPI: IGroupFoldersAPI = nc.getGroupFoldersAPI()

            val folderName3 = "Testprojekt mit Fix"
            val groupFolderToBeDeleted = nc.findGroupFolder("Eingang " + folderName3)
            if (groupFolderToBeDeleted != null) {
                groupFoldersAPI.deleteGroupFolder(groupFolderToBeDeleted.id)
            }

            webdavAPI.createFolder("IOG/PR+FR/$folderName3")
            val baseFolders3 = webdavAPI.listFiles("/IOG/PR+FR")
            check(baseFolders3.contains(folderName3))

            val memberGroup = Group(
                "1234", "testgroup", "Administratoren", null,
                emptySet<String>(), emptySet<String>(), emptySet<String>(),
                emptySet<String>(), emptySet<String>(), emptySet<String>()
            )
            val folderCreator3 = InputInternalPublicFolder(nc, "IOG/PR+FR/" + folderName3,
                folderName3, memberGroup.name)
            // create with fix
            folderCreator3.createSubfolders(true)

            val subFolderEntries3 = webdavAPI.listFiles("IOG/PR+FR/$folderName3")

            check(subFolderEntries3.count() == 5)
            check(subFolderEntries3.contains("Eingang"))
            check(subFolderEntries3.contains("Gruppenintern"))
            check(subFolderEntries3.contains("Öffentlich"))
            check(subFolderEntries3.contains("Readme.md"))

            val subFolderEntries4 = webdavAPI.listFiles("Eingang " + folderName3)

            check(subFolderEntries4.count() == 1)

            val groupFolderToBeDeleted2 = nc.findGroupFolder("Eingang " + folderName3)
            checkNotNull(groupFolderToBeDeleted2)
            groupFoldersAPI.deleteGroupFolder(groupFolderToBeDeleted2.id)
            webdavAPI.delete("IOG/PR+FR/$folderName3")
        }
    }

    private fun loadMockScenario() {
        mockWebdavAPI.loadIntoMockShareAPI(mockNextcloud)
    }
}*/
