/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.testframework

import dev.maximilian.feather.Group
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.OPGroupNames
import dev.maximilian.feather.iog.internal.settings.OPPName
import dev.maximilian.feather.iog.internal.tools.OpenProjectPCreator
import dev.maximilian.feather.iog.mockserver.GroupSyncMock
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.IogServiceApiConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectDescription
import dev.maximilian.feather.openproject.OpenProjectMembership
import dev.maximilian.feather.openproject.OpenProjectProject
import kotlinx.coroutines.runBlocking
import java.util.UUID

internal class OPUtilities(private val op: IOpenProject, backgroundJobManager: BackgroundJobManager, groupSyncMock: GroupSyncMock) {

    private val onc = OPNameConfig(mutableMapOf())
    private val opc = OpenProjectPCreator(op, onc, UUID.randomUUID(), groupSyncMock, backgroundJobManager)

    internal fun createProject(identifier: String, name: String, groupList: List<Group>, parentProjectName: String, groupKind: GroupKind) {
        runBlocking {
            createStandardProjects()
            val parent = op.getProjectByIdentifierOrName(parentProjectName)
            op.getProjectByIdentifierOrName(identifier)?.let { op.deleteProject(it) }

            val newProject = OpenProjectProject(
                3,
                name,
                identifier,
                OpenProjectDescription("", "Description of $name", "op/${OPPName.OTHER}}/$parentProjectName/$identifier"),
                parent
            )
            val projectCreated = op.createProject(newProject)

            val roles = op.getRoles()
            val no = when (groupKind) {
                GroupKind.REGIONAL_GROUP, GroupKind.PROJECT, GroupKind.BILA_GROUP, GroupKind.PR_FR_GROUP -> IogServiceApiConstants.GROUPS_PER_MEMBER_ADMIN_INTERESTED_PATTERN
                else -> IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN
            }
            val rolesToBeAssigned = opc.getRolesToBeAssigned(
                groupKind,
                no,
                roles
            )

            rolesToBeAssigned.forEachIndexed { index, openProjectRole ->
                openProjectRole?.let {
                    opc.reassignRole(groupList[index], projectCreated, openProjectRole.name, null)
                } ?: opc.createGroups(listOf(groupList[index]))
            }

            opc.getPublicRoles(groupKind, roles).forEach { op.createMembership(OpenProjectMembership(0, projectCreated, it.group, listOf(it.role))) }
        }
    }

    internal suspend fun createStandardProjects(): Pair<OpenProjectProject, OpenProjectProject> {
        val mainProject = createProject(OPPName.OTHER, "IOG", null)
        val rgIntern = createProject(OPPName.REGIONAL_GROUP, "RG Intern", mainProject)
        createProject(OPPName.PROJECTS, "Projekte", mainProject)
        createProject(OPPName.BILA, "BiLa", mainProject)
        createProject(OPPName.PR_FR, "PR-FR", mainProject)
        createProject(OPPName.COLLABORATION, "Projektübergreifende Zusammenarbeit", mainProject)
        val kgIntern = createProject(OPPName.COMMITTEE, "Ausschüsse und KGs", mainProject)
        if (op.getProjectByIdentifierOrName("vorstand") == null)
            opc.copyProjectAndAssignRoles(
                "Vorstand",
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
                "vorstand",
                listOf(
                    op.getGroupByName(LdapNames.ASSOCIATION_BOARD)!!,
                    op.getGroupByName(LdapNames.ASSOCIATION_BOARD + IogPluginConstants.admin_suffix)!!
                )
            )
        if (op.getProjectByIdentifierOrName("oms") == null)
            opc.copyProjectAndAssignRoles(
                "Ordentliche Mitglieder",
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
                IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
                "oms",
                listOf(
                    op.getGroupByName(LdapNames.OMS)!!,
                    op.getGroupByName(LdapNames.OMS + IogPluginConstants.admin_suffix)!!
                )
            )

        val gsProject = op.getProjectByIdentifierOrName("geschaeftsstelle") ?: opc.copyProjectAndAssignRoles(
            "Geschäftsstelle",
            GroupKind.NO_SPECIALITY_MEMBER_ADMIN_SECRET,
            IogServiceApiConstants.GROUPS_PER_SIMPLE_MEMBER_ADMIN_PATTERN,
            "geschaeftsstelle",
            listOf(
                op.getGroupByName(LdapNames.CENTRAL_OFFICE)!!,
                op.getGroupByName(LdapNames.CENTRAL_OFFICE + IogPluginConstants.admin_suffix)!!
            )
        )
        createProject("proko", "PROKO", gsProject)

        if (op.getProjectByIdentifierOrName("ap-sprecherinnen") == null)
            opc.copyProjectAndAssignRoles(
                "AP SprecherInnen",
                GroupKind.NO_SPECIALITY_MEMBER_ADMIN_PUBLIC,
                2,
                "ap-sprecherinnen",
                listOf(
                    op.getGroupByName(LdapNames.AP_SPRECHERINNEN)!!,
                    op.getGroupByName(LdapNames.AP_SPRECHERINNEN + IogPluginConstants.admin_suffix)!!
                )
            )
        return Pair(rgIntern, kgIntern)
    }

    private suspend fun createProject(identifier: String, name: String, parentProject: OpenProjectProject?): OpenProjectProject {
        val project = op.getProjectByIdentifierOrName(identifier)
        if (project == null) {
            val projectNew = OpenProjectProject(
                2,
                name,
                identifier,
                OpenProjectDescription("", identifier, "op/${OPPName.OTHER}/$identifier"),
                parentProject
            )
            return op.createProject(projectNew)
        }
        return project
    }

    internal suspend fun createGroups(groups: List<Group>) {
        val missingGroups = groups.filter { op.getGroupByName(onc.credentialGroupToOpenProjectGroup(it.name)) == null }
        if (missingGroups.isNotEmpty()) {
            opc.createGroups(missingGroups)
            // opc.createLdapGroupSynchronisations(groups, opGroups)
        }
    }

    fun removeProject(projectId: String, groups: List<String>) {
        runBlocking {
            val existingProject = op.getProjectByIdentifierOrName(projectId)
            if (existingProject != null) op.deleteProject(existingProject)

            groups.forEach { g -> op.getGroupByName(g)?.let { op.deleteGroup(it) } }
        }
    }

    fun reset() {
        runBlocking {
            op.getProjectByIdentifierOrName("iog")?.let { op.deleteProject(it) }
        }
    }
    fun removeGeneralTrialMembers() {
        runBlocking {
            op.getGroupByName(OPGroupNames.TRIAL_MEMBERS)?.let { op.deleteGroup(it) }
        }
    }
}
