/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package dev.maximilian.feather.iog

import dev.maximilian.feather.iog.internal.supportmember.SupportMemberDB
import dev.maximilian.feather.iog.internal.tools.DatabaseValidation
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull

class SupportRegisterTest {
    companion object {
        private val db = ApiTestUtilities.db
        private val databaseValidation = DatabaseValidation()
    }

    @Test
    fun `Test store and getExternalIDbyLDAPTest`() {
        val supportMemberRegister = SupportMemberDB(db)
        supportMemberRegister.deleteAll()
        supportMemberRegister.storeLDAPandExternalID("test", 221)
        supportMemberRegister.storeLDAPandExternalID("123", 777)
        supportMemberRegister.storeLDAPandExternalID("other", 444)
        val id = supportMemberRegister.getExternalIDbyLDAP("123")
        assertEquals(id, 777)
    }

    @Test
    fun `Test deleteAll`() {
        val supportMemberRegister = SupportMemberDB(db)

        supportMemberRegister.storeLDAPandExternalID("123", 777)
        val c1 = supportMemberRegister.countRegisteredDKPUser()
        assertEquals(1, c1)
        supportMemberRegister.deleteAll()

        val c2 = supportMemberRegister.countRegisteredDKPUser()
        assertEquals(0, c2)
    }

    @Test
    fun `Test notFoundMemberDeliversNull`() {
        val supportMemberRegister = SupportMemberDB(db)
        supportMemberRegister.deleteAll()
        assertNull(supportMemberRegister.getExternalIDbyLDAP("123"))
    }

    @Test
    fun `Test registerBirthday And Compare`() {
        val supportMemberRegister = SupportMemberDB(db)
        supportMemberRegister.deleteAll()
        val hash = databaseValidation.makeHash("HaraldTest01.01.1995")
        supportMemberRegister.storeHash(
            444,
            databaseValidation.makeHash("Donald05.02.1995"),
            databaseValidation.makeHash("ewe@t-online.de"),
            databaseValidation.makeHash("zzt@blabla.de")
        )
        supportMemberRegister.storeHash(
            777,
            hash,
            databaseValidation.makeHash("tester@t-online.de"),
            databaseValidation.makeHash("cheese@blabla.de")
        )
        supportMemberRegister.storeHash(
            999,
            databaseValidation.makeHash("Renate01.01.1994"),
            databaseValidation.makeHash("qq@t-online.de"),
            databaseValidation.makeHash("tt@blabla.de")
        )
        val q = supportMemberRegister.getExternalIDbyNameHash(hash)
        assertEquals(q, 777)
    }

    @Test
    fun `Test getExternalIDPerMail`() {
        val supportMemberRegister = SupportMemberDB(db)
        supportMemberRegister.deleteAll()
        val hash = databaseValidation.makeHash("tester@t-online.de")
        supportMemberRegister.storeHash(
            444,
            databaseValidation.makeHash("Donald05.02.1995"),
            databaseValidation.makeHash("ewe@t-online.de"),
            databaseValidation.makeHash("zzt@blabla.de")
        )
        supportMemberRegister.storeHash(
            777,
            databaseValidation.makeHash("HaraldTest01.01.1995"),
            hash,
            databaseValidation.makeHash("cheese@blabla.de")
        )
        supportMemberRegister.storeHash(
            999,
            databaseValidation.makeHash("Renate01.01.1994"),
            databaseValidation.makeHash("qq@t-online.de"),
            databaseValidation.makeHash("tt@blabla.de")
        )

        val c = supportMemberRegister.countSupportMember()
        assertEquals(c, 3)
        val q = supportMemberRegister.getExternalIDbyMail(hash)
        assertEquals(q, 777)
    }

    @Test
    fun `Test getExternalIDByAlternativeMail`() {
        val supportMemberRegister = SupportMemberDB(db)
        supportMemberRegister.deleteAll()
        val hash = databaseValidation.makeHash("tester@t-online.de")
        supportMemberRegister.storeHash(
            444,
            databaseValidation.makeHash("Donald05.02.1995"),
            databaseValidation.makeHash("ewe@t-online.de"),
            databaseValidation.makeHash("zzt@blabla.de")
        )
        supportMemberRegister.storeHash(
            777,
            databaseValidation.makeHash("HaraldTest01.01.1995"),
            databaseValidation.makeHash("cheese@blabla.de"),
            hash
        )
        supportMemberRegister.storeHash(
            999,
            databaseValidation.makeHash("Renate01.01.1994"),
            databaseValidation.makeHash("qq@t-online.de"),
            databaseValidation.makeHash("tt@blabla.de")
        )
        val q = supportMemberRegister.getExternalIDbyMail(hash)
        assertEquals(777, q)
    }
}
