/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

import dev.maximilian.feather.Group
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.groupPattern.MemberAdminInterestedPattern
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.user.IogUserInviteCheck
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.testframework.TestCredentialProvider
import dev.maximilian.feather.iog.testframework.TestUser
import dev.maximilian.feather.multiservice.settings.CheckUserEvent
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserInvitePermissionTest {
    private val credentials = TestCredentialProvider.credentialProvider
    private val checkUserEvent = IogUserInviteCheck(credentials) as CheckUserEvent

    init {
        if (credentials.getGroupByName(LdapNames.IOG_MEMBERS) == null)
            credentials.createGroup(Group("", LdapNames.IOG_MEMBERS, "InterestedPeople", "", emptySet(), emptySet(), emptySet(), emptySet(), emptySet(), emptySet()))
        if (credentials.getGroupByName(LdapNames.INTERESTED_PEOPLE) == null)
            credentials.createGroup(Group("", LdapNames.INTERESTED_PEOPLE, "IogMembers", "", emptySet(), emptySet(), emptySet(), emptySet(), emptySet(), emptySet()))

        val id = "rg-test"
        val description = "RG Test"
        val pat = MemberAdminInterestedPattern(credentials)
        pat.delete(id)
        val conf = CreateGroupConfig(id, description, emptySet(), GroupKind.REGIONAL_GROUP, "test.nextcloud.something")
        pat.create(conf)
    }

    @Test
    fun `Plausibility check ignores standard user in -admin group without INVITE permission`() {
        resetTestUser()
        val backupGroup = credentials.getGroupByName("rg-test-admin")!!
        val testUser = addStandardUserTo("rg-test-admin")
        val res = checkUserEvent.check(testUser, false)
        credentials.updateGroup(backupGroup)
        assertEquals(null, res)
    }

    @Test
    fun `Plausibility check ignores standard user with INVITE permission in -admin group`() {
        resetTestUser()
        val backupGroup = credentials.getGroupByName("rg-test-admin")!!
        addStandardUserTo("rg-test-admin")
        val testUser = addInvitePermissionToStandardUser()
        val res = checkUserEvent.check(testUser, false)
        credentials.updateGroup(backupGroup)
        assertEquals(null, res)
    }

    @Test
    fun `Plausibility check detects wrongly set INVITE permission`() {
        resetTestUser()
        val backupGroup = credentials.getGroupByName("rg-test-member")!!
        addStandardUserTo("rg-test-member")
        val testUser = addInvitePermissionToStandardUser()
        val res = checkUserEvent.check(testUser, false)
        credentials.updateGroup(backupGroup)
        assertEquals("INVITE Permission not necessary.", res)
    }

    @Test
    fun `Plausibility check detects user in two groups of same node`() {
        resetTestUser()
        val backupGroup = credentials.getGroupByName("rg-test-member")!!
        val backupGroup2 = credentials.getGroupByName("rg-test-admin")!!
        addStandardUserTo("rg-test-member")
        addStandardUserTo("rg-test-interested")
        val res = checkUserEvent.check(credentials.getUserByUsername(TestUser.standardUser.username)!!, false)
        credentials.updateGroup(backupGroup)
        credentials.updateGroup(backupGroup2)
        assertEquals("User is in two groups of same node.", res)
    }

    private fun addInvitePermissionToStandardUser(): User {
        val testUser = credentials.getUserByUsername(TestUser.standardUser.username)
        return credentials.updateUser(testUser!!.copy(permissions = setOf(Permission.INVITE)))!!
    }

    private fun addStandardUserTo(groupname: String): User {
        val adminGroup = credentials.getGroupByName(groupname)
        val testUser = credentials.getUserByUsername(TestUser.standardUser.username)
        credentials.updateGroup(adminGroup!!.copy(userMembers = setOf(testUser!!.id)))
        return credentials.getUserByUsername(TestUser.standardUser.username)!!
    }

    private fun resetTestUser() {
        credentials.getUserByUsername(TestUser.standardUser.username)?.also { credentials.deleteUser(it) }
        credentials.createUser(TestUser.standardUser)
    }
}
