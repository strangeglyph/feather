package dev.maximilian.feather.iog.internal.tools

import dev.maximilian.feather.openproject.OpenProjectFactory
import kotlinx.coroutines.runBlocking
import org.apache.commons.mail.util.MimeMessageParser
import java.util.Properties
import javax.mail.Authenticator
import javax.mail.Message
import javax.mail.Multipart
import javax.mail.PasswordAuthentication
import javax.mail.Session
import javax.mail.Transport
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import kotlin.test.Test
import kotlin.test.assertEquals

class EmailTicketCreatorTest {

    @Test
    fun testFetcher() {
        val fetcher = EmailFetcher(EmailTestUtil.imapConnectionInfo)

        val msg = makeTestMail(EmailTestUtil.smtpSession)
        Transport.send(msg)

        val email = fetcher.fetch(64)[0] as MimeMessage

        assertEquals(email.from[0].toString(), "debug@example.org")
        assertEquals(email.subject, "Test subject pls ignore")
        assertEquals(MimeMessageParser(email).parse().plainContent, "Test mail pls ignore")
    }

    @Test
    fun testCreateTicketFromMail() {
        val ticketsProject = runBlocking {
            OPTestUtil.openProject.getProjectByIdentifierOrName("Tickets")!!
        }

        val ticketeer =
            EmailTicketCreator(EmailTestUtil.imapConnectionInfo, 10, 10, OPTestUtil.openProject, ticketsProject)

        val msg = makeTestMail(EmailTestUtil.smtpSession)
        Transport.send(msg)

        assertEquals(1, ticketeer.poll())
    }

    private fun makeTestMail(session: Session): Message {
        val msg = MimeMessage(session)
        msg.setFrom("debug@example.org")
        msg.setRecipients(Message.RecipientType.TO, "debug@example.org")
        msg.subject = "Test subject pls ignore"

        val content = "Test mail pls ignore"

        val mimeBodyPart = MimeBodyPart()
        mimeBodyPart.setContent(content, "text/plain; charset=utf-8")

        val multipart: Multipart = MimeMultipart()
        multipart.addBodyPart(mimeBodyPart)

        msg.setContent(multipart)
        return msg
    }
}

object OPTestUtil {
    // Host and api baseUrl
    private val host: String = getEnv("OPENPROJECT_HOST", "http://127.0.0.1:8080")

    // Api auth
    private val authUser = getEnv("OPENPROJECT_USER", "admin")
    private val authPassword = getEnv("OPENPROJECT_PASSWORD", "admin")

    // SSO Webpage Auth
    private val ssoHeader: String = getEnv("OPENPROJECT_SSO_HEADER", "X-Remote-User")
    private val ssoSecret: String = getEnv("OPENPROJECT_SSO_SECRET", "s3cr3t")
    private val ssoAdmin: String = getEnv("OPENPROJECT_SSO_ADMIN", "sso_admin")

    internal val openProject = OpenProjectFactory().create(
        host,
        authUser,
        authPassword,
        ssoHeader,
        ssoAdmin,
        ssoSecret
    )
}

object EmailTestUtil {
    private val imapHost: String = getEnv("IMAP_HOST", "127.0.0.1")
    private val imapPort: Int = getEnv("IMAP_PORT", "9930").toInt()
    private val imapUser: String = getEnv("IMAP_USER", "debug@localdomain.test")
    private val imapPass: String = getEnv("IMAP_PASS", "debug")
    private val imapUseSSL: Boolean = getEnv("IMAP_SSL", "true").toBooleanStrict()

    internal val imapConnectionInfo =
        ConnectionInfo(imapHost, imapPort, imapUser, imapPass, imapUseSSL, overrideTrustStore = true)

    private val smtpHost: String = getEnv("SMTP_HOST", "127.0.0.1")
    private val smtpPort: Int = getEnv("SMTP_PORT", "2500").toInt()
    private val smtpUser: String = getEnv("SMTP_USER", "debug@example.org")
    private val smtpPass: String = getEnv("SMTP_PASS", "debug")
    private val smtpUseAuth: Boolean = getEnv("SMTP_AUTH", "false").toBooleanStrict()
    private val smtpUseSSL: Boolean = getEnv("SMTP_SSL", "false").toBooleanStrict()

    internal val smtpSession = makeSmtpSession()

    private fun makeSmtpSession(): Session {
        val prop = Properties()
        prop.put("mail.smtp.auth", smtpUseAuth)
        prop.put("mail.smtp.ssl.enable", smtpUseSSL)
        prop.put("mail.smtp.host", smtpHost)
        prop.put("mail.smtp.port", smtpPort)

        if (smtpUseSSL) {
            prop.put("mail.smtp.ssl.trust", smtpHost)
        }

        val session = if (smtpUseAuth) {
            val auth: Authenticator = object : Authenticator() {
                override fun getPasswordAuthentication(): PasswordAuthentication {
                    return PasswordAuthentication(smtpUser, smtpPass)
                }
            }

            Session.getInstance(prop, auth)!!
        } else {
            Session.getInstance(prop)
        }

        session.debug = true
        return session
    }
}

internal fun getEnv(name: String, default: String? = null): String =
    System.getenv(name) ?: default ?: throw IllegalArgumentException("Environment variable $name not found")
