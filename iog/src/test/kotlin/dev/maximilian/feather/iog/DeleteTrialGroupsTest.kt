/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog

import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.iog.testframework.ApiTestUtilities
import dev.maximilian.feather.iog.testframework.TestCredentialProvider
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.openproject.OpenProjectGroup
import kong.unirest.GenericType
import kong.unirest.HttpRequest
import kong.unirest.HttpRequestWithBody
import kong.unirest.HttpResponse
import kotlinx.coroutines.runBlocking
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DeleteTrialGroupsTest {

    private val backgroundJobManager = BackgroundJobManager(ApiTestUtilities.pool)
    private val apiTestUtilities = ApiTestUtilities()

    init {
        apiTestUtilities.startIogPlugin(true, backgroundJobManager)
        apiTestUtilities.createStandardGroups()
        runBlocking {
            apiTestUtilities.scenario!!.opUtilities.createStandardProjects()
        }
    }

    private val credentials = TestCredentialProvider.credentialProvider

    @Test
    fun `Delete trial groups by standard user returns FORBIDDEN (403)`() {
        apiTestUtilities.loginWithStandardUser()
        val response = deleteTrialByApi().asEmpty()
        apiTestUtilities.loginWithAdminUser()
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun
    `Delete trial group returns correct body and removes all trial groups`() {
        apiTestUtilities.loginWithAdminUser()
        apiTestUtilities.createRGTest()
        val response = deleteTrialByApi().asObject<BackgroundJobWithStrings>()
        assertEquals(HttpStatus.CREATED_201, response.status)
        val job = response.body
        assertNotNull(job)
        // assertEquals(Permission.ADMIN, job.permission)
        assertEquals("ADMIN", job.permission)
        assertEquals("started", job.status)

        // wait until job is completed (otherwise the parallel execution breaks the test)
        do {
            val jobWorking = backgroundJobManager.getJobStatus(job.id)
            checkNotNull(jobWorking)
        } while (jobWorking?.completed == null)
        val g = credentials.getGroups()
        assertTrue(g.any { it.name == "rg-test-member" }, "Es wurde die RG Member-Gruppe gelöscht.")
        assertTrue(g.any { it.name == "rg-test-interested" }, "Es wurde die RG Interested-Gruppe gelöscht.")
        assertTrue(g.any { it.name == "rg-test" }, "Es wurde die RG HauptGruppe gelöscht.")
        assertTrue(g.filter { it.name.endsWith(IogPluginConstants.trial_suffix) }.isEmpty(), "Es wurde die RG Trial-Gruppe nicht gelöscht.")
        runBlocking {
            assertTrue(
                apiTestUtilities.scenario!!.openProject!!.getGroups()
                    .filter { it.name.endsWith(IogPluginConstants.trial_suffix) }.isEmpty(),
                "Es wurde die RG Trial-Gruppe nicht in OP gelöscht."
            )
            assertFalse(
                apiTestUtilities.scenario!!.openProject!!.getGroups()
                    .any { it.name == OPNameConfig(mutableMapOf()).trialMemberGroupName },
                "Es wurde die TrialMembers-Gruppe nicht in OP gelöscht."
            )
            val roles = apiTestUtilities.scenario!!.openProject!!.getRoles()
            val r = apiTestUtilities.scenario!!.openProject!!.getMemberships(
                listOf(roles.find { it.name == OPNameConfig(mutableMapOf()).memberRoleName }!!)
            )
            val t = r.mapNotNull { (it.principal.value as? OpenProjectGroup)?.name }.filter { it.endsWith(IogPluginConstants.trial_suffix) }
            assertTrue(t.isEmpty(), "Es gibt noch Trial Memberships in OP: " + t.joinToString())
        }
        val ncGroups = apiTestUtilities.scenario!!.ncUtilities.nextcloud.findGroupFolder(NextcloudFolders.GroupShare)?.groups?.mapNotNull { it.key } ?: emptyList()

        assertFalse(ncGroups.contains(LdapNames.TRIAL_MEMBERS))
        assertTrue(ncGroups.filter { it.endsWith(IogPluginConstants.trial_suffix) }.isEmpty())
        apiTestUtilities.removeRGTest()
    }

    private fun deleteTrialByApi(): HttpRequestWithBody =
        apiTestUtilities
            .restConnection
            .delete("${apiTestUtilities.scenario!!.basePath}/bindings/iog/chapter/trials")

    private inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> =
        this.asObject(object : GenericType<B>() {})
}
