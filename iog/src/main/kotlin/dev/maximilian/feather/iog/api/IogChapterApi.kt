/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.api

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.iog.internal.chapter.CreateTrials
import dev.maximilian.feather.iog.internal.chapter.DeleteTrialGroups
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder
import io.javalin.core.security.SecurityUtil
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import kong.unirest.HttpStatus
import mu.KotlinLogging

internal class IogChapterApi(
    app: Javalin,
    val credentialProvider: ICredentialProvider,
    val backgroundJobManager: BackgroundJobManager,
    private val openProjectService: OpenProjectService,
    private val syncEvent: GroupSynchronizationEvent,
    private val onc: OPNameConfig,
    private val nc: Nextcloud,
    private val nextcloudPublicURL: String
) {
    init {
        app.routes {
            ApiBuilder.path("bindings/iog/chapter") {
                ApiBuilder.post("/trials", ::handleCreateTrialGroupsJob, SecurityUtil.roles(Permission.ADMIN))
                ApiBuilder.delete("/trials", ::handleDeleteTrialGroupsJob, SecurityUtil.roles(Permission.ADMIN))
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    private fun handleCreateTrialGroupsJob(ctx: Context) {
        val session = ctx.session()

        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        val config = ctx.body<Boolean>()

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not create trial groups by admin ${creator.displayName} due to errors: $errorMessages" }

            ctx.status(HttpStatus.BAD_REQUEST)
            ctx.json(errorMessages)
        } else {
            logger.info { "About to create trial groups by admin ${creator.displayName}" }

            val t = CreateTrials(
                credentialProvider,
                backgroundJobManager,
                openProjectService,
                syncEvent,
                onc,
                nc,
                nextcloudPublicURL
            )

            try {
                val newJob = backgroundJobManager.runBackgroundJob(
                    t::createTrialGroups,
                    config
                )

                ctx.status(HttpStatus.CREATED)
                ctx.json(newJob)
            } catch (e: Exception) {
                logger.warn(e) { "Will not create trial groups by admin ${creator.displayName} due to exception" }

                errorMessages += "Interner Fehler (Siehe Feather Logs)"
                ctx.status(HttpStatus.BAD_REQUEST)
                ctx.json(errorMessages)
                return
            }
        }
    }

    private fun handleDeleteTrialGroupsJob(ctx: Context) {
        val session = ctx.session()

        val creator = session.user
        if (!creator.permissions.contains(Permission.ADMIN)) {
            throw ForbiddenResponse()
        }

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not delete trial groups by admin ${creator.displayName} due to errors: $errorMessages" }

            ctx.status(HttpStatus.BAD_REQUEST)
            ctx.json(errorMessages)
        } else {
            logger.info { "About to delete trial groups by admin ${creator.displayName}" }

            val t = DeleteTrialGroups(
                credentialProvider,
                openProjectService.openproject,
                backgroundJobManager,
                nc,
                onc
            )

            try {
                val newJob = backgroundJobManager.runBackgroundJob(
                    t::delete,
                    true
                )

                ctx.status(HttpStatus.CREATED)
                ctx.json(newJob)
            } catch (e: Exception) {
                logger.warn(e) { "Will not delete trials by admin ${creator.displayName} due to exception" }

                errorMessages += "Interner Fehler (Siehe Feather Logs)"
                ctx.status(HttpStatus.BAD_REQUEST)
                ctx.json(errorMessages)
                return
            }
        }
    }
}
