/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.groupPattern

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.CreateGroup
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.groupPattern.metaDescription.GroupMetaDescription
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.settings.IogPluginConstants

internal class MemberAdminInterestedPattern(
    var credentialProvider: ICredentialProvider
) : IGroupPattern {
    companion object {
        fun concatOrIgnore(name: String, prefixAndSuffix: Pair<String?, String?>): String {
            var result = name
            val prefix: String? = prefixAndSuffix.first
            if (prefix != null) {
                result = concatPrefix(result, prefix)
            }
            val suffix: String? = prefixAndSuffix.second
            if (suffix != null) {
                result = concatSuffix(result, suffix)
            }
            return result
        }

        private fun concatPrefix(name: String, prefix: String): String {
            val dashedPrefix = "$prefix-"
            return if (name.startsWith(dashedPrefix)) {
                name
            } else {
                dashedPrefix + name
            }
        }

        private fun concatSuffix(name: String, suffix: String): String {
            val dashedSuffix = "-$suffix"
            return if (name.endsWith(dashedSuffix)) {
                name
            } else {
                name + dashedSuffix
            }
        }
    }

    private val adminDescriptionPrefix = "Administratoren von "
    private val memberDescriptionPrefix = "Fördermitglieder der Gruppe "
    private val interestedDescriptionPrefix = "Interessierte der Gruppe "
    private val trialDescriptionPrefix = "Aktive Interessierte der Gruppe "
    private val mainGroupDescriptionPrefix = "Alle Mitglieder von "

    override fun create(config: CreateGroupConfig): List<Group> {
        val iogMembers = credentialProvider.getGroupByName(LdapNames.IOG_MEMBERS)
        requireNotNull(iogMembers) { "Cannot find group '${LdapNames.IOG_MEMBERS}'" }

        val interested = credentialProvider.getGroupByName(LdapNames.INTERESTED_PEOPLE)
        requireNotNull(interested) { "Cannot find group '${LdapNames.INTERESTED_PEOPLE}'" }

        val trials = credentialProvider.getGroupByName(LdapNames.TRIAL_MEMBERS)
        requireNotNull(trials) { "Cannot find group '${LdapNames.TRIAL_MEMBERS}'" }

        val interestedGroup = credentialProvider.createGroup(createInterestedGroupBody(interested.id, config))
        val trialGroup = credentialProvider.createGroup(createTrialGroupBody(trials.id, config))
        val memberGroup = credentialProvider.createGroup(createMemberGroupBody(iogMembers.id, config))
        val masterGroup = credentialProvider.createGroup(createMainGroupBody(interestedGroup.id, memberGroup.id, config))
        val adminGroup = credentialProvider.createGroup(createAdminGroupBody(setOf(interestedGroup.id, memberGroup.id), config))

        CreateGroup.logger.info {
            "MemberAdminInterestedPattern::create Created LDAP group ${masterGroup.name} with admin group ${adminGroup.name}, "
            "trial group ${trialGroup.name}, interested group ${interestedGroup.name}, and member group ${memberGroup.name}"
        }
        return arrayOf(masterGroup, adminGroup, interestedGroup, memberGroup, trialGroup).toList()
    }

    override fun search(prefixedName: String): List<Group> {
        val iogMembers = credentialProvider.getGroupByName(LdapNames.IOG_MEMBERS)
        requireNotNull(iogMembers) { "Cannot find group '${LdapNames.IOG_MEMBERS}'" }

        val interested = credentialProvider.getGroupByName(LdapNames.INTERESTED_PEOPLE)
        requireNotNull(interested) { "Cannot find group '${LdapNames.INTERESTED_PEOPLE}'" }

        val interestedGroup = credentialProvider.getGroupByName(getInterestedGroupName(prefixedName))
        requireNotNull(interestedGroup) { "Cannot find group '${getInterestedGroupName(prefixedName)}'" }

        val memberGroup = credentialProvider.getGroupByName(getMemberGroupName(prefixedName))
        requireNotNull(memberGroup) { "Cannot find group '${getMemberGroupName(prefixedName)}'" }

        val rgGroup = credentialProvider.getGroupByName(prefixedName)
        requireNotNull(rgGroup) { "Cannot find group '$prefixedName'" }

        val adminGroup = credentialProvider.getGroupByName(getAdminGroupName(prefixedName))
        requireNotNull(adminGroup) { "Cannot find group '${getAdminGroupName(prefixedName)}'" }

        val trialGroup = credentialProvider.getGroupByName(getTrialGroupName(prefixedName))
        requireNotNull(trialGroup) { "Cannot find group '${getTrialGroupName(prefixedName)}'" }

        CreateGroup.logger.info {
            "MemberAdminInterestedPattern::search Found existing LDAP group" +
                " ${rgGroup.name} with admin group ${adminGroup.name}, interested group ${interestedGroup.name}, " +
                "and member group ${memberGroup.name} as well as trial group ${trialGroup.name}"
        }

        return arrayOf(rgGroup, adminGroup, interestedGroup, memberGroup, trialGroup).toList()
    }

    private fun createMemberGroupBody(parentID: String, conf: CreateGroupConfig): Group {
        return Group(
            id = "",
            name = getMemberGroupName(conf.ldapName),
            description = "$memberDescriptionPrefix${conf.description}",
            dnParent = null,
            parentGroups = setOf(parentID),
            userMembers = emptySet(),
            owners = emptySet(),
            groupMembers = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet()
        )
    }

    private fun createInterestedGroupBody(parentGroupID: String, conf: CreateGroupConfig): Group {
        return Group(
            id = "",
            name = getInterestedGroupName(conf.ldapName),
            description = "$interestedDescriptionPrefix${conf.description}",
            dnParent = null,
            parentGroups = setOf(parentGroupID),
            userMembers = emptySet(),
            groupMembers = emptySet(),
            owners = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet()
        )
    }

    private fun createTrialGroupBody(parentGroupID: String, conf: CreateGroupConfig): Group {
        return Group(
            id = "",
            name = getTrialGroupName(conf.ldapName),
            description = "$trialDescriptionPrefix${conf.description}",
            dnParent = null,
            parentGroups = setOf(parentGroupID),
            userMembers = emptySet(),
            groupMembers = emptySet(),
            owners = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet()
        )
    }

    private fun createMainGroupBody(interestedGroupID: String, memberGroupID: String, conf: CreateGroupConfig): Group {
        return Group(
            id = "",
            name = conf.ldapName,
            description = "$mainGroupDescriptionPrefix${conf.description}",
            dnParent = null,
            parentGroups = emptySet(),
            userMembers = emptySet(),
            groupMembers = setOf(interestedGroupID, memberGroupID),
            owners = emptySet(),
            ownerGroups = emptySet(), // filled later
            ownedGroups = emptySet()
        )
    }

    private fun createAdminGroupBody(ownedGroupsG: Set<String>, conf: CreateGroupConfig): Group {
        return Group(
            id = "",
            name = getAdminGroupName(conf.ldapName),
            description = "$adminDescriptionPrefix${conf.description}",
            dnParent = null,
            parentGroups = emptySet(),
            userMembers = conf.ownerIDs,
            owners = emptySet(),
            groupMembers = emptySet(),
            ownerGroups = emptySet(),
            ownedGroups = ownedGroupsG
        )
    }

    private fun getTrialGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.trial_suffix
    }

    private fun getMemberGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.member_suffix
    }

    private fun getInterestedGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.interested_suffix
    }

    private fun getAdminGroupName(prefixedName: String): String {
        return prefixedName + IogPluginConstants.admin_suffix
    }

    override fun getMetaDescription(groupName: String, description: String): GroupMetaDescription {
        if (groupName.endsWith(IogPluginConstants.admin_suffix)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.admin_suffix.length)
            return GroupMetaDescription(
                name = getAdminGroupName(prefixedName),
                description = "$adminDescriptionPrefix$description",
                dnParent = "",
                parentGroupNames = emptySet(),
                ownerNames = emptySet(),
                groupMemberNames = emptySet(),
                ownerGroupNames = emptySet(),
                ownedGroupNames = setOf(getInterestedGroupName(prefixedName), getMemberGroupName(prefixedName)),
                descriptionWithoutPrefix = description
            )
        } else if (groupName.endsWith(IogPluginConstants.member_suffix)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.member_suffix.length)
            return GroupMetaDescription(
                name = getMemberGroupName(prefixedName),
                description = "$memberDescriptionPrefix$description",
                dnParent = "",
                parentGroupNames = setOf(LdapNames.IOG_MEMBERS),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description
            )
        } else if (groupName.endsWith(IogPluginConstants.interested_suffix)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.interested_suffix.length)
            return GroupMetaDescription(
                name = getInterestedGroupName(prefixedName),
                description = "$interestedDescriptionPrefix$description",
                dnParent = "",
                parentGroupNames = setOf(LdapNames.INTERESTED_PEOPLE),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description
            )
        } else if (groupName.endsWith(IogPluginConstants.trial_suffix)) {
            val prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.trial_suffix.length)
            return GroupMetaDescription(
                name = getTrialGroupName(prefixedName),
                description = "$trialDescriptionPrefix$description",
                dnParent = "",
                parentGroupNames = setOf(LdapNames.INTERESTED_PEOPLE),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getAdminGroupName(prefixedName)),
                ownerGroupNames = emptySet(),
                ownedGroupNames = emptySet(),
                descriptionWithoutPrefix = description
            )
        } else {
            return GroupMetaDescription(
                name = groupName,
                description = "$mainGroupDescriptionPrefix$description",
                dnParent = "",
                parentGroupNames = setOf(),
                ownerNames = emptySet(),
                groupMemberNames = setOf(getMemberGroupName(groupName), getInterestedGroupName(groupName)),
                ownerGroupNames = setOf(),
                ownedGroupNames = setOf(),
                descriptionWithoutPrefix = description
            )
        }
    }
    override fun getAllMetaDescriptions(groupName: String, description: String): List<GroupMetaDescription> {
        val prefixedName = removeSuffixIfNecessary(groupName)
        return listOf(
            getMetaDescription(prefixedName, description),
            getMetaDescription(getAdminGroupName(prefixedName), description),
            getMetaDescription(getInterestedGroupName(prefixedName), description),
            getMetaDescription(getMemberGroupName(prefixedName), description),
            getMetaDescription(getTrialGroupName(prefixedName), description)
        )
    }
    private fun removeSuffixIfNecessary(groupName: String): String {
        var prefixedName = groupName
        if (groupName.endsWith(IogPluginConstants.admin_suffix)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.admin_suffix.length)
        } else if (groupName.endsWith(IogPluginConstants.member_suffix)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.member_suffix.length)
        } else if (groupName.endsWith(IogPluginConstants.interested_suffix)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.interested_suffix.length)
        } else if (groupName.endsWith(IogPluginConstants.trial_suffix)) {
            prefixedName = groupName.substring(0, groupName.length - IogPluginConstants.trial_suffix.length)
        }
        return prefixedName
    }

    override fun isAdminGroup(groupName: String): Boolean {
        return groupName.endsWith(IogPluginConstants.admin_suffix)
    }

    override fun userMembersAllowed(groupName: String): Boolean {
        return groupName.endsWith(IogPluginConstants.admin_suffix) ||
            groupName.endsWith(IogPluginConstants.member_suffix) ||
            groupName.endsWith(IogPluginConstants.interested_suffix) ||
            groupName.endsWith(IogPluginConstants.trial_suffix)
    }

    override fun delete(groupName: String) {
        credentialProvider.getGroupByName(getMemberGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(getAdminGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(getInterestedGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(getTrialGroupName(groupName))?.let { credentialProvider.deleteGroup(it) }
        credentialProvider.getGroupByName(groupName)?.let { credentialProvider.deleteGroup(it) }
    }

    override fun isMainGroup(groupName: String): Boolean {
        return groupName == removeSuffixIfNecessary(groupName)
    }

    fun addTrialGroup(config: CreateGroupConfig): Group {
        val trials = credentialProvider.getGroupByName(LdapNames.TRIAL_MEMBERS)
        requireNotNull(trials) { "Cannot find group '${LdapNames.TRIAL_MEMBERS}'" }

        val trialGroup = credentialProvider.createGroup(createTrialGroupBody(trials.id, config))
        val adminName = getAdminGroupName(config.ldapName)
        val adminGroup = credentialProvider.getGroupByName(adminName) ?: throw Exception("Could not find admin group $adminName to create trial group.")
        credentialProvider.updateGroup(adminGroup.copy(groupMembers = adminGroup.groupMembers.plus(trialGroup.id)))
        return trialGroup
    }
}
