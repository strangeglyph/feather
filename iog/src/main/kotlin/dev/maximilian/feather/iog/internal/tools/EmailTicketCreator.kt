package dev.maximilian.feather.iog.internal.tools

import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.openproject.OpenProjectProject
import dev.maximilian.feather.openproject.OpenProjectWorkPackageType
import kotlinx.coroutines.runBlocking
import mu.KLogging
import org.apache.commons.mail.util.MimeMessageParser
import org.isomorphism.util.TokenBucket
import org.isomorphism.util.TokenBuckets
import java.time.Duration
import java.util.Properties
import java.util.concurrent.TimeUnit
import javax.mail.Flags
import javax.mail.Folder
import javax.mail.Session
import javax.mail.internet.MimeMessage
import kotlin.concurrent.fixedRateTimer
import kotlin.math.min

public data class ConnectionInfo(
    val host: String,
    val port: Int,
    val user: String,
    val password: String,
    val useSSL: Boolean = true,
    val overrideTrustStore: Boolean = false
)

/**
 * Service to regularly check a given IMAP inbox for new emails and create corresponding tickets in a given OP Project.
 * The inbox is checked every 10 seconds, but to prevent attacks, only a limited number of tickets can be created in any
 * one hour.
 *
 * The maximum number of tickets per hour is given by `checksPerHour`. If no new emails were received for several hours,
 * this accumulates up to `maxBurst`.
 *
 * This service requires an issue tracker to create work packages in. This project needs to support work packages with
 * the type BUG. This can be specified in the settings dialogue of the project.
 */
internal class EmailTicketCreator(
    connectionInfo: ConnectionInfo,
    maxBurst: Long,
    checksPerHour: Long,
    private val openproject: IOpenProject,
    private val issueTracker: OpenProjectProject
) {
    companion object : KLogging()

    private val bucket: TokenBucket = TokenBuckets
        .builder()
        .withCapacity(maxBurst)
        .withFixedIntervalRefillStrategy(1, 60 /* sec/min */ * 60 /* min/hr */ / checksPerHour, TimeUnit.SECONDS)
        .withInitialTokens(maxBurst)
        .build()

    private val fetcher = EmailFetcher(connectionInfo)

    fun run() {
        fixedRateTimer("Email->Ticket poll thread", true, 0, Duration.ofSeconds(10).toMillis()) {
            poll()
        }
    }

    internal fun poll(): Int {
        if (bucket.capacity == 0L) {
            return 0
        }

        logger.info("Checking for up to ${bucket.capacity} new emails")
        val messages = fetcher.fetch(bucket.capacity.toInt())
        bucket.consume(messages.size.toLong())
        logger.info("Received ${messages.size} messages")

        messages.parallelStream().forEach { message ->
            runBlocking {
                val body = MimeMessageParser(message).parse()
                val content = if (body.hasPlainContent()) {
                    body.plainContent
                } else {
                    // TODO error logging or handling - do we strip HTML or just post with tags?
                    body.htmlContent
                }

                val workpackage =
                    openproject.createWorkPackage(
                        message.subject,
                        content,
                        issueTracker,
                        OpenProjectWorkPackageType.BUG
                    )
                logger.info("Message ${message.hashCode()}: Created work package ${workpackage.id}")

                val numSenders = message.from?.size ?: 0
                if (numSenders > 0) {
                    val user = openproject.getUserByEmail(message.from[0].toString())
                    if (user != null) {
                        openproject.assignWatcher(workpackage, user)
                        logger.info("Found user corresponding to ${user.email} - succesfully assigned as watcher")
                    } else {
                        // TODO send email reply instead?
                        logger.info("No user found with mail ${message.from[0]}")
                    }
                }
            }
        }

        return messages.size
    }
}

internal class EmailFetcher(private val connectionInfo: ConnectionInfo) {
    private val session: Session

    init {
        val properties = Properties()
        properties.setProperty("mail.imap.ssl.enable", connectionInfo.useSSL.toString())
        if (connectionInfo.overrideTrustStore) {
            properties.setProperty("mail.imap.ssl.trust", connectionInfo.host)
        }
        session = Session.getInstance(properties)
    }

    /**
     * Fetches emails from the specified IMAP account's inbox. Fetched emails are subsequently deleted.
     *
     * @param amount The number of messages to fetch. The actual number of messages fetched may be lower if
     * the inbox does not contain enough messages.
     */
    internal fun fetch(amount: Int): List<MimeMessage> {
        val result = mutableListOf<MimeMessage>()

        val store = session.getStore("imap")
        store.connect(connectionInfo.host, connectionInfo.port, connectionInfo.user, connectionInfo.password)

        store.use { store ->
            store.getFolder("inbox").use { inbox ->
                inbox.open(Folder.READ_WRITE)

                val retrieved = min(amount, inbox.messageCount)

                inbox.getMessages(1, retrieved).forEach {
                    // javax.mail fetches message content lazily. However, this retrieval is not possible
                    // once the folder containing the message has been closed.
                    // Explicitly copying the message forces the message content to be fetched and makes
                    // it available outside this block.
                    if (it is MimeMessage) {
                        result.add(MimeMessage(it))
                    } else {
                        // TODO error logging
                    }

                    it.setFlag(Flags.Flag.DELETED, true)
                }
                inbox.expunge()
            }
        }

        return result
    }
}
