/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.group

import dev.maximilian.feather.Group
import dev.maximilian.feather.iog.internal.groupPattern.MemberAdminInterestedPattern
import dev.maximilian.feather.iog.internal.tools.NextcloudFolderCreator
import dev.maximilian.feather.iog.internal.tools.OpenProjectPCreator
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.openproject.IOpenProject
import mu.KLogging
import java.util.UUID

internal class CreateGroup(
    private val openproject: IOpenProject?,
    private val nextcloud: Nextcloud?,
    private val onc: OPNameConfig,
    private val synchronizer: GroupSynchronizationEvent,
    private val backgroundJobManager: BackgroundJobManager,
    private val ldapGroupCreator: IogGroupSchema
) {
    companion object : KLogging()

    suspend fun createOPandNCandLDAP(jobId: UUID?, config: CreateGroupConfig): List<Group> {
        logger.info { "CreateGroup::createOPandNCandLDAP Creating group (${config.ldapName})" }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Creating LDAP group(s)") }

        val createdGroups = ldapGroupCreator.createSubGroups(config)

        // use the prefixed name instead of groupToBeCreated.name
        val groupIdentifier = createdGroups.first().name

        if (config.groupKind != GroupKind.LDAP_ONLY) {
            // Create group(s) in OpenProject
            if (openproject != null) {
                val opc = OpenProjectPCreator(openproject, onc, jobId, synchronizer, backgroundJobManager)
                try {
                    val listOfGroups = opc.createGroups(createdGroups)
                    val newProject = opc.copyProjectAndAssignRoles(
                        config.description, config.groupKind,
                        createdGroups.size, groupIdentifier, listOfGroups
                    )
                    logger.info { "CreateGroup::createOP Create group(s) and project (new ID: ${newProject.id}) in OpenProject successfull." }
                } catch (e: Exception) {
                    logger.error(e) { "CreateGroup::createOPandNCandLDAP Create group(s) succesful. Create Project in OpenProject not successfull." }
                    throw RuntimeException("Create project(s) in OpenProject not successfull")
                }
            }
        }

        if (nextcloud != null && config.groupKind != GroupKind.LDAP_ONLY) {
            createNextcloud(config, createdGroups, jobId, config.publicUrl)
        }

        return createdGroups
    }

    suspend fun createOPandNCOnly(jobId: UUID?, config: CreateGroupConfig): List<Group> {
        logger.info { "CreateGroup::createOPandNCOnly Creating group (${config.ldapName})" }

        jobId?.let { backgroundJobManager.setJobStatus(it, "Searching LDAP group(s)") }

        val createdGroups = when (config.groupKind) {
            // search for the RG in case of BILA and PR/FR groups
            GroupKind.BILA_GROUP,
            GroupKind.PR_FR_GROUP -> ldapGroupCreator.searchSubGroups(config.ldapName, GroupKind.REGIONAL_GROUP)

            else -> ldapGroupCreator.searchSubGroups(config.ldapName, config.groupKind)
        }

        // use the prefixed name instead of groupToBeCreated.name (note: createdGroups could be another group)
        val prefixAndSuffix = IogGroupSchema.groupPrefixAndSuffix(config.groupKind)
        val groupIdentifier = MemberAdminInterestedPattern.concatOrIgnore(config.ldapName, prefixAndSuffix)

        if (config.groupKind != GroupKind.LDAP_ONLY) {
            // Create group(s) in OpenProject
            if (openproject != null) {
                try {
                    val opc = OpenProjectPCreator(openproject, onc, jobId, synchronizer, backgroundJobManager)
                    val listOfGroups = opc.searchGroups(createdGroups)
                    val newProject = opc.copyProjectAndAssignRoles(config.description, config.groupKind, createdGroups.size, groupIdentifier, listOfGroups)
                    logger.info { "CreateGroup::createOP Create group(s) and project (new ID: ${newProject.id}) in OpenProject successfull." }
                } catch (e: Exception) {
                    logger.error(e) { "CreateGroup::createOPandNCOnly Search group(s) succesful. Create Project in OpenProject not successfull." }
                    throw RuntimeException("Create project(s) in OpenProject not successfull")
                }
            }
        }

        if (nextcloud != null && config.groupKind != GroupKind.LDAP_ONLY) {
            createNextcloud(config, createdGroups, jobId, config.publicUrl)
        }

        return createdGroups
    }

    private fun createNextcloud(
        config: CreateGroupConfig,
        createdGroups: List<Group>,
        jobId: UUID?,
        publicUrl: String
    ) {
        if (nextcloud == null)
            throw IllegalStateException()

        try {
            val nfc = NextcloudFolderCreator(nextcloud, jobId, backgroundJobManager, publicUrl)
            nfc.createFolderStructureForGroupNextcloud(config.ldapName, config.description, config.groupKind, createdGroups)
            logger.info("CreateGroup::createNextcloud Creating folder structure for group in Nextcloud successfull")
        } catch (e: Exception) {
            logger.error(e) { "Failed to create folder structure" }
            // forward error to caller
            throw e
        }
    }
}
