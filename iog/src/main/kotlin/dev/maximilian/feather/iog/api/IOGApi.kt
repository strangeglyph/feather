/*
*    Copyright [2020-2021] Feather development team, see AUTHORS.md
*
*    Licensed under the Apache License, Version 2.0 (the "License");
*    you may not use this file except in compliance with the License.
*    You may obtain a copy of the License at
*
*        http://www.apache.org/licenses/LICENSE-2.0
*
*    Unless required by applicable law or agreed to in writing, software
*    distributed under the License is distributed on an "AS IS" BASIS,
*    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*    See the License for the specific language governing permissions and
*    limitations under the License.
*/

package dev.maximilian.feather.iog.api

import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.iog.api.bindings.ResetSandboxRequest
import dev.maximilian.feather.iog.internal.tools.ResetSandbox
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.openproject.IOpenProject
import dev.maximilian.feather.session
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.core.security.SecurityUtil.roles
import io.javalin.http.Context
import io.javalin.http.ForbiddenResponse
import kotlinx.coroutines.runBlocking
import mu.KotlinLogging
import java.util.UUID

internal class IOGApi(
    app: Javalin,
    private val openProject: IOpenProject,
    private val backgroundJobManager: BackgroundJobManager,
    private val onc: OPNameConfig
) {
    init {
        app.routes {
            path("bindings/iog") {
                get("/sandbox", ::handleGetSandbox, roles(Permission.ADMIN, Permission.RESET_SANDBOX))
                post("/sandbox/reset", ::handleResetSandbox, roles(Permission.ADMIN, Permission.RESET_SANDBOX))
            }
        }
    }

    private val logger = KotlinLogging.logger {}

    private fun User.canResetSandbox() =
        permissions.contains(Permission.RESET_SANDBOX) || permissions.contains(Permission.ADMIN)

    private fun handleGetSandbox(ctx: Context) {
        val session = ctx.session()

        val creator = session.user
        if (!creator.canResetSandbox()) {
            throw ForbiddenResponse()
        }

        val resetSandboxService = ResetSandbox(
            onc,
            openProject,
            backgroundJobManager
        )

        logger.info { "About to get sandbox status" }

        try {
            val sandboxStatus = resetSandboxService.getSandboxStatus()

            ctx.status(200)
            ctx.json(sandboxStatus)
        } catch (e: Exception) {
            logger.warn(e) { "Failed to get sandbox status due to exception" }

            val errorMessages = arrayOf("Interner Fehler (Benachrichtige die IT)")
            ctx.status(400)
            ctx.json(errorMessages)
        }
    }

    private fun handleResetSandbox(ctx: Context) {
        val session = ctx.session()

        val creator = session.user
        if (!creator.canResetSandbox()) {
            throw ForbiddenResponse()
        }

        val launchAsync = ctx.queryParam("async")?.toBoolean() ?: false

        val body = ctx.body<ResetSandboxRequest>()

        // Sanity checks, TODO json schema is a more better way to check on this!
        val errorMessages = mutableListOf<String>()

        if (!body.verified) errorMessages += "Die Identität des Benutzers muss überprüft werden"

        if (errorMessages.isNotEmpty()) {
            logger.warn { "Will not reset sandbox due to errors: $errorMessages" }

            ctx.status(400)
            ctx.json(errorMessages)
        } else {
            val resetSandboxService = ResetSandbox(
                onc,
                openProject,
                backgroundJobManager
            )

            logger.info { "About to reset sandbox" }

            try {
                if (launchAsync) {
                    logger.info { "Create the new group as a background job" }

                    val jobFunction: suspend (UUID, String) -> Unit = { jobId: UUID, _: String -> resetSandboxService.resetSandbox(jobId) }
                    val newJob = backgroundJobManager.runBackgroundJob(jobFunction, "")

                    ctx.status(201)
                    ctx.json(newJob)
                } else {
                    runBlocking { resetSandboxService.resetSandbox(null) }
                    ctx.status(201)
                }
            } catch (e: Exception) {
                logger.warn(e) { "Failed to reset sandbox due to exception" }

                errorMessages += "Interner Fehler (Benachrichtige die IT)"
                ctx.status(400)
                ctx.json(errorMessages)
                return
            }

            ctx.status(200)
        }
    }
}
