/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.CreateGroupConfig
import dev.maximilian.feather.iog.internal.group.GetGroupDetails
import dev.maximilian.feather.iog.internal.group.plausibility.OpenProjectPlausibility
import dev.maximilian.feather.iog.internal.groupPattern.MemberAdminInterestedPattern
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.internal.tools.OpenProjectPCreator
import dev.maximilian.feather.iog.settings.GroupKind
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.multiservice.events.GroupSynchronizationEvent
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.nextcloud.groupfolders.entities.GroupFolderDetails
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.openproject.OpenProjectProject
import java.util.UUID

internal data class PreconditionResult(
    val allProjects: List<OpenProjectProject>,
    val groupFolderDetails: GroupFolderDetails?,
    val errorMessages: List<String>
)

internal class CreateTrials(
    private val credentials: ICredentialProvider,
    val backgroundJobManager: BackgroundJobManager,
    private val openProjectService: OpenProjectService,
    private val syncEvent: GroupSynchronizationEvent,
    private val onc: OPNameConfig,
    private val nc: Nextcloud,
    private val nextcloudPublicURL: String
) {
    var groups: Collection<Group> = emptyList()

    suspend fun createTrialGroups(jobId: UUID, autoRepair: Boolean): String {
        val result: String
        GetGroupDetails.logger.info { "CreateTrial::createTrialGroups fetching all groups " }
        backgroundJobManager.setJobStatus(jobId, "Fetching groups")

        val userAndGroups = credentials.getUsersAndGroups()
        groups = userAndGroups.second
        backgroundJobManager.setJobStatus(jobId, "Processing existing groups")
        GetGroupDetails.logger.info { "CreateTrial::createTrialGroups create OPC" }
        val opc = OpenProjectPCreator(openProjectService.openproject, onc, jobId, syncEvent, backgroundJobManager)
        val opp = OpenProjectPlausibility(openProjectService.openproject, credentials, opc, onc, userAndGroups.first)

        GetGroupDetails.logger.info { "CreateTrial::createTrialGroups check preconditions" }
        backgroundJobManager.setJobStatus(jobId, "Check preconditions")

        val cachedResults = checkPreconditions()
        if (!cachedResults.errorMessages.isEmpty())
            result =
                "Bei der Prüfung der Vorbedingunen für eine Autokonvertierung sind folgende Probleme aufgetreten:" +
                cachedResults.errorMessages.joinToString { " " }
        else if (autoRepair) {
            backgroundJobManager.setJobStatus(jobId, "Add trial groups")
            GetGroupDetails.logger.warn { "CreateTrial::createTrialGroups convert" }
            createTrialGroups(cachedResults, opc, opp)
            result = "Die Trial-Konvertierung wurde erfolgreich durchgeführt."
        } else result = "Vorbedingungen für eine Trial-Konvertierung sind erfüllt."
        GetGroupDetails.logger.warn { "CreateTrial::createTrialGroups result: $result" }
        return result
    }

    private suspend fun checkPreconditions(): PreconditionResult {
        val errorMessages = mutableListOf<String>()

        if (credentials.getGroupByName(LdapNames.IOG_MEMBERS) == null) errorMessages += "Gruppe '${LdapNames.IOG_MEMBERS}' existiert nicht"
        if (credentials.getGroupByName(LdapNames.INTERESTED_PEOPLE) == null) errorMessages += "Gruppe '${LdapNames.INTERESTED_PEOPLE}' existiert nicht"

        val allProjects = openProjectService.openproject.getProjects()
        groups.filter { it.name.endsWith(IogPluginConstants.interested_suffix) }.forEach {
            val groupNameWithoutPrefix = it.name.removeSuffix(IogPluginConstants.interested_suffix)
            if (credentials.getGroupByName(groupNameWithoutPrefix) == null)
                errorMessages += "Konnte Basisgruppe für $groupNameWithoutPrefix nicht findet. Gebildet aus ${it.name}"
            if (credentials.getGroupByName(groupNameWithoutPrefix + IogPluginConstants.admin_suffix) == null)
                errorMessages += "Admin Gruppe <${groupNameWithoutPrefix + IogPluginConstants.admin_suffix} existiert nicht!"

            if (!allProjects.any { it.identifier == groupNameWithoutPrefix })
                errorMessages += "Konnte kein Projekt zur Gruppe ${it.name} finden."
        }
        val gf = nc.findGroupFolder(NextcloudFolders.GroupShare)
        if (gf == null) errorMessages += "Groupfolder ${NextcloudFolders.GroupShare} existiert nicht."
        return PreconditionResult(allProjects, gf, errorMessages)
    }

    private suspend fun createTrialGroups(g: PreconditionResult, opc: OpenProjectPCreator, opPlausibility: OpenProjectPlausibility) {
        val allProjects = g.allProjects
        val gf = g.groupFolderDetails!!
        val interested = credentials.getGroupByName(LdapNames.INTERESTED_PEOPLE)!!
        val memberRole = openProjectService.openproject.getRoles().find { it.name == onc.memberRoleName }!!
        val generalTrialGroupLdap = credentials.getGroupByName(LdapNames.TRIAL_MEMBERS) ?: credentials.createGroup(
            Group(
                "", LdapNames.TRIAL_MEMBERS, "Aktive Interessierte mit temporärer Fördermitgliedschaft", null,
                emptySet(), emptySet(), setOf(interested.id), emptySet(), emptySet(), emptySet()
            )
        )
        opc.getOrCreateGeneralTrialGroup(generalTrialGroupLdap)

        val allNC = gf.groups?.mapNotNull { it.key } ?: emptyList()
        val ids = mutableListOf<String>()
        groups.filter { it.name.endsWith(IogPluginConstants.interested_suffix) }.forEach {
            val pat = MemberAdminInterestedPattern(credentials)
            val groupNameWithoutPrefix = it.name.removeSuffix(IogPluginConstants.interested_suffix)
            ids.add(groupNameWithoutPrefix)

            val opDescription = opPlausibility.getDescription(onc.credentialGroupToOpenProjectGroup(groupNameWithoutPrefix))
            val gk = if (it.name.startsWith(IogPluginConstants.rg_prefix)) GroupKind.REGIONAL_GROUP else GroupKind.PROJECT
            val trialOfProjectGroup = pat.addTrialGroup(CreateGroupConfig(groupNameWithoutPrefix, opDescription, emptySet(), gk, nextcloudPublicURL))

            opc.addTrialGroup(groupNameWithoutPrefix, trialOfProjectGroup, allProjects, memberRole)
            if (!allNC.contains(trialOfProjectGroup.name))
                nc.addGroupToGroupFolder(gf, trialOfProjectGroup.name, PermissionType.readWrite)
        }
    }
}
