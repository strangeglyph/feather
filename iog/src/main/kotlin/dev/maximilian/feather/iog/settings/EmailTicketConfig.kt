package dev.maximilian.feather.iog.settings

import dev.maximilian.feather.iog.internal.tools.ConnectionInfo

data class EmailTicketConfig(
    val connection: ConnectionInfo,
    val projectName: String,
    val checksPerHour: Long = 5,
    val maxBurst: Long = 10,
) {
    companion object {
        fun fromProperties(emailConnInfo: ConnectionInfo, opNames: OPNameConfig): EmailTicketConfig {
            return EmailTicketConfig(
                connection = emailConnInfo,
                projectName = opNames.asDkp2GroupName, // TODO verify
            )
        }
    }
}
