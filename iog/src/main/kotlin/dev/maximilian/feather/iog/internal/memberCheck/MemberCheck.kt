/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.iog.internal.memberCheck

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.iog.api.bindings.SupportMemberHashRow
import dev.maximilian.feather.iog.internal.supportmember.SupportMember
import dev.maximilian.feather.iog.internal.supportmember.SupportMemberDB
import dev.maximilian.feather.iog.internal.tools.DatabaseValidation
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.multiservice.events.UserCreationEvent
import dev.maximilian.feather.multiservice.events.UserDeletionEvent
import mu.KLogging
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.Date
import kotlin.concurrent.fixedRateTimer

internal class MemberCheck(
    private val credentials: ICredentialProvider,
    val smr: SupportMemberDB,
    private val actionController: ActionController?,
    private val getUserFunction: () -> Iterable<User>,
    hashAlgorithm: String,
    private val centralOfficeGroupName: String
) : UserCreationEvent, UserDeletionEvent {

    enum class RegistrationState {
        NO_SUPPORT_MEMBERSHIP_REQUIRED,
        REGISTERED_SUPPORT_MEMBER,
        MAIL_REGISTRATION_POSSIBLE,
        NAME_BIRTHDAY_REGISTRATION_REQUIRED,
        AUTOMATIC_UPGRADE_POSSIBLE,
        UPGRADE_CAN_BE_OFFERED
    }

    data class UserIdAndState(val userId: String, val result: RegistrationState)

    companion object : KLogging() {
        internal fun userIsBlocked(result: RegistrationState) =
            (result == MemberCheck.RegistrationState.MAIL_REGISTRATION_POSSIBLE) ||
                (result == MemberCheck.RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED)

        internal fun userCanUpgrade(result: RegistrationState) =
            (result == MemberCheck.RegistrationState.AUTOMATIC_UPGRADE_POSSIBLE) ||
                (result == MemberCheck.RegistrationState.UPGRADE_CAN_BE_OFFERED)
    }

    val databaseValidation = DatabaseValidation(hashAlgorithm)

    fun makeHash(s: String): String = databaseValidation.makeHash(s)

    init {
        if (actionController != null) {
            // Register the MemberCheck Action ...
            actionController.registerActionType(MemberCheckAction.NAME) { userId: String, payload: String ->
                MemberCheckAction(userId, RegistrationState.valueOf(payload))
            }
            // ... and periodically add the action to users which have not verified their membership state
            // Also check on application start
            fixedRateTimer(
                daemon = true,
                startAt = Date.from(Instant.now().truncatedTo(ChronoUnit.DAYS).plus(1L, ChronoUnit.DAYS)),
                period = Duration.ofDays(1).toMillis()
            ) { addActionsToUsers() }
            addActionsToUsers()
        }
    }

    private fun addActionsToUsers() {
        if (actionController == null) return
        val startTime = Instant.now()

        val users = getUserFunction()
        val usersBlocked = users.map { UserIdAndState(it.id, getRegistrationState(it)) }
            .filter { userIsBlocked(it.result) }
        val usersActionOpen = actionController.getOpenUserActions(MemberCheckAction.NAME).map { it.userId }.toSet()

        val usersNeedingAction = usersBlocked.filter { !usersActionOpen.contains(it.userId) }

        usersNeedingAction
            .map { MemberCheckAction(it.userId, it.result) }
            .forEach { actionController.insertAction(it) }

        val duration = Duration.between(startTime, Instant.now()).toMillis()
        logger.info {
            "MemberCheck::addActionsToUsers created ${usersNeedingAction.count()} new actions in $duration ms."
        }
    }

    fun getRegistrationState(user: User): RegistrationState {
        logger.debug { "MemberCheck::getRegistrationState for user ${user.displayName}" }
        var needsSupportMembership = false
        var upgradePossible = false
        var isAdminOrInCentralOffice = user.permissions.contains(Permission.ADMIN)
        user.groups.forEach {
            val groupName = credentials.getGroup(it)!!.name
            if (groupName.contains(IogPluginConstants.member_suffix) ||
                groupName.contains(IogPluginConstants.admin_suffix) ||
                groupName.contains(IogPluginConstants.kg_prefix) ||
                groupName.contains(IogPluginConstants.as_prefix)
            ) {
                needsSupportMembership = true
            }
            if (groupName.contains(IogPluginConstants.interested_suffix)) {
                upgradePossible = true
            }
            if (groupName.startsWith(centralOfficeGroupName)) {
                isAdminOrInCentralOffice = true
            }
        }

        if (isAdminOrInCentralOffice) {
            needsSupportMembership = false
            upgradePossible = false
        }

        if (needsSupportMembership) {
            var externID = smr.getExternalIDbyLDAP(user.id)
            if (externID == null) {
                externID = smr.getExternalIDbyMail(makeHash(user.mail))
                return if (externID != null) {
                    if (smr.countMailHash(makeHash(user.mail)) == 1) {
                        logger.debug { "MemberCheck::getRegistrationState user  ${user.displayName} could be registered as support member by mail." }
                        RegistrationState.MAIL_REGISTRATION_POSSIBLE
                    } else {
                        logger.debug { "MemberCheck::getRegistrationState user  ${user.displayName} has same mail hash as other user. Manual registration necessary" }
                        RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED
                    }
                } else {
                    logger.debug { "MemberCheck::getRegistrationState ${user.displayName} with LDAP ID ${user.id} could not be automatically registered as support member by mail. FAILED" }
                    RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED
                }
            } else {
                logger.debug { "MemberCheck::getRegistrationState already registered" }
                return RegistrationState.REGISTERED_SUPPORT_MEMBER
            }
        } else {
            val externalId = smr.getExternalIDbyMail(makeHash(user.mail))
            return if (!isAdminOrInCentralOffice && (externalId != null)) {
                logger.debug { "MemberCheck::getRegistrationState ${user.displayName} could be upgraded." }
                RegistrationState.AUTOMATIC_UPGRADE_POSSIBLE
            } else if (!isAdminOrInCentralOffice && upgradePossible) {
                logger.debug { "MemberCheck::getRegistrationState ${user.displayName} can choose to upgrade." }
                RegistrationState.UPGRADE_CAN_BE_OFFERED
            } else {
                logger.debug { "MemberCheck::getRegistrationState ${user.displayName} does not need support membership." }
                RegistrationState.NO_SUPPORT_MEMBERSHIP_REQUIRED
            }
        }
    }

    internal fun registerByMail(user: User): Boolean {
        logger.debug { "MemberCheck::registerByMail for user ${user.displayName}" }
        val state = getRegistrationState(user)
        if (state == RegistrationState.MAIL_REGISTRATION_POSSIBLE) {
            val externID = smr.getExternalIDbyMail(makeHash(user.mail))
            smr.storeLDAPandExternalID(user.id, externID!!).apply {
                actionController?.deleteActionsByNameAndUser(MemberCheckAction.NAME, user)
            }
            return true
        } else {
            logger.debug { "MemberCheck::registerByMail for user ${user.displayName} not possible. State is $state" }
            return false
        }
    }

    internal fun registerByNameAndBirthdate(nameBirthdateHash: String, ldapID: String): Boolean {
        val externalID = smr.getExternalIDbyNameHash(nameBirthdateHash)
        return if (externalID != null) {
            smr.storeLDAPandExternalID(ldapID, externalID).apply {
                credentials.getUser(ldapID)?.let {
                    actionController?.deleteActionsByNameAndUser(MemberCheckAction.NAME, it)
                }
            }
            logger.info { "MemberCheck::registerByNameAndBirthdate successfull for user with LDAP ID $ldapID" }
            true
        } else {
            logger.info { "MemberCheck::registerByNameAndBirthdate for ldap id $ldapID failed." }
            false
        }
    }

    internal fun downgrade(ldapID: String) {
        val user = credentials.getUser(ldapID)
        if (user != null) {
            logger.info { "MemberCheck::downgrade for user ${user.displayName}" }
            val groupsWithoutKGAS = mutableListOf<String>()
            user.groups.forEach {
                val groupName = credentials.getGroup(it)!!.name
                if (!groupName.startsWith(IogPluginConstants.kg_prefix) && !groupName.startsWith(
                        IogPluginConstants.as_prefix
                    )
                ) {
                    groupsWithoutKGAS.add(it)
                    logger.info { "MemberCheck::downgrade remove user from group $groupName" }
                }
            }

            val groupsWithReplacedMember = mutableListOf<String>()
            groupsWithoutKGAS.forEach {
                val group = credentials.getGroup(it)!!
                val groupName = group.name
                var interestedGroupName = ""
                if (groupName.contains(IogPluginConstants.admin_suffix))
                    interestedGroupName =
                        groupName.replace(IogPluginConstants.admin_suffix, IogPluginConstants.interested_suffix)
                else if (groupName.contains(IogPluginConstants.member_suffix))
                    interestedGroupName =
                        groupName.replace(IogPluginConstants.member_suffix, IogPluginConstants.interested_suffix)

                if (interestedGroupName != "") {
                    logger.info { "MemberCheck::downgrade replace membership $groupName with $interestedGroupName" }
                    groupsWithReplacedMember.add(credentials.getGroups().first { it.name == interestedGroupName }.id)
                } else {
                    groupsWithReplacedMember.add(it)
                    logger.info { "MemberCheck::downgrade keep group membership $groupName" }
                }
            }
            credentials.updateUser(user.copy(groups = groupsWithReplacedMember.toSet()))

            // now that the user was downgraded, we can remove any blocking member-check actions
            actionController?.deleteActionsByNameAndUser(MemberCheckAction.NAME, user)

            logger.info { "MemberCheck::downgrade done" }
        } else logger.warn { "MemberCheck::downgrade could not find user for ldap id $ldapID" }
    }

    internal fun upgrade(ldapID: String) {
        val user = credentials.getUser(ldapID)
        if (user != null) {
            logger.info { "MemberCheck::upgrade Try to upgrade ${user.displayName}" }
            var groupsWithReplacedMember = mutableListOf<String>()
            user.groups.forEach {
                val group = credentials.getGroup(it)!!
                val groupName = group.name
                if (groupName.contains(IogPluginConstants.interested_suffix)) {
                    val groupWithMemberName =
                        groupName.replace(IogPluginConstants.interested_suffix, IogPluginConstants.member_suffix)
                    logger.info { "MemberCheck::upgrade replace $groupName by $groupWithMemberName}" }
                    groupsWithReplacedMember.add(credentials.getGroups().first { it.name == groupWithMemberName }.id)
                } else groupsWithReplacedMember.add(it)
            }
            credentials.updateUser(user.copy(groups = groupsWithReplacedMember.toSet()))
            logger.info { "MemberCheck::upgrade done" }
        } else logger.warn { "MemberCheck::upgrade could not find user for ldap id $ldapID" }
    }

    internal fun checkAndUpgrade(ldapID: String): Boolean {
        val user = credentials.getUser(ldapID)
        if (user != null) {
            logger.info { "MemberCheck::checkAndUpgrade Try to upgrade user (${user.displayName})" }
            var externalID = smr.getExternalIDbyLDAP(ldapID)
            if (externalID != null) logger.info { "MemberCheck::checkAndUpgrade user was already registered." }
            else {
                logger.info { "MemberCheck::checkAndUpgrade try to register ${user.displayName} by mail." }
                externalID = smr.getExternalIDbyMail(makeHash(user.mail))
                if (externalID != null) {
                    smr.storeLDAPandExternalID(ldapID, externalID)
                    upgrade(ldapID)
                    logger.info { "MemberCheck::checkAndUpgrade Upgrade done" }
                    return true
                } else {
                    logger.info { "MemberCheck::checkAndUpgrade could not find mail in db, auto registration impossible." }
                    return false
                }
            }
        } else logger.warn { "MemberCheck::checkAndUpgrade could not find user for ldap id $ldapID" }
        return false
    }

    internal fun updateDatabase(hashEntries: List<SupportMemberHashRow>) {
        val newIDs = hashEntries.map { it.externalID }
        val registeredIDs = smr.getAllRegisteredExternalID()
        registeredIDs.forEach { if (!newIDs.contains(it)) smr.deleteSupportMembership(it) }
        val supportMemberIDs = smr.getAllSupportMemberExternalID()
        supportMemberIDs.forEach { if (!newIDs.contains(it)) smr.deleteSupportMembership(it) }
        hashEntries.forEach { smr.storeHash(it.externalID, it.nameBirthdayHash, it.mailHash1, it.mailHash2) }
    }

    internal fun getAllRegisteredSupportMember(): List<RegisteredSupportMember> {
        var q = mutableListOf<RegisteredSupportMember>()
        val ldapIDs = smr.getAllRegistered()
        ldapIDs.forEach {
            val user = credentials.getUser(it.first)
            if (user == null) {
                logger.warn { "MemberCheck::getAllRegisteredSupportMember could not find user with ldapID=${it.first}, although registered as support member. Forgot to delete?" }
                q.add(RegisteredSupportMember("", "", it.first, it.second))
            } else {
                q.add(RegisteredSupportMember(user.firstname, user.surname, it.first, it.second))
            }
        }
        return q
    }

    internal fun getAllSupportMembersOfFundraisingBox(): List<SupportMember> {
        return smr.getAllSupportMember()
    }

    internal fun getSimulation(): List<SimulatedSupportMember> {
        val user = credentials.getUsers()
        val userWithAutoregisterableMails = mutableListOf<SimulatedSupportMember>()

        user.forEach {
            val state = getRegistrationState(it)
            if (state == RegistrationState.MAIL_REGISTRATION_POSSIBLE ||
                state == RegistrationState.AUTOMATIC_UPGRADE_POSSIBLE
            ) {
                val externalID = smr.getExternalIDbyMail(makeHash(it.mail))
                userWithAutoregisterableMails.add(
                    SimulatedSupportMember(
                        it.firstname,
                        it.surname,
                        it.id,
                        externalID,
                        state
                    )
                )
            } else if (state == RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED) {
                userWithAutoregisterableMails.add(
                    SimulatedSupportMember(
                        it.firstname,
                        it.surname,
                        it.id,
                        null,
                        RegistrationState.NAME_BIRTHDAY_REGISTRATION_REQUIRED
                    )
                )
            }
        }
        return userWithAutoregisterableMails
    }

    private fun deleteFromSupportMemberRegistration(user: User) {
        val state = getRegistrationState(user)
        if (state == RegistrationState.REGISTERED_SUPPORT_MEMBER) {
            smr.deleteUserRegistration(user.id)
            logger.info { "MemberCheck::deleteFromSupportMemberRegistration user ${user.displayName} with ldapID=${user.id} deleted from support member registration database" }
        } else logger.debug { "MemberCheck::deleteFromSupportMemberRegistration did not find user ${user.displayName} with ldapID ${user.id} in support member registration." }
    }

    override fun userCreated(user: User) {
        val result = getRegistrationState(user)
        if (userIsBlocked(result)) {
            actionController?.insertAction(MemberCheckAction(user.id, result))
        }
    }

    override fun userDeleted(user: User) {
        deleteFromSupportMemberRegistration(user)
        actionController?.deleteActionsByNameAndUser(MemberCheckAction.NAME, user)
    }
}
