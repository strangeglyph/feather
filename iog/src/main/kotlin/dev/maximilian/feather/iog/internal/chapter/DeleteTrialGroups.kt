package dev.maximilian.feather.iog.internal.chapter

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.iog.internal.group.GetGroupDetails
import dev.maximilian.feather.iog.internal.settings.LdapNames
import dev.maximilian.feather.iog.internal.settings.NextcloudFolders
import dev.maximilian.feather.iog.settings.IogPluginConstants
import dev.maximilian.feather.iog.settings.OPNameConfig
import dev.maximilian.feather.multiservice.BackgroundJobManager
import dev.maximilian.feather.nextcloud.Nextcloud
import dev.maximilian.feather.openproject.IOpenProject
import java.util.UUID

class DeleteTrialGroups(
    private val credentials: ICredentialProvider,
    private val op: IOpenProject,
    private val backgroundJobManager: BackgroundJobManager,
    private val nc: Nextcloud,
    private val onc: OPNameConfig
) {
    suspend fun delete(jobId: UUID, param: Boolean) {
        GetGroupDetails.logger.warn { "DeleteTrial::delete remove all -trial groups " }
        backgroundJobManager.setJobStatus(jobId, "remove all -trial groups")

        val nct = nc.findGroupFolder(NextcloudFolders.GroupShare) ?: throw Exception("Could not find IOG share in NC")
        val trialGroups = credentials.getGroups().filter { it.name.endsWith(IogPluginConstants.trial_suffix) }
        val allNames = nc.findGroupFolder(NextcloudFolders.GroupShare)?.groups?.mapNotNull { it.key } ?: emptyList()
        trialGroups.forEach {
            credentials.deleteGroup(it)
        }
        op.getGroups().filter { it.name.endsWith(IogPluginConstants.trial_suffix) }.forEach { op.deleteGroup(it) }

        allNames.filter { it.endsWith(IogPluginConstants.trial_suffix) }.forEach { nc.removeGroupFromGroupFolder(nct, it) }

        GetGroupDetails.logger.warn { "DeleteTrial::delete remove TrialMember group " }
        backgroundJobManager.setJobStatus(jobId, "remove all TrialMember group")

        if (allNames.contains(LdapNames.TRIAL_MEMBERS))
            nc.removeGroupFromGroupFolder(nct, LdapNames.TRIAL_MEMBERS)
        credentials.getGroupByName(LdapNames.TRIAL_MEMBERS)?.let { credentials.deleteGroup(it) }
        op.getGroupByName(onc.trialMemberGroupName)?.let { op.deleteGroup(it) }

        GetGroupDetails.logger.warn { "DeleteTrial::delete finished." }
        backgroundJobManager.setJobStatus(jobId, "Finished.")
    }
}
