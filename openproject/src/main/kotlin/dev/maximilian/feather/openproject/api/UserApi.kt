/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject.api

import dev.maximilian.feather.openproject.OpenProjectUser
import dev.maximilian.feather.openproject.retryWithExponentialBackoff
import io.ktor.client.HttpClient
import io.ktor.client.request.delete
import io.ktor.client.request.get
import io.ktor.client.request.patch
import io.ktor.client.request.post
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.Duration

public interface IOpenProjectUserApi {
    public suspend fun getUsers(): List<OpenProjectUser>
    public suspend fun getUser(userId: Int): OpenProjectUser?
    public suspend fun getUserByLogin(login: String): OpenProjectUser?
    public suspend fun getUserByEmail(email: String): OpenProjectUser?

    public suspend fun getMe(): OpenProjectUser

    public suspend fun createUser(user: OpenProjectUser, password: String): OpenProjectUser
    public suspend fun createAuthSourceUser(user: OpenProjectUser, authSource: Int): OpenProjectUser

    public suspend fun changeName(
        user: OpenProjectUser,
        newFirstname: String,
        newSurname: String
    ): OpenProjectUser

    public suspend fun changeMailAddress(user: OpenProjectUser, newMailAddress: String): OpenProjectUser

    public suspend fun deleteUser(user: OpenProjectUser)

    public suspend fun lockUser(user: OpenProjectUser)
    public suspend fun unlockUser(user: OpenProjectUser)
}

internal class UserApi(
    private val client: HttpClient,
    private val baseUrl: String
) : IOpenProjectUserApi {
    companion object {
        internal fun userUrl(baseUrl: String, id: Int? = null) = "$baseUrl/api/v3/users" + (id?.let { "/$it" } ?: "")

        private fun loginFilter(login: String) = buildFilter(setOf(Triple("login", "=", setOf(login))))
        // private fun mailFilter(mail: String) = buildFilter(setOf(Triple("name", "=", setOf(mail))))
    }

    override suspend fun getUsers(): List<OpenProjectUser> =
        client.getAllPages<UserAnswer>(userUrl(baseUrl)).toOpenProjectEntity()

    override suspend fun getUser(userId: Int): OpenProjectUser? =
        client.getOrNull<UserAnswer>(userUrl(baseUrl, userId))?.toOpenProjectEntity()

    override suspend fun getUserByLogin(login: String): OpenProjectUser? =
        client.getAllPages<UserAnswer>("${userUrl(baseUrl)}/?filters=${loginFilter(login)}")
            .firstOrNull()
            ?.toOpenProjectEntity()

    // Filtering mail seems to not work, instead we filter manually
    override suspend fun getUserByEmail(email: String): OpenProjectUser? =
        getUsers().firstOrNull { it.email == email }

    override suspend fun getMe(): OpenProjectUser =
        client.get<UserAnswer>(userUrl(baseUrl) + "/me").toOpenProjectEntity()

    override suspend fun createUser(user: OpenProjectUser, password: String): OpenProjectUser =
        createUserImpl(user, password, null)

    override suspend fun createAuthSourceUser(user: OpenProjectUser, authSource: Int): OpenProjectUser =
        createUserImpl(user, null, authSource)

    private suspend fun createUserImpl(
        user: OpenProjectUser,
        password: String? = null,
        authSource: Int? = null
    ): OpenProjectUser = client.post<UserAnswer>(userUrl(baseUrl)) {
        body = CreateUserRequest(
            login = user.login,
            firstName = user.firstName,
            lastName = user.lastName,
            email = user.email,
            admin = user.admin,
            status = user.status,
            password = password,
            links = authSource?.let {
                CreateUserRequestLinks(HrefAnswer("/api/v3/auth_sources/$it"))
            }
        )
    }.toOpenProjectEntity()

    override suspend fun changeMailAddress(user: OpenProjectUser, newMailAddress: String) =
        changeUserImpl(user, email = newMailAddress)

    override suspend fun changeName(
        user: OpenProjectUser,
        newFirstname: String,
        newSurname: String
    ) =
        changeUserImpl(user, firstName = newFirstname, lastName = newSurname)

    private suspend fun changeUserImpl(
        user: OpenProjectUser,
        login: String? = null,
        firstName: String? = null,
        lastName: String? = null,
        email: String? = null,
        admin: Boolean? = null
    ): OpenProjectUser = client.patch<UserAnswer>(userUrl(baseUrl, user.id)) {
        body = ChangeUserRequest(
            login = if (login != null && login != user.login) { login } else { null },
            firstName = if (firstName != null && firstName != user.firstName) { firstName } else { null },
            lastName = if (lastName != null && lastName != user.lastName) { lastName } else { null },
            email = if (email != null && email != user.email) { email } else { null },
            admin = if (admin != null && admin != user.admin) { admin } else { null },
        )
    }.toOpenProjectEntity()

    override suspend fun deleteUser(user: OpenProjectUser) {
        client.delete<Unit>(userUrl(baseUrl, user.id)) {
            // Workaround for https://youtrack.jetbrains.com/issue/KTOR-1407
            // Also why OpenProject expects a content-type here?
            body = ""
        }

        val waitingFunction: suspend() -> Unit = {
            require(getUser(user.id) == null) { "User deletion exceeds time limit" }
        }

        waitingFunction.retryWithExponentialBackoff(6, Duration.ofSeconds(180))
    }

    override suspend fun lockUser(user: OpenProjectUser) {
        client.post<Unit>("${userUrl(baseUrl, user.id)}/lock") {
            // Workaround for https://youtrack.jetbrains.com/issue/KTOR-1407
            // Also why OpenProject expects a content-type here?
            body = ""
        }
    }

    override suspend fun unlockUser(user: OpenProjectUser) {
        client.delete<Unit>("${userUrl(baseUrl, user.id)}/lock") {
            // Workaround for https://youtrack.jetbrains.com/issue/KTOR-1407
            // Also why OpenProject expects a content-type here?
            body = ""
        }
    }
}

@Serializable
private data class CreateUserRequest(
    val login: String,
    val firstName: String,
    val lastName: String,
    val email: String?,
    val admin: Boolean,
    val status: String,
    val password: String? = null,
    @SerialName("_links")
    val links: CreateUserRequestLinks? = null
)

@Serializable
private data class CreateUserRequestLinks(
    @SerialName("auth_source")
    val authSource: HrefAnswer
)

@Serializable
internal data class UserAnswer(
    val id: Int,
    val login: String,
    val firstName: String,
    val lastName: String,
    val email: String? = null,
    val admin: Boolean,
    val status: String,
    val language: String,
    @SerialName("_links")
    val links: UserAnswerLinks
)

@Serializable
internal data class UserAnswerLinks(
    @SerialName("auth_source")
    val authSource: UserAnswerAuthSource? = null
)

@Serializable
internal data class UserAnswerAuthSource(
    val href: String
)

internal fun UserAnswer.toOpenProjectEntity() = OpenProjectUser(
    id = id,
    login = login,
    firstName = firstName,
    lastName = lastName,
    email = email,
    admin = admin,
    status = status,
    authSource = links.authSource?.href?.idFromURL() ?: 0
)

internal fun Iterable<UserAnswer>.toOpenProjectEntity() = this.map { it.toOpenProjectEntity() }

@Serializable
private data class ChangeUserRequest(
    val login: String? = null,
    val email: String? = null,
    val firstName: String? = null,
    val lastName: String? = null,
    val admin: Boolean? = null,
    val language: String? = null
)
