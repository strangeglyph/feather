/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject.api

import dev.maximilian.feather.openproject.OpenProjectStatus
import io.ktor.client.HttpClient
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

public interface IOpenProjectStatusApi {
    public suspend fun getStatus(): List<OpenProjectStatus>
}

internal class StatusApi(
    private val client: HttpClient,
    baseURL: String
) : IOpenProjectStatusApi {
    private val reqUrl = "$baseURL/api/v3/statuses"

    override suspend fun getStatus(): List<OpenProjectStatus> =
        client.getAllPages<StatusAnswer>(reqUrl).toOpenProjectEntity()
}

@Serializable
private data class StatusAnswer(
    val id: Int,
    val name: String,
    @SerialName("isClosed")
    val closed: Boolean
)

private fun StatusAnswer.toOpenProjectEntity() = OpenProjectStatus(
    id = id,
    name = name,
    closed = closed
)

private fun Iterable<StatusAnswer>.toOpenProjectEntity() = map { it.toOpenProjectEntity() }
