/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject.api

import dev.maximilian.feather.openproject.IdLazyEntity
import dev.maximilian.feather.openproject.OpenProjectCloneFields
import dev.maximilian.feather.openproject.OpenProjectDescription
import dev.maximilian.feather.openproject.OpenProjectJobStatus
import dev.maximilian.feather.openproject.OpenProjectJobStatusStatus
import dev.maximilian.feather.openproject.OpenProjectProject
import dev.maximilian.feather.openproject.retryWithExponentialBackoff
import io.ktor.client.HttpClient
import io.ktor.client.features.RedirectResponseException
import io.ktor.client.request.delete
import io.ktor.client.request.post
import io.ktor.http.HttpStatusCode
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.time.Duration

public interface IOpenProjectProjectApi {
    public suspend fun cloneProject(
        toClone: OpenProjectProject,
        cloneName: String,
        cloneDescription: String?,
        fieldsToBeCloned: List<OpenProjectCloneFields>,
        sendNotifications: Boolean = false,
        newParent: OpenProjectProject? = null
    ): OpenProjectProject

    public suspend fun createProject(project: OpenProjectProject): OpenProjectProject
    public suspend fun getProjects(): List<OpenProjectProject>
    public suspend fun getProject(id: Int): OpenProjectProject?
    public suspend fun getProjectByIdentifierOrName(projectName: String): OpenProjectProject?
    public suspend fun deleteProject(project: OpenProjectProject)
}

internal class ProjectApi(
    private val client: HttpClient,
    private val baseUrl: String,
    private val statusApi: IOpenProjectJobStatusApi
) : IOpenProjectProjectApi {
    companion object {
        internal fun projectUrl(baseUrl: String, id: Int? = null) =
            "$baseUrl/api/v3/projects" + (id?.let { "/$it" } ?: "")

        private fun idOrNameFilter(idOrName: String) =
            buildFilter(setOf(Triple("name_and_identifier", "=", setOf(idOrName))))
    }

    override suspend fun cloneProject(
        toClone: OpenProjectProject,
        cloneName: String,
        cloneDescription: String?,
        fieldsToBeCloned: List<OpenProjectCloneFields>,
        sendNotifications: Boolean,
        newParent: OpenProjectProject?
    ): OpenProjectProject {
        try {
            client.post<Unit>("${projectUrl(baseUrl, toClone.id)}/copy") {
                body = CloneRequestEntity(
                    name = cloneName,
                    public = false,
                    links = CreateProjectEntityLinks(
                        parent = HrefAnswer(newParent?.id?.let { projectUrl(baseUrl, it) })
                    ),
                    meta = CloneMeta(
                        copyBoards = fieldsToBeCloned.contains(OpenProjectCloneFields.BOARDS),
                        copyCategories = fieldsToBeCloned.contains(OpenProjectCloneFields.CATEGORIES),
                        copyForums = fieldsToBeCloned.contains(OpenProjectCloneFields.FORUMS),
                        copyMembers = fieldsToBeCloned.contains(OpenProjectCloneFields.MEMBERS),
                        copyOverview = fieldsToBeCloned.contains(OpenProjectCloneFields.OVERVIEW),
                        copyQueries = fieldsToBeCloned.contains(OpenProjectCloneFields.QUERIES),
                        copyVersions = fieldsToBeCloned.contains(OpenProjectCloneFields.VERSIONS),
                        copyWiki = fieldsToBeCloned.contains(OpenProjectCloneFields.WIKI),
                        copyWikiPageAttachments = fieldsToBeCloned.contains(OpenProjectCloneFields.WIKI_PAGE_ATTACHMENTS),
                        copyWorkPackageAttachments = fieldsToBeCloned.contains(OpenProjectCloneFields.WORK_PACKAGE_ATTACHMENTS),
                        copyWorkPackages = fieldsToBeCloned.contains(OpenProjectCloneFields.WORK_PACKAGES),
                        sendNotifications = false
                    )
                )
            }

            throw IllegalStateException("Project copy should always redirect or throw")
        } catch (e: RedirectResponseException) {
            require(e.response.status == HttpStatusCode.SeeOther) { "Copy project endpoint should return 303 SEE OTHER, but was ${e.response.status}" }
            val redirectUri = e.response.headers["Location"]

            requireNotNull(redirectUri) { "OpenProject redirects without a location header" }

            val jobStatusId = redirectUri.pathParts().last()

            val waitingFunction: suspend () -> OpenProjectJobStatus<OpenProjectProject>? = {
                val jobStatus = statusApi.getJob(jobStatusId) { getProjectByIdentifierOrName(it.toString()) }

                if (jobStatus != null) {
                    require(
                        jobStatus.status != OpenProjectJobStatusStatus.IN_QUEUE && jobStatus.status != OpenProjectJobStatusStatus.IN_PROCESS
                    ) { "JobStatus is IN_QUEUE or IN_PROCESS for longer than 3 minutes" }
                }

                jobStatus
            }

            val resultJob = waitingFunction.retryWithExponentialBackoff(6, Duration.ofSeconds(180))

            requireNotNull(resultJob) { "The job OpenProject redirects to ($jobStatusId) does not exist" }
            require(resultJob.status == OpenProjectJobStatusStatus.SUCCESS) {
                "The copy process has a problem and state ${resultJob.status}. Some error hints: ${
                resultJob.errors.joinToString(", ", "[", "]")
                }"
            }

            require(resultJob.errors.isEmpty()) {
                "The copy job was successful, but there were errors: ${
                resultJob.errors.joinToString(", ", "[", "]")
                }"
            }

            val copiedProject = resultJob.refObject?.value

            requireNotNull(copiedProject) { "The new project with id: ${resultJob.refObject?.id ?: "UNKNOWN"} could not be found" }

            return copiedProject
        }
    }

    override suspend fun createProject(project: OpenProjectProject): OpenProjectProject {
        val answer = client.post<ProjectAnswer>(projectUrl(baseUrl)) {
            body = CreateProjectEntity(
                name = project.name,
                identifier = project.identifier,
                links = project.parent?.id?.let { CreateProjectEntityLinks(HrefAnswer(projectUrl(baseUrl, it))) },
                description = FormatRawHTMLEntity(
                    format = project.description.format,
                    raw = project.description.raw,
                    html = project.description.html
                )
            )
        }

        // Don't touch or lazy load any dependencies, as this is not necessary to perform this operation
        return project.copy(id = answer.id)
    }

    override suspend fun getProject(id: Int): OpenProjectProject? =
        client.getOrNull<ProjectAnswer>(projectUrl(baseUrl, id))?.toOpenProjectEntity(mutableMapOf())

    override suspend fun getProjects(): List<OpenProjectProject> {
        val answer = client.getAllPages<ProjectAnswer>(projectUrl(baseUrl))
        val lazyMap = mutableMapOf<Int, IdLazyEntity<OpenProjectProject?>>()

        return answer.map { it.toOpenProjectEntity(lazyMap) }
    }

    override suspend fun getProjectByIdentifierOrName(projectName: String): OpenProjectProject? =
        client.getAllPages<ProjectAnswer>("${projectUrl(baseUrl)}?filters=${idOrNameFilter(projectName)}").firstOrNull()
            ?.toOpenProjectEntity(mutableMapOf())

    override suspend fun deleteProject(project: OpenProjectProject) {
        client.delete<Unit>(projectUrl(baseUrl, project.id)) {
            // Workaround for https://youtrack.jetbrains.com/issue/KTOR-1407
            // Also why OpenProject expects a content-type here?
            body = ""
        }

        val waitingFunction: suspend () -> Unit = {
            require(getProject(project.id) == null) { "Project deletion exceeds time limit" }
        }

        waitingFunction.retryWithExponentialBackoff(6, Duration.ofSeconds(180))
    }

    private fun ProjectAnswer.toOpenProjectEntity(
        lazyParentMap: MutableMap<Int, IdLazyEntity<OpenProjectProject?>>
    ): OpenProjectProject = OpenProjectProject(
        id = id,
        name = name,
        identifier = identifier,
        description = OpenProjectDescription(
            html = description.html,
            format = description.format,
            raw = description.raw ?: ""
        ),
        parent = links.parent?.href?.idFromURL()
            ?.let { lazyParentMap.getOrPut(it) { IdLazyEntity(it) { runBlocking { getProject(it) } } } }
    )
}

@Serializable
private data class CreateProjectEntity(
    val identifier: String,
    val name: String,
    @SerialName("_links")
    val links: CreateProjectEntityLinks? = null,
    val description: FormatRawHTMLEntity
)

@Serializable
private data class CreateProjectEntityLinks(
    val parent: HrefAnswer
)

@Serializable
private data class ProjectAnswer(
    val id: Int,
    val identifier: String,
    val name: String,
    val description: FormatRawHTMLEntity,
    val active: Boolean,
    val public: Boolean,
    val statusExplanation: FormatRawHTMLEntity,
    val createdAt: String,
    val updatedAt: String,
    @SerialName("_links")
    val links: ProjectLinks
)

@Serializable
private data class ProjectLinks(
    val parent: HrefAnswer? = null
)

@Serializable
private data class CloneRequestEntity(
    val name: String,
    @SerialName("_meta")
    val meta: CloneMeta,
    @SerialName("_links")
    val links: CreateProjectEntityLinks,
    val public: Boolean
)

@Serializable
private data class CloneMeta(
    val copyBoards: Boolean,
    val copyCategories: Boolean,
    val copyForums: Boolean,
    val copyMembers: Boolean,
    val copyOverview: Boolean,
    val copyQueries: Boolean,
    val copyVersions: Boolean,
    val copyWiki: Boolean,
    val copyWikiPageAttachments: Boolean,
    val copyWorkPackageAttachments: Boolean,
    val copyWorkPackages: Boolean,
    val sendNotifications: Boolean
)
