/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.openproject

import dev.maximilian.feather.openproject.api.IOpenProjectMembershipApi
import dev.maximilian.feather.openproject.api.IOpenProjectProjectApi
import dev.maximilian.feather.openproject.api.IOpenProjectRoleApi
import dev.maximilian.feather.openproject.api.IOpenProjectStatusApi
import dev.maximilian.feather.openproject.api.IOpenProjectUserApi
import dev.maximilian.feather.openproject.api.IOpenProjectWorkPackageApi
import dev.maximilian.feather.openproject.api.WorkPackageApi
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import java.lang.IllegalArgumentException
import kotlin.test.Test
import kotlin.test.assertEquals

class WorkPackageTest {
    private val api: IOpenProjectWorkPackageApi = TestUtil.openProject
    private val userApi: IOpenProjectUserApi = TestUtil.openProject
    private val projectApi: IOpenProjectProjectApi = TestUtil.openProject
    private val statusApi: IOpenProjectStatusApi = TestUtil.openProject
    private val membershipApi: IOpenProjectMembershipApi = TestUtil.openProject
    private val roleApi: IOpenProjectRoleApi = TestUtil.openProject

    @Test
    fun `WorkPackages by user returns 33 packages where admin is assignee`() {
        runBlocking {
            val op = TestUtil.openProject as OpenProject
            val user = userApi.createUser(generateTestUser(), "test123456")
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val role = roleApi.getRoles().first { it.name == "Project admin" }
            membershipApi.createMembership(OpenProjectMembership(0, project, user, listOf(role)))
            val workPackage =
                api.getWorkPackagesOfProject(project, statusApi.getStatus(), false).minByOrNull { it.id }!!
            WorkPackageApi(op.apiClient, op.baseUrl).assignUserToWorkPackage(workPackage, user)
            val packages = api.getWorkPackagesOfUser(user, emptyList()).toSet()

            assertEquals(setOf(workPackage.id), packages.map { it.id }.toSet())
        }
    }

    @Test
    fun `WorkPackages Api ignores OpenProject bug on user with no memberships`() {
        val user = runBlocking { userApi.createUser(generateTestUser(), "test123456") }
        val workPackages = assertDoesNotThrow { runBlocking { api.getWorkPackagesOfUser(user, emptyList()) } }

        assertEquals(emptyList(), workPackages)
    }

    @Test
    fun `WorkPackages by project returns 9 open packages for demo project`() {
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val workPackages = api.getWorkPackagesOfProject(project, statusApi.getStatus(), false)

            assertEquals(9, workPackages.size, "Expected 9 open work packages to be part of demo project")
        }
    }

    @Test
    fun `WorkPackages by project returns 10 open and closed packages for demo project`() {
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val workPackages = api.getWorkPackagesOfProject(project, statusApi.getStatus(), true)

            assertEquals(10, workPackages.size, "Expected 10 open and closed work packages to be part of demo project")
        }
    }

    @Test
    fun `createWorkPackage creates work package as expected`() {
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val workPackage = api.createWorkPackage(
                "Test package",
                "This is **test** content",
                project,
                OpenProjectWorkPackageType.TASK
            )

            assertEquals("Test package", workPackage.subject)
            // TODO too brittle?
            assertEquals("<p class=\"op-uc-p\">This is <strong>test</strong> content</p>", workPackage.description.html)
        }
    }

    @Test
    fun `createWorkPackage rejects empty subjects`() {
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            assertThrows<IllegalArgumentException> {
                api.createWorkPackage("", "", project, OpenProjectWorkPackageType.TASK)
            }
        }
    }

    @Test
    fun `createWorkPackage truncates subjects with length over 255 characters`() {
        // Maximum fitting subject length
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val subject =
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas dictum leo eget accumsan " +
                    "dapibus. Proin finibus ipsum mi, ut consectetur massa vehicula eu. Vivamus " +
                    "urna dui, dignissim nec turpis nec, vestibulum vestibulum nunc. Quisque blandit placerat."

            assertEquals(subject, api.createWorkPackage(subject, "", project, OpenProjectWorkPackageType.TASK).subject)
        }

        // Exactly over the subject length
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val subject =
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas dictum leo eget accumsan " +
                    "dapibus. Proin finibus ipsum mi, ut consectetur massa vehicula eu. Vivamus " +
                    "urna dui, dignissim nec turpis nec, vestibulum vestibulum nunc. Quisquem blandit placerat."
            val truncatedSubject =
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas dictum leo eget accumsan " +
                    "dapibus. Proin finibus ipsum mi, ut consectetur massa vehicula eu. Vivamus " +
                    "urna dui, dignissim nec turpis nec, vestibulum vestibulum nunc. Quisquem blandit place..."

            assertEquals(
                truncatedSubject,
                api.createWorkPackage(subject, "", project, OpenProjectWorkPackageType.TASK).subject
            )
        }

        // Significantly over the subject length
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val subject = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ullamcorper metus sapien, " +
                "nec convallis justo accumsan ut. Donec sed dapibus ligula, ut varius arcu. " +
                "Curabitur vel sodales sapien. Class aptent taciti sociosqu ad litora torquent per conubia nostra, " +
                "per inceptos himenaeos. Etiam quam at."
            val truncatedSubject =
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ullamcorper metus sapien, " +
                    "nec convallis justo accumsan ut. Donec sed dapibus ligula, ut varius arcu. " +
                    "Curabitur vel sodales sapien. Class aptent taciti sociosqu ad litora torquent per conubi..."

            assertEquals(
                truncatedSubject,
                api.createWorkPackage(subject, "", project, OpenProjectWorkPackageType.TASK).subject
            )
        }
    }

    @Test
    fun `createWorkPackage with unsupported type BUG fails in demo project`() {
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            assertThrows<IllegalArgumentException> {
                api.createWorkPackage("Test package", "", project, OpenProjectWorkPackageType.BUG)
            }
        }
    }

    @Test
    fun `createWorkPackage with non-existent project fails`() {
        runBlocking {
            val project = generateTestProject(id = 10)
            assertThrows<IllegalArgumentException> {
                api.createWorkPackage("Test package", "", project, OpenProjectWorkPackageType.TASK)
            }
        }
    }

    @Test
    fun `assignWatcher succeeds`() {
        runBlocking {
            val project = projectApi.getProjectByIdentifierOrName(DEMO_PROJECT.identifier)!!
            val wp = api.createWorkPackage(
                "Test package",
                "This is **test** content",
                project,
                OpenProjectWorkPackageType.TASK
            )
            val user = userApi.getUser(1)!!
            api.assignWatcher(wp, user)
        }
    }
}
