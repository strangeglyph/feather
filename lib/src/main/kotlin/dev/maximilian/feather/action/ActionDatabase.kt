package dev.maximilian.feather.action

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction

internal class ActionDatabase(private val db: Database) {
    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(ActionTable)
        }
    }

    internal fun insertAction(action: Action<*>) {
        transaction(db) {
            ActionTable.insert {
                it[this.userId] = action.userId
                it[this.actionName] = action.name
                it[this.payload] = action.payloadToString()
            }
        }
    }

    internal fun getActionsByUser(userId: String, producer: (String, String, String) -> Action<*>) = transaction(db) {
        ActionTable.select { ActionTable.userId eq userId }.map {
            producer(it[ActionTable.actionName], it[ActionTable.userId], it[ActionTable.payload])
        }
    }

    internal fun getActionsByName(actionName: String, producer: (String, String, String) -> Action<*>) = transaction(db) {
        ActionTable.select { ActionTable.actionName eq actionName }.map {
            producer(it[ActionTable.actionName], it[ActionTable.userId], it[ActionTable.payload])
        }
    }

    internal fun deleteActionsByName(actionName: String) {
        transaction(db) {
            ActionTable.deleteWhere { ActionTable.actionName eq actionName }
        }
    }

    internal fun deleteActionsByNameAndUser(actionName: String, userId: String) {
        transaction(db) {
            ActionTable.deleteWhere { ActionTable.actionName eq actionName and (ActionTable.userId eq userId) }
        }
    }

    private object ActionTable : IntIdTable("actions") {
        val userId = varchar("user_id", 50)
        val actionName = varchar("action_name", 50)
        val payload = text("payload")
    }
}
