/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.IAuthorizer
import dev.maximilian.feather.Session
import dev.maximilian.feather.User
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.UUIDTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.insertAndGetId
import org.jetbrains.exposed.sql.javatime.timestamp
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import java.time.Instant
import java.util.UUID

internal class SessionDatabase(
    private val db: Database,
    private val userFromIdFunction: (String) -> User?,
    private val authorizerMap: Map<String, IAuthorizer>
) {
    init {
        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(SessionTable, AfterActionsRedirectTable)
        }
    }

    internal fun create(session: Session): Session {
        require(session.id == EMPTY_UUID) { "Can only create a session with an empty uuid" }

        return session.copy(
            id = transaction(db) {
                SessionTable.insertAndGetId {
                    it[userId] = session.user.id
                    it[validUntil] = session.validUntil
                    it[authorizer] = session.authorizer.id
                    it[refreshToken] = session.refreshToken
                    it[accessToken] = session.accessToken
                    it[minisession] = session.miniSession
                }
            }.value
        )
    }

    internal fun getSession(id: UUID): Session? = transaction(db) {
        SessionTable.select { SessionTable.id eq id }.firstOrNull()?.let {
            val user = userFromIdFunction(it[SessionTable.userId]) ?: return@transaction null
            val authorizer =
                it[SessionTable.authorizer].let { authorizer -> authorizerMap[authorizer] } ?: return@transaction null
            Session(
                id = it[SessionTable.id].value,
                validUntil = it[SessionTable.validUntil],
                refreshToken = it[SessionTable.refreshToken],
                accessToken = it[SessionTable.accessToken],
                user = user,
                authorizer = authorizer,
                miniSession = it[SessionTable.minisession]
            )
        }
    }

    internal fun updateSession(session: Session): Session {
        val oldSession = getSession(session.id) ?: throw IllegalArgumentException("Cannot update not existing entity")
        require(
            oldSession.copy(
                validUntil = session.validUntil,
                refreshToken = session.refreshToken,
                accessToken = session.accessToken,
                miniSession = session.miniSession
            ) == session
        ) {
            "Can only update validUntil, refreshToken or minisession from session"
        }

        transaction(db) {
            SessionTable.update({ SessionTable.id eq session.id }) {
                it[validUntil] = session.validUntil
                it[accessToken] = accessToken
                it[refreshToken] = session.refreshToken
                it[minisession] = session.miniSession
            }
        }

        return session
    }

    internal fun deleteSession(session: Session) {
        transaction(db) {
            AfterActionsRedirectTable.deleteWhere { AfterActionsRedirectTable.sessionId eq session.id }
            SessionTable.deleteWhere { SessionTable.id eq session.id }
        }
    }

    internal fun deleteSessionsForUser(userId: String) {
        transaction(db) {
            val sessionIds = SessionTable.slice(SessionTable.id).select { SessionTable.userId eq userId }.map { it[SessionTable.id].value }

            AfterActionsRedirectTable.deleteWhere { AfterActionsRedirectTable.sessionId inList sessionIds }
            SessionTable.deleteWhere { SessionTable.userId eq userId }
        }
    }

    internal fun getAfterActionsRedirect(session: Session): String? = transaction(db) {
        AfterActionsRedirectTable.select { AfterActionsRedirectTable.sessionId eq session.id }
            .firstOrNull()
            ?.let { it[AfterActionsRedirectTable.redirectUrl] }
    }

    internal fun insertAfterActionsRedirect(session: Session, redirectUrl: String) {
        transaction(db) {
            AfterActionsRedirectTable.insert {
                it[sessionId] = session.id
                it[AfterActionsRedirectTable.redirectUrl] = redirectUrl
            }
        }
    }

    private object SessionTable : UUIDTable("sessions") {
        val userId: Column<String> = SessionTable.varchar("userId", 255)
        val validUntil: Column<Instant> = timestamp("validUntil")
        val authorizer: Column<String> = varchar("authorizer", 255)
        val accessToken: Column<String?> = text("access_token").nullable()
        val refreshToken: Column<String?> = text("refresh_token").nullable()
        val minisession: Column<Boolean> = bool("minisession").default(false)
    }

    private object AfterActionsRedirectTable : Table("after_actions") {
        val sessionId: Column<EntityID<UUID>> = reference("session_id", SessionTable.id)
        val redirectUrl: Column<String> = varchar("redirect_url", 4096)

        override val primaryKey: PrimaryKey = PrimaryKey(sessionId)
    }
}
