package dev.maximilian.feather.gdpr

import dev.maximilian.feather.action.Action

public data class GdprAction(
    override val userId: String,
    override val payload: Int
) : Action<Int> {
    public companion object {
        public const val NAME: String = "gdpr"
    }

    override val name: String = NAME

    override fun payloadToString(): String = "$payload"

    override val description: String = "Datenschutzbestimmungen akzeptieren"
}
