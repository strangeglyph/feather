FROM alpine:3.13

WORKDIR /usr/local/app

RUN apk add --no-cache openjdk11-jre-headless && \
    addgroup -S app && \
    adduser -S -G app app && \
    chown app:app /usr/local/app

USER app

COPY core/build/install/ ./

CMD ["/usr/local/app/feather/bin/feather"]
