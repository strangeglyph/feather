/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.action.ActionController
import dev.maximilian.feather.gdpr.GdprController
import dev.maximilian.feather.getEnv
import dev.maximilian.feather.multiservice.mockserver.EmptyCheckUserEvent
import dev.maximilian.feather.multiservice.mockserver.GroupSyncMock
import dev.maximilian.feather.multiservice.mockserver.ServiceMock
import dev.maximilian.feather.multiservice.mockserver.SessionTerminatorMock
import dev.maximilian.feather.multiservice.mockserver.UserDeletionEventMock
import dev.maximilian.feather.multiservice.nextcloud.NextcloudService
import dev.maximilian.feather.multiservice.nextcloud.NextcloudSettings
import dev.maximilian.feather.multiservice.openproject.OpenProjectService
import dev.maximilian.feather.multiservice.settings.ChangeMailSetting
import dev.maximilian.feather.multiservice.settings.MultiServiceConfig
import dev.maximilian.feather.multiservice.settings.MultiServiceEvents
import dev.maximilian.feather.multiservice.settings.MultiServiceMailSettings
import dev.maximilian.feather.multiservice.settings.OpenProjectTestVariables
import dev.maximilian.feather.multiservice.settings.SMTPSetting
import dev.maximilian.feather.multiservice.settings.SendMailContent
import dev.maximilian.feather.multiservice.settings.TestCredentialProvider
import io.javalin.Javalin
import org.junit.jupiter.api.Test
import javax.mail.internet.InternetAddress

class MultiserviceTest {
    val pool = ApiTestUtilities.pool

    private val credentialProvider = TestCredentialProvider.credentialProvider
    private val gdprController = GdprController(ApiTestUtilities.db, ActionController(ApiTestUtilities.db), credentialProvider::getUsers)

    private val syncEvent = GroupSyncMock()
    private val invitationMailSettings =
        SendMailContent("invitation@mail.de", "you are invited to test server!!!")

    private val internetAddress = InternetAddress("generalMails@mail.de", "myTestBot")

    private val forgotMailSettings = SendMailContent(
        "forgotten@mail.de",
        "No problem. Here ist your restored mail."
    )

    private val changeMailSetting = ChangeMailSetting(
        "mailchanger@test.de",
        "your mail had been changed",
        "notifcation 1 of change mail",
        "notifcation 2 of change mail"
    )
    private val smtp = SMTPSetting(
        "smtp.testmail.com",
        "533",
        "PASSWORD",
        "TRUE",
        "user@smpt.de",
        "topsecret",
    )

    private val mailSetting = MultiServiceMailSettings(
        "mytest.de",
        smtp, internetAddress,
        forgotMailSettings,
        invitationMailSettings, changeMailSetting, "nextcloud.mytest.de"
    )

    private val nextcloudSettings = NextcloudSettings(
        getEnv("NEXTCLOUD_USER_BINDING", "harry"),
        getEnv("NEXTCLOUD_PUBLIC_URL", "http://127.0.0.1:8082"),
        getEnv("NEXTCLOUD_BASE_URL", "http://127.0.0.1:8082"),
        getEnv("NEXTCLOUD_USER", "ncadmin"),
        getEnv("NEXTCLOUD_PWD", "ncadminsecret")
    )

    private val backgroundJobManager = BackgroundJobManager(pool)

    @Test
    fun `Multiservice can be constructed without errors`() {
        val userDeletionEvents = listOf(UserDeletionEventMock())
        val sessionTerminatorEvent = SessionTerminatorMock()

        val callbackEvents = MultiServiceEvents(syncEvent, userDeletionEvents, emptyList(), sessionTerminatorEvent, EmptyCheckUserEvent())

        val multiserviceConfig =
            MultiServiceConfig(credentialProvider, backgroundJobManager, ApiTestUtilities.db, mailSetting, callbackEvents, gdprController)
        Multiservice(multiserviceConfig)
    }

    @Test
    fun `Multiservice is starting api without services`() {

        val userDeletionEvents = listOf(UserDeletionEventMock())
        val sessionTerminatorEvent = SessionTerminatorMock()

        val callbackEvents = MultiServiceEvents(syncEvent, userDeletionEvents, emptyList(), sessionTerminatorEvent, EmptyCheckUserEvent())

        val multiserviceConfig =
            MultiServiceConfig(credentialProvider, backgroundJobManager, ApiTestUtilities.db, mailSetting, callbackEvents, gdprController)
        val ms = Multiservice(multiserviceConfig)

        val app = Javalin.create {
            it.showJavalinBanner = false
            it.enableCorsForAllOrigins()
            it.logIfServerNotStarted = false
            it.defaultContentType = "application/json"
            it.contextPath = "/v1"
        }
        ms.startApis(app)
    }

    @Test
    fun `Multiservice starts with dummy service`() {
        val userDeletionEvents = listOf(UserDeletionEventMock())
        val sessionTerminatorEvent = SessionTerminatorMock()

        val callbackEvents = MultiServiceEvents(syncEvent, userDeletionEvents, emptyList(), sessionTerminatorEvent, EmptyCheckUserEvent())

        val multiserviceConfig =
            MultiServiceConfig(credentialProvider, backgroundJobManager, ApiTestUtilities.db, mailSetting, callbackEvents, gdprController)
        val ms = Multiservice(multiserviceConfig)

        val app = Javalin.create {
            it.showJavalinBanner = false
            it.enableCorsForAllOrigins()
            it.logIfServerNotStarted = false
            it.defaultContentType = "application/json"
            it.contextPath = "/v1"
        }
        val testService = ServiceMock()
        ms.services.add(testService)
        ms.startApis(app)
    }

    @Test
    fun `Multiservice starts with openproject service`() {
        val userDeletionEvent = UserDeletionEventMock()
        val sessionTerminatorEvent = SessionTerminatorMock()

        val callbackEvents = MultiServiceEvents(syncEvent, listOf(userDeletionEvent), emptyList(), sessionTerminatorEvent, EmptyCheckUserEvent())
        val multiserviceConfig = MultiServiceConfig(credentialProvider, backgroundJobManager, ApiTestUtilities.db, mailSetting, callbackEvents, gdprController)
        val ms = Multiservice(multiserviceConfig)

        val groupSynchronizationEvent = GroupSyncMock()
        val opService = OpenProjectService(backgroundJobManager, groupSynchronizationEvent, OpenProjectTestVariables.settings, credentialProvider, 3)
        ms.services.add(opService)

        val app = Javalin.create {
            it.showJavalinBanner = false
            it.enableCorsForAllOrigins()
            it.logIfServerNotStarted = false
            it.defaultContentType = "application/json"
            it.contextPath = "/v1"
        }
        ms.startApis(app)
    }

    @Test
    fun `Multiservice starts with nextcloud service`() {
        val userDeletionEvents = listOf(UserDeletionEventMock())
        val sessionTerminatorEvent = SessionTerminatorMock()

        val callbackEvents = MultiServiceEvents(syncEvent, userDeletionEvents, emptyList(), sessionTerminatorEvent, EmptyCheckUserEvent())
        val multiserviceConfig =
            MultiServiceConfig(credentialProvider, backgroundJobManager, ApiTestUtilities.db, mailSetting, callbackEvents, gdprController)
        val ms = Multiservice(multiserviceConfig)

        val app = Javalin.create {
            it.showJavalinBanner = false
            it.enableCorsForAllOrigins()
            it.logIfServerNotStarted = false
            it.defaultContentType = "application/json"
            it.contextPath = "/v1"
        }
        val ns = NextcloudService(app, pool, nextcloudSettings, credentialProvider)

        ms.services.add(ns)
        ms.startApis(app)
    }
}
