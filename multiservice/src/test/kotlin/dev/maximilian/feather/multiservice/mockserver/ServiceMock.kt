/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.mockserver

import com.shopify.promises.Promise
import dev.maximilian.feather.User
import dev.maximilian.feather.multiservice.IControllableService
import java.util.UUID

class ServiceMock : IControllableService {
    val users = mutableListOf<ServiceMockUserEntry>()
    var createUserCounter = 0
    var deleteUserCounter = 0

    override fun activateUser(user: User, activate: Boolean): Boolean {
        val c = users.firstOrNull { it.user.id == user.id }
        return if (c == null)
            false
        else if (c.active != activate) {
            c.active = activate
            true
        } else false
    }

    override fun changeMailAddress(user: User, newMail: String): Boolean {
        users.firstOrNull { it.user.id == user.id } ?: return false
        users.removeAll { it.user.id == user.id }
        users.add(ServiceMockUserEntry(user.copy(mail = newMail), true))
        return true
    }

    override fun changeUserName(
        user: User,
        newFirstname: String,
        newSurname: String,
        newDisplayName: String
    ): Boolean {
        users.firstOrNull { it.user.id == user.id } ?: return false
        users.removeAll { it.user.id == user.id }
        users.add(
            ServiceMockUserEntry(
                user.copy(
                    firstname = newFirstname,
                    surname = newSurname, displayName = newDisplayName
                ),
                true
            )
        )
        return true
    }

    override suspend fun checkUser(user: User, autoRepair: Boolean): String {
        users.firstOrNull { it.user.id == user.id } ?: return "Not Found."
        return "OK"
    }

    override fun createUser(createdUser: User, jobId: UUID?): Promise<User, RuntimeException> {
        createUserCounter++
        if (users.firstOrNull { it.user.id == createdUser.id } == null) {
            users.add(ServiceMockUserEntry(createdUser, true))
            return Promise.ofError(java.lang.RuntimeException("not found"))
        }
        return Promise.ofSuccess(createdUser)
    }

    override fun deleteUser(user: User): Boolean {
        deleteUserCounter++
        users.firstOrNull { it.user.id == user.id } ?: return false
        users.removeAll { it.user.id == user.id }
        return true
    }

    override fun getServiceName(): String {
        return "TestService"
    }
}
