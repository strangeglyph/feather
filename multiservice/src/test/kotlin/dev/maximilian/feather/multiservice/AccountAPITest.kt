/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.multiservice.api.internal.ChangePasswordRequest
import dev.maximilian.feather.multiservice.mockserver.PasswordSavingCredential
import dev.maximilian.feather.multiservice.settings.TestCredentialProvider
import dev.maximilian.feather.multiservice.settings.TestUser
import kong.unirest.HttpResponse
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class AccountAPITest {
    @Test
    fun `POST to account-changepass with modified password leads to 204 (NO CONTENT)`() {
        val apiUtilities = ApiTestUtilities()
        val result = executePasswordChange(
            apiUtilities,
            TestUser.standardLogin.password,
            TestUser.standardLogin.password + "NEW",
            TestUser.standardLogin.password + "NEW"
        )
        assertEquals(HttpStatus.NO_CONTENT_204, result.status)
    }

    @Test
    fun `POST to account-changepass with modified password leads to real password change`() {
        val pwdCredentials = PasswordSavingCredential(TestCredentialProvider.credentialProvider)
        val apiUtilities = ApiTestUtilities()
        apiUtilities.setCredentialProvider(pwdCredentials)
        executePasswordChange(
            apiUtilities,
            TestUser.standardLogin.password,
            TestUser.standardLogin.password + "NEW",
            TestUser.standardLogin.password + "NEW"
        )
        val id = pwdCredentials.getUserByUsername(TestUser.standardLogin.name)!!.id
        assertEquals(TestUser.standardLogin.password + "NEW", pwdCredentials.passwords[id])
    }

    @Test
    fun `POST to account-changepass with 7 signs leads to 400 (BAD REQUEST)`() {
        val pwdCredentials = PasswordSavingCredential(TestCredentialProvider.credentialProvider)
        val apiUtilities = ApiTestUtilities()
        apiUtilities.setCredentialProvider(pwdCredentials)
        val result = executePasswordChange(
            apiUtilities,
            TestUser.standardLogin.password,
            "1234567",
            "1234567"
        )
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST to account-changepass with different passwords leads to 400 (BAD REQUEST)`() {
        val apiUtilities = ApiTestUtilities()
        val result = executePasswordChange(
            apiUtilities,
            TestUser.standardLogin.password,
            TestUser.standardLogin.password + "NEW",
            TestUser.standardLogin.password + "ASD"
        )
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `POST to account-changepass with different passwords doesn't change password`() {
        val pwdCredentials = PasswordSavingCredential(TestCredentialProvider.credentialProvider)
        val apiUtilities = ApiTestUtilities()
        apiUtilities.setCredentialProvider(pwdCredentials)
        executePasswordChange(
            apiUtilities,
            TestUser.standardLogin.password,
            TestUser.standardLogin.password + "NEW", TestUser.standardLogin.password + "ASD"
        )

        val id = pwdCredentials.getUserByUsername(TestUser.standardLogin.name)!!.id
        assertEquals(TestUser.standardLogin.password, pwdCredentials.passwords[id])
    }

    @Test
    fun `POST to account-changepass with wrong passwords doesn't change password`() {
        val pwdCredentials = PasswordSavingCredential(TestCredentialProvider.credentialProvider)
        val apiUtilities = ApiTestUtilities()
        apiUtilities.setCredentialProvider(pwdCredentials)
        executePasswordChange(
            apiUtilities,
            "wrongPassword",
            TestUser.standardLogin.password + "NEW",
            TestUser.standardLogin.password + "NEW"
        )
        val id = pwdCredentials.getUserByUsername(TestUser.standardLogin.name)!!.id
        assertEquals(TestUser.standardLogin.password, pwdCredentials.passwords[id])
    }

    @Test
    fun `POST to account-changepass with wrong passwords leads to 403 (FORBIDDEN)`() {
        val apiUtilities = ApiTestUtilities()
        val result = executePasswordChange(
            apiUtilities,
            "wrongPassword",
            TestUser.standardLogin.password + "NEW",
            TestUser.standardLogin.password + "NEW"
        )
        assertEquals(HttpStatus.FORBIDDEN_403, result.status)
    }

    private fun executePasswordChange(apiUtilities: ApiTestUtilities, oldPassword: String, newPassword1: String, newPassword2: String): HttpResponse<Any> {
        val scenario = apiUtilities.startWithDummyService(false)
        val changePasswordRequest = ChangePasswordRequest(
            oldPassword,
            newPassword1,
            newPassword2
        )
        return apiUtilities.restConnection.post("${scenario.basePath}/account/changepass").body(changePasswordRequest).asEmpty()
    }
}
