/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import dev.maximilian.feather.Permission
import dev.maximilian.feather.multiservice.api.internal.MultiServiceUserCreateRequest
import dev.maximilian.feather.multiservice.api.internal.MultiServiceUserDeleteRequest
import dev.maximilian.feather.multiservice.api.internal.MultiServiceUsersDeleteRequest
import dev.maximilian.feather.multiservice.mockserver.ServiceMockUserEntry
import dev.maximilian.feather.multiservice.settings.TestCredentialProvider
import dev.maximilian.feather.multiservice.settings.TestUser
import kong.unirest.GenericType
import kong.unirest.HttpRequest
import kong.unirest.HttpResponse
import kong.unirest.RequestBodyEntity
import org.eclipse.jetty.http.HttpStatus
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class MultiServiceUserApiTest {
    private val credentials = TestCredentialProvider.credentialProvider

    @Test
    fun `Create user by standard USER leads to FORBIDDEN (403)`() {
        val apiUtilities = ApiTestUtilities()
        apiUtilities.startWithDummyService(false)
        val result = buildCreateRequest(apiUtilities, emptySet(), false).asEmpty()
        assertEquals(HttpStatus.FORBIDDEN_403, result.status)
    }

    @Test
    fun `Create user by ADMIN leads to CREATED (201)`() {
        val apiUtilities = ApiTestUtilities()
        apiUtilities.startWithDummyService(true)
        val result = buildCreateRequest(apiUtilities, emptySet(), false).asEmpty()
        assertEquals(HttpStatus.CREATED_201, result.status)
    }

    @Test
    fun `Create user with ADMIN permission leads to FORBIDDEN (403)`() {
        val apiUtilities = ApiTestUtilities()
        apiUtilities.startWithDummyService(true)
        val result = buildCreateRequest(apiUtilities, setOf(Permission.ADMIN.toString()), false).asEmpty()
        assertEquals(HttpStatus.FORBIDDEN_403, result.status)
    }

    @Test
    fun `Create user leads to one create user call`() {
        val apiUtilities = ApiTestUtilities()
        val testService = apiUtilities.startWithDummyService(true)
        buildCreateRequest(apiUtilities, emptySet(), false).asEmpty()
        assertEquals(1, testService.serviceMock.createUserCounter)
    }

    @Test
    fun `Create user leads to correct user call`() {
        val apiUtilities = ApiTestUtilities()
        val testService = apiUtilities.startWithDummyService(true)
        buildCreateRequest(apiUtilities, emptySet(), false).asEmpty()
        assertTrue(testService.serviceMock.users[0].active)
        assertEquals("harry55", testService.serviceMock.users[0].user.username)
        assertEquals(1, testService.serviceMock.createUserCounter)
    }

    @Test
    fun `DELETE of foreign accounts by standard user leads to FORBIDDEN`() {
        val apiUtilities = ApiTestUtilities()
        apiUtilities.startWithDummyService(false)
        val id = credentials.getUserByUsername(TestUser.adminLogin.name)!!.id
        val multiServiceUserDeleteRequest = MultiServiceUserDeleteRequest(TestUser.standardLogin.password, true)
        val result = executeDeleteRequest(apiUtilities, id, multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.FORBIDDEN_403, result.status)
    }

    @Test
    fun `DELETE of USER of own account leads to SUCCESS (200)`() {
        val apiUtilities = ApiTestUtilities()
        apiUtilities.startWithDummyService(false)
        val testUser = credentials.getUserByUsername(TestUser.standardLogin.name)
        apiUtilities.scenario!!.serviceMock.users.add(ServiceMockUserEntry(testUser!!, true))
        val multiServiceUserDeleteRequest = MultiServiceUserDeleteRequest(TestUser.standardLogin.password, true)
        val result = executeDeleteRequest(apiUtilities, testUser.id, multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.OK_200, result.status)
    }

    @Test
    fun `DELETE without verification leads to BAD REQUEST (400)`() {
        val apiUtilities = ApiTestUtilities()
        apiUtilities.startWithDummyService(false)
        val id = credentials.getUserByUsername(TestUser.standardLogin.name)!!.id
        val multiServiceUserDeleteRequest = MultiServiceUserDeleteRequest(TestUser.standardLogin.password, false)
        val result = executeDeleteRequest(apiUtilities, id, multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.BAD_REQUEST_400, result.status)
    }

    @Test
    fun `DELETE leads to one delete user call`() {
        val apiUtilities = ApiTestUtilities()
        val testService = apiUtilities.startWithDummyService(false)

        val id = credentials.getUserByUsername(TestUser.standardLogin.name)!!.id
        val multiServiceUserDeleteRequest = MultiServiceUserDeleteRequest(TestUser.standardLogin.password, true)
        executeDeleteRequest(apiUtilities, id, multiServiceUserDeleteRequest)
        assertEquals(1, testService.serviceMock.deleteUserCounter)
    }

    @Test
    fun `DELETE of ADMIN deletes correct user`() {
        val apiUtilities = ApiTestUtilities()
        val testService = apiUtilities.startWithDummyService(false)
        val testUser = credentials.getUserByUsername(TestUser.standardLogin.name)
        testService.serviceMock.users.add(ServiceMockUserEntry(testUser!!, true))
        val multiServiceUserDeleteRequest = MultiServiceUserDeleteRequest(TestUser.standardLogin.password, true)
        executeDeleteRequest(apiUtilities, testUser.id, multiServiceUserDeleteRequest)
        assertEquals(0, testService.serviceMock.users.count())
    }

    @Test
    fun `Multiple DELETE as Admin is CREATED (201)`() {
        val apiUtilities = ApiTestUtilities()
        val testService = apiUtilities.startWithDummyService(true)

        val testUser = credentials.getUserByUsername(TestUser.standardLogin.name)
        testService.serviceMock.users.add(ServiceMockUserEntry(testUser!!, true))
        val multiServiceUserDeleteRequest = MultiServiceUsersDeleteRequest(TestUser.adminLogin.password, listOf(testUser.id), true)
        val response = executeMultipleDeleteRequest(apiUtilities, multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.CREATED_201, response.status)
    }

    @Test
    fun `Multiple DELETE with wrong password is FORBIDDEN (403)`() {
        val apiUtilities = ApiTestUtilities()
        val testService = apiUtilities.startWithDummyService(true)

        val testUser = credentials.getUserByUsername(TestUser.standardLogin.name)
        testService.serviceMock.users.add(ServiceMockUserEntry(testUser!!, true))
        val multiServiceUserDeleteRequest = MultiServiceUsersDeleteRequest("lalala", listOf(testUser.id), true)
        val response = executeMultipleDeleteRequest(apiUtilities, multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    @Test
    fun `Multiple DELETE as user is FORBIDDEN (403)`() {
        val apiUtilities = ApiTestUtilities()
        val testService = apiUtilities.startWithDummyService(true)

        val testUser = credentials.getUserByUsername(TestUser.standardLogin.name)
        testService.serviceMock.users.add(ServiceMockUserEntry(testUser!!, true))
        val multiServiceUserDeleteRequest = MultiServiceUsersDeleteRequest(TestUser.standardLogin.password, listOf(testUser.id), true)
        val response = executeMultipleDeleteRequest(apiUtilities, multiServiceUserDeleteRequest)
        assertEquals(HttpStatus.FORBIDDEN_403, response.status)
    }

    private fun buildCreateRequest(apiTestUtilities: ApiTestUtilities, permissions: Set<String>, async: Boolean): RequestBodyEntity {
        val multiServiceUserCreateRequest = MultiServiceUserCreateRequest(
            "harry55",
            "test12345",
            "test12345",
            "harry@somemail.de",
            "Harry",
            "Test",
            setOf(),
            permissions,
            true,
            apiTestUtilities.gdprController.getLatestDocument()?.id
        )
        val param = if (async) "?async=TRUE" else ""

        return apiTestUtilities
            .restConnection
            .put("${apiTestUtilities.scenario!!.basePath}/bindings/multiservice/users$param")
            .body(multiServiceUserCreateRequest)
    }

    private fun executeDeleteRequest(apiTestUtilities: ApiTestUtilities, userID: String, multiServiceUserDeleteRequest: MultiServiceUserDeleteRequest): HttpResponse<Any> {
        return apiTestUtilities
            .restConnection
            .delete("${apiTestUtilities.scenario!!.basePath}/bindings/multiservice/users/$userID")
            .body(multiServiceUserDeleteRequest)
            .asEmpty()
    }

    private fun executeMultipleDeleteRequest(apiTestUtilities: ApiTestUtilities, multiServiceUsersDeleteRequest: MultiServiceUsersDeleteRequest): HttpResponse<Any> {
        return apiTestUtilities
            .restConnection
            .post("${apiTestUtilities.scenario!!.basePath}/bindings/multiservice/users/delete")
            .body(multiServiceUsersDeleteRequest)
            .asEmpty()
    }

    inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> =
        this.asObject(object : GenericType<B>() {})
}
