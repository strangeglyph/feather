/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.test.Test
import kotlin.test.assertEquals

class JsonBTest {
    private val db = ApiTestUtilities.db

    @Test
    fun `Test jsonb crd operations on h2 database`() {
        val testSet = setOf(
            ComplexTestObject("hello world", 42),
            ComplexTestObject("hello foo", 43)
        )

        transaction(db) {
            SchemaUtils.createMissingTablesAndColumns(JsonBTestTable)
        }

        transaction(db) {
            JsonBTestTable.insert {
                it[complex] = testSet.first()
                it[complexIterable] = testSet
            }
        }

        val retrieved = transaction(db) {
            JsonBTestTable.selectAll().first().let {
                it[JsonBTestTable.complex] to it[JsonBTestTable.complexIterable].toSet()
            }
        }

        assertEquals(testSet.first(), retrieved.first)
        assertEquals(testSet, retrieved.second)

        transaction(db) {
            JsonBTestTable.deleteAll()
        }
    }
}

private data class ComplexTestObject(
    val foo: String,
    val bar: Int
)

private object JsonBTestTable : IntIdTable("test_jsonb") {
    private val objectMapper = jacksonObjectMapper()

    val complex: Column<ComplexTestObject> = jsonb("complex", objectMapper)
    val complexIterable: Column<Set<ComplexTestObject>> = jsonb("complex_iterable", objectMapper)
}
