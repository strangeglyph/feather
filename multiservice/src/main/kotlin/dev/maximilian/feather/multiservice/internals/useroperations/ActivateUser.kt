/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.multiservice.internals.useroperations

import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.User
import dev.maximilian.feather.authorization.ISessionOperations
import dev.maximilian.feather.multiservice.IControllableService
import mu.KLogging

internal class ActivateUser(
    private val credentialProvider: ICredentialProvider,
    private val services: List<IControllableService>
) {
    companion object : KLogging()

    fun activateUser(userId: String, activate: Boolean, sessionTerminator: ISessionOperations): User {
        logger.info { "ActivateUser::activateUser (De)activating user ($userId)" }

        var user = credentialProvider.getUser(userId)
            ?: throw IllegalArgumentException("ActivateUser::activateUser User ($userId) not found")

        var ok = true
        services.forEach {
            if (!it.activateUser(user, activate))
                ok = false
        }

        if (!ok) {
            throw IllegalStateException("Cannot (de)activate LDAP user because of previous error")
        }

        if (!activate) {
            sessionTerminator.deleteSessionsForUser(user.id)
        }

        user = credentialProvider.updateUser(user.copy(disabled = !activate))
            ?: throw IllegalStateException("Failed to (de)activate LDAP user")

        return user
    }
}
