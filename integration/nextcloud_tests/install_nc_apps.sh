#!/bin/bash -e

SCRIPT_DIR="$(dirname "$0")"
source "$SCRIPT_DIR/variables.sh"

if [ -n "$NEXTCLOUD_BASE_URL" ]; then

  NEXTCLOUD_APPS_SETTINGS_URL="$NEXTCLOUD_BASE_URL/settings/apps/files"
  NEXTCLOUD_ENABLE_APP_URL="$NEXTCLOUD_BASE_URL/settings/apps/enable"

  nc_install_app() {
    rm -f cookie.txt
    # for whatever reason, initializing the cookies must be done twice
    curl -u $NEXTCLOUD_USER:$NEXTCLOUD_PWD \
      --fail "$NEXTCLOUD_APPS_SETTINGS_URL" \
      -s -o /dev/null -c cookie.txt
    curl -u $NEXTCLOUD_USER:$NEXTCLOUD_PWD \
      --fail "$NEXTCLOUD_APPS_SETTINGS_URL" \
      -s -o /dev/null -b cookie.txt -c cookie.txt
    result=$(curl -b cookie.txt \
      --fail "$NEXTCLOUD_APPS_SETTINGS_URL" -s | grep -o data-requesttoken=\".*\")
    [[ "$result" =~ data-requesttoken=\"(.*)\" ]]
    requesttoken=${BASH_REMATCH[1]}
    curl -X POST -b cookie.txt \
      --header "Content-Type: application/json" \
      --header "requesttoken: $requesttoken" \
      --data "{\"appIds\": [\"$1\"],\"groups\":[]}" \
      --fail "$NEXTCLOUD_ENABLE_APP_URL" -s -o /dev/null || false
    ret=$?
    rm -f cookie.txt
    return $ret
  }

  nc_install_app groupfolders

else

  if ! ./occ app:list | grep groupfolders; then
    ./occ app:install groupfolders
    echo "App groupfolders installed"
  else
    echo "App groupfolders already installed"
  fi

fi
