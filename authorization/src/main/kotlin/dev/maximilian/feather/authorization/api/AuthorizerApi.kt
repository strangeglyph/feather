/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization.api

import dev.maximilian.feather.ICredentialAuthorizer
import dev.maximilian.feather.IOidcAuthorizer
import dev.maximilian.feather.ITokenAuthorizer
import dev.maximilian.feather.Session
import dev.maximilian.feather.authorization.AuthorizationController
import dev.maximilian.feather.authorization.ISessionOperations
import dev.maximilian.feather.authorization.cookie
import dev.maximilian.feather.sessionOrMinisessionOrNull
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.delete
import io.javalin.apibuilder.ApiBuilder.get
import io.javalin.apibuilder.ApiBuilder.path
import io.javalin.apibuilder.ApiBuilder.post
import io.javalin.http.Context
import io.javalin.http.NotFoundResponse
import io.javalin.http.UnauthorizedResponse
import java.util.UUID
import javax.servlet.http.HttpServletResponse

class AuthorizerApi(
    app: Javalin,
    private val authorizationController: AuthorizationController,
    private val disableSecureCookie: Boolean = false
) : ISessionOperations {
    init {
        app.before(::sessionBeforeHandler)
        app.routes {
            path("/session") {
                get(::getSession)
                delete(::deleteSession)
            }

            authorizationController.authorizer.forEach { authorizer ->
                path("/authorizer/${authorizer.id}") {
                    when (authorizer) {
                        is IOidcAuthorizer -> {
                            get { initOidcAuth(it, authorizer) }
                            post { finalizeOidcAuth(it, authorizer) }
                        }
                        is ICredentialAuthorizer -> {
                            post { finalizeCredentialAuth(it, authorizer) }
                        }
                        is ITokenAuthorizer -> {
                            post { finalizeTokenAuth(it, authorizer) }
                        }
                    }
                }
            }
        }
    }

    private fun sessionBeforeHandler(ctx: Context) {
        val token = ctx.header("X-Token") ?: ctx.cookie("X-Token")

        val session = token?.let { kotlin.runCatching { UUID.fromString(it) }.getOrNull() }
            ?.let { authorizationController.getSession(it) }

        ctx.attribute("session", session)
    }

    private fun getSession(ctx: Context) {
        ctx.json(ctx.sessionOrMinisessionOrNull()?.toAnswer() ?: throw NotFoundResponse())
    }

    override fun deleteSession(ctx: Context) {
        val session = ctx.sessionOrMinisessionOrNull() ?: throw NotFoundResponse()
        authorizationController.deleteSession(session)
        ctx.removeCookie("X-Token")
        ctx.json(session.authorizer.afterLogoutURL())
        ctx.status(200)
    }

    private fun initOidcAuth(ctx: Context, authorizer: IOidcAuthorizer) {
        ctx.redirect(authorizer.redirectUrl(), HttpServletResponse.SC_FOUND)
    }

    private fun finalizeOidcAuth(ctx: Context, authorizer: IOidcAuthorizer) {
        val code = ctx.body<String>()
        val session = authorizer.authorize(code)?.let { authorizationController.finalizeAuth(it) } ?: throw UnauthorizedResponse()

        sendCreationAnswer(ctx, session)
    }

    private fun finalizeTokenAuth(ctx: Context, authorizer: ITokenAuthorizer) {
        val code = ctx.body<String>()
        val session = authorizer.authorize(code)?.let { authorizationController.finalizeAuth(it) } ?: throw UnauthorizedResponse()

        sendCreationAnswer(ctx, session)
    }

    private fun finalizeCredentialAuth(ctx: Context, authorizer: ICredentialAuthorizer) {
        val loginRequest = ctx.body<LoginRequest>()
        val session = authorizer.authorize(loginRequest.mail, loginRequest.password)
            ?.let { authorizationController.finalizeAuth(it) } ?: throw UnauthorizedResponse()

        sendCreationAnswer(ctx, session)
    }

    private fun sendCreationAnswer(ctx: Context, session: Session) {
        ctx.status(201)
        ctx.json(session.toAnswer())
        ctx.cookie("X-Token", session.id.toString(), secure = !disableSecureCookie)
    }

    override fun deleteSessionsForUser(userId: String) = authorizationController.deleteSessionsForUser(userId)

    private fun Session.toAnswer() = SessionAnswer(
        id = id,
        validUntil = validUntil,
        user = user,
        authorizer = authorizer.id,
        miniSession = miniSession
    )
}
