/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization

import dev.maximilian.feather.Group
import dev.maximilian.feather.ICredentialProvider
import dev.maximilian.feather.Permission
import dev.maximilian.feather.User
import dev.maximilian.feather.authorization.internals.ldap.GroupOfNames
import dev.maximilian.feather.authorization.internals.ldap.InetOrgPerson
import dev.maximilian.feather.authorization.internals.ldap.LdapORM
import dev.maximilian.feather.authorization.internals.ldap.getUUID
import dev.maximilian.feather.authorization.internals.ldap.toEntity
import dev.maximilian.feather.authorization.internals.ldap.toLdapEntry
import org.apache.directory.api.ldap.model.constants.LdapSecurityConstants
import org.apache.directory.api.ldap.model.constants.SchemaConstants
import org.apache.directory.api.ldap.model.entry.DefaultModification
import org.apache.directory.api.ldap.model.entry.Modification
import org.apache.directory.api.ldap.model.entry.ModificationOperation
import org.apache.directory.api.ldap.model.message.SearchScope
import org.apache.directory.api.ldap.model.name.Dn
import org.apache.directory.api.ldap.model.password.PasswordUtil
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.Locale

class LdapCredentialProvider internal constructor(val ldap: ILdapProvider) : ICredentialProvider {
    // companion object : KLogging()

    private val groupObjectClass = if (ldap.uniqueGroups) "groupOfUniqueNames" else "groupOfNames"
    private val groupMemberAttribute = if (ldap.uniqueGroups) "uniqueMember" else "member"

    override fun getUser(id: String): User? = ldap.searchOneEntry(
        ldap.baseDnUsers,
        "(&(objectClass=inetOrgPerson)(${ldap.uuidType}=$id))",
        SearchScope.ONELEVEL
    )?.toEntity<InetOrgPerson>(ldap)?.let {
        val memberGroups = ldap.searchEntries(
            dn = ldap.baseDnGroups,
            filter = "(&(objectClass=$groupObjectClass)($groupMemberAttribute=${it.dn}))"
        ).map { e -> e.getUUID() }

        val ownedGroups = ldap.searchEntries(
            dn = ldap.baseDnGroups,
            filter = "(&(objectClass=$groupObjectClass)(owner=${it.dn}))"
        ).map { e -> e.getUUID() }

        val permissions = ldap.searchEntries(
            dn = ldap.baseDnPermissions,
            filter = "(&(objectClass=$groupObjectClass)($groupMemberAttribute=${it.dn}))"
        ).map { e ->
            e[ldap.rdnGroup].string.uppercase(Locale.getDefault())
        }.filter {
            e ->
            enumContains<Permission>(e)
        }.map {
            e ->
            Permission.valueOf(e)
        }

        val disabled = ldap.getByUUID(
            it.entryUUID,
            ldap.baseDnUsers,
            arrayOf("nsAccountLock")
        )?.get("nsAccountLock")?.let { attr ->
            attr.string == "TRUE"
        }

        User(
            id = it.entryUUID,
            displayName = it.displayName,
            firstname = it.givenName ?: "",
            mail = it.mail?.firstOrNull() ?: "",
            registeredSince = it.createTimestamp,
            surname = it.sn,
            username = it.rdn,
            permissions = permissions.toSet(),
            groups = memberGroups.toSet(),
            ownedGroups = ownedGroups.toSet(),
            disabled = disabled ?: false
        )
    }

    override fun getUserByMail(mail: String): User? {
        return getUsers().firstOrNull() { it.mail == mail }
    }

    override fun getUserByUsername(username: String): User? {
        return getUsers().firstOrNull() { it.username == username }
    }

    override fun getUsers(): Collection<User> = getUsersAndGroups().first

    override fun createUser(user: User, password: String?): User {
        val cn = if (ldap.rdnUser != "cn") user.displayName else null
        val passwordToBeSend = if (ldap.plainTextPasswords) {
            password
        } else {
            password?.let {
                String(PasswordUtil.createStoragePassword(it, LdapSecurityConstants.HASH_METHOD_CRYPT_SHA256))
            }
        }
        val created = ldap.create(
            InetOrgPerson(
                dn = "${ldap.rdnUser}=${user.username},${ldap.baseDnUsers}",
                cn = cn,
                rdn = user.username,
                mail = setOf(user.mail),
                displayName = user.displayName,
                sn = user.surname,
                givenName = user.firstname,
                entryUUID = "",
                employeeNumber = null,
                userPassword = passwordToBeSend
            ).toLdapEntry(ldap)
        )

        val permissionMap = ldap.searchEntries(
            ldap.baseDnPermissions,
            filter = "(objectClass=$groupObjectClass)",
            scope = SearchScope.ONELEVEL
        ).filter {
            e ->
            enumContains<Permission>(e[ldap.rdnGroup].string.uppercase(Locale.getDefault()))
        }.associateBy {
            e ->
            Permission.valueOf(e[ldap.rdnGroup].string.uppercase(Locale.getDefault()))
        }

        user.permissions.mapNotNull { permissionMap[it] }.forEach {
            val listOfCurrentMembers = it[groupMemberAttribute]?.map { e -> e.string } ?: listOf<String>()

            ldap.modify(
                it.dn.toString(),
                DefaultModification(
                    ModificationOperation.REPLACE_ATTRIBUTE,
                    groupMemberAttribute,
                    *((listOfCurrentMembers + created.dn.toString()).toTypedArray())
                )
            )
        }

        val groupFilter = (user.groups + user.ownedGroups).joinToString(
            separator = ")(${ldap.uuidType}=",
            prefix = "(${ldap.uuidType}=",
            postfix = ")"
        )
        ldap.searchEntries(ldap.baseDnGroups, "(&(objectClass=$groupObjectClass)(|$groupFilter))", SearchScope.SUBTREE)
            .map { it.toEntity<GroupOfNames>(ldap) }.forEach {
                val groupModifications = mutableListOf<Modification>()

                // Add user to all groups he is owner or member
                // User is always member of groups he/she is owner
                if (!it.member.contains(created.dn.toString())) {
                    groupModifications.add(
                        DefaultModification(
                            ModificationOperation.REPLACE_ATTRIBUTE,
                            groupMemberAttribute,
                            *(it.member + created.dn.toString()).toTypedArray()
                        )
                    )
                }

                // Add user to groups only where he/she is owner
                if (!it.owner.contains(created.dn.toString()) && user.ownedGroups.contains(it.entryUUID)) {
                    groupModifications.add(
                        DefaultModification(
                            ModificationOperation.REPLACE_ATTRIBUTE,
                            SchemaConstants.OWNER_AT,
                            *(it.owner + created.dn.toString()).toTypedArray()
                        )
                    )
                }

                if (groupModifications.isNotEmpty()) {
                    ldap.modify(it.dn, groupModifications)
                }
            }

        return user.copy(
            id = created.getUUID(),
            registeredSince = Instant.from(
                LocalDateTime.parse(
                    created[SchemaConstants.CREATE_TIMESTAMP_AT].string,
                    LdapORM.ldapIsoFormatter
                ).toInstant(ZoneOffset.UTC)
            ),
            // User is always member of groups he/she is owner
            groups = user.groups + user.ownedGroups
        )
    }

    override fun deleteUser(user: User) {
        getUser(user.id)?.apply {
            // Clear all references to groups/permissions
            updateUser(
                user.copy(
                    permissions = emptySet(),
                    ownedGroups = emptySet(),
                    groups = emptySet()
                )
            )

            ldap.getByUUID(user.id, ldap.baseDnUsers)?.apply {
                ldap.delete(this)
            }
        }
    }

    override fun updateUser(user: User): User? {
        // Special case, user (no longer) existing, ignoring update request
        val savedUser = getUser(user.id) ?: return null

        // Special case rdn modification
        if (savedUser.username != user.username) {
            ldap.rename("${ldap.rdnUser}=${savedUser.username},${ldap.baseDnUsers}", "${ldap.rdnUser}=${user.username}")
        }

        val userDn = "${ldap.rdnUser}=${user.username},${ldap.baseDnUsers}"
        val modifications = mutableListOf<Modification>()
        attributeModificationOperation(SchemaConstants.DISPLAY_NAME_AT, savedUser.displayName, user.displayName)
            ?.also { modifications.add(it) }
        attributeModificationOperation(SchemaConstants.GIVENNAME_AT, savedUser.firstname, user.firstname)
            ?.also { modifications.add(it) }
        attributeModificationOperation(SchemaConstants.SURNAME_AT, savedUser.surname, user.surname)
            ?.also { modifications.add(it) }
        attributeModificationOperation(SchemaConstants.MAIL_AT, savedUser.mail, user.mail)
            ?.also { modifications.add(it) }

        val nsAccountLockAsString = ldap.getByUUID(
            user.id,
            ldap.baseDnUsers,
            arrayOf("nsAccountLock")
        )?.get("nsAccountLock")?.string

        if (nsAccountLockAsString != null || user.disabled) {
            attributeModificationOperation("nsAccountLock", nsAccountLockAsString, if (user.disabled) "TRUE" else "FALSE")
                ?.also { modifications.add(it) }
        }

        if (modifications.isNotEmpty()) {
            ldap.modify(userDn, modifications)
        }

        val permissionMap = ldap.searchEntries(
            ldap.baseDnPermissions,
            filter = "(objectClass=$groupObjectClass)",
            scope = SearchScope.ONELEVEL
        ).filter {
            e ->
            enumContains<Permission>(e[ldap.rdnGroup].string.uppercase(Locale.getDefault()))
        }.associateBy {
            e ->
            Permission.valueOf(e[ldap.rdnGroup].string.uppercase(Locale.getDefault()))
        }

        // Foreach removed permission
        savedUser.permissions.minus(user.permissions).mapNotNull { permissionMap[it] }.forEach {
            val listOfCurrentMembers = it[groupMemberAttribute]?.map { e -> e.string } ?: listOf<String>()

            val newMembers = (listOfCurrentMembers + ldap.dummyAccountDn - userDn).toSet()

            if (newMembers != listOfCurrentMembers.toSet()) {
                ldap.modify(
                    it.dn.toString(),
                    DefaultModification(
                        ModificationOperation.REPLACE_ATTRIBUTE,
                        groupMemberAttribute,
                        *newMembers.toTypedArray()
                    )
                )
            }
        }

        // Foreach new permission
        user.permissions.minus(savedUser.permissions).mapNotNull { permissionMap[it] }.forEach {
            val listOfCurrentMembers = it[groupMemberAttribute]?.map { e -> e.string } ?: listOf<String>()

            val newMembers = (listOfCurrentMembers + ldap.dummyAccountDn + userDn).toSet()

            if (newMembers != listOfCurrentMembers.toSet()) {
                ldap.modify(
                    it.dn.toString(),
                    DefaultModification(
                        ModificationOperation.REPLACE_ATTRIBUTE,
                        groupMemberAttribute,
                        *newMembers.toTypedArray()
                    )
                )
            }
        }

        val allOldUserGroups = savedUser.groups + savedUser.ownedGroups
        val allNewUserGroups = user.groups + user.ownedGroups

        // Foreach group (old and new)
        allOldUserGroups.plus(allNewUserGroups).mapNotNull { getGroup(it) }.forEach {
            if (user.ownedGroups.contains(it.id) && !it.owners.contains(user.id)) {
                // add this user as owner (and member) to that group
                updateGroup(it.copy(owners = it.owners + user.id, userMembers = it.userMembers + user.id))
            } else if (!allNewUserGroups.contains(it.id) && it.userMembers.contains(user.id)) {
                // remove this user as member (and owner) from that group
                updateGroup(it.copy(owners = it.owners - user.id, userMembers = it.userMembers - user.id))
            } else if (allNewUserGroups.contains(it.id) && !it.userMembers.contains(user.id)) {
                // add this user as member to that group
                updateGroup(it.copy(userMembers = it.userMembers + user.id))
            } else if (it.userMembers.contains(user.id) && !allNewUserGroups.contains(it.id)) {
                // remove this user as member from that group
                updateGroup(it.copy(userMembers = it.userMembers - user.id))
            }
        }

        return user.copy(groups = user.groups + user.ownedGroups)
    }

    override fun authenticateUser(username: String, password: String): User? = ldap.searchOneEntry(
        ldap.baseDnUsers,
        "(&(objectClass=inetOrgPerson)(${ldap.rdnUser}=$username))",
        SearchScope.ONELEVEL
    )?.takeIf { ldap.bind(it.dn.toString(), password) }?.getUUID()?.let { getUser(it) }

    override fun authenticateUserByMail(mail: String, password: String): User? = ldap.searchOneEntry(
        ldap.baseDnUsers,
        "(&(objectClass=inetOrgPerson)(mail=$mail))",
        SearchScope.ONELEVEL
    )?.takeIf { ldap.bind(it.dn.toString(), password) }?.getUUID()?.let { getUser(it) }

    override fun updateUserPassword(user: User, password: String): Boolean =
        ldap.getByUUID(user.id, ldap.baseDnUsers)
            ?.toEntity<InetOrgPerson>(ldap)
            ?.apply {
                val operation = when (userPassword) {
                    null -> ModificationOperation.ADD_ATTRIBUTE
                    else -> ModificationOperation.REPLACE_ATTRIBUTE
                }

                val passwordToBeSend = if (ldap.plainTextPasswords) {
                    password
                } else {
                    String(
                        PasswordUtil.createStoragePassword(
                            password,
                            LdapSecurityConstants.HASH_METHOD_CRYPT_SHA256
                        )
                    )
                }
                ldap.modify(
                    this.dn,
                    DefaultModification(
                        operation,
                        SchemaConstants.USER_PASSWORD_AT,
                        passwordToBeSend
                    )
                )
            } != null

    override fun getGroup(id: String): Group? = ldap.searchOneEntry(
        ldap.baseDnGroups,
        "(&(objectClass=$groupObjectClass)(${ldap.uuidType}=$id))",
        SearchScope.SUBTREE
    )?.toEntity<GroupOfNames>(ldap)?.let {
        val memberAndOwnerFilter =
            (it.member + it.owner).joinToString(separator = ")(entryDN=", prefix = "(entryDN=", postfix = ")")
        val personUIDs = ldap.searchEntries(
            dn = ldap.baseDnUsers,
            filter = "(&(objectClass=inetOrgPerson)(|$memberAndOwnerFilter))"
        ).associate { e -> e.dn.toString() to e.getUUID() }
        val groupUIDs = ldap.searchEntries(
            dn = ldap.baseDnGroups,
            filter = "(&(objectClass=$groupObjectClass)(|$memberAndOwnerFilter))"
        ).associate { e -> e.dn.toString() to e.getUUID() }

        val memberGroups = ldap.searchEntries(
            dn = ldap.baseDnGroups,
            filter = "(&(objectClass=$groupObjectClass)($groupMemberAttribute=${it.dn}))"
        ).map { e -> e.getUUID() }

        val ownedGroups = ldap.searchEntries(
            dn = ldap.baseDnGroups,
            filter = "(&(objectClass=$groupObjectClass)(owner=${it.dn}))"
        ).map { e -> e.getUUID() }

        Group(
            id = it.entryUUID,
            // TODO: how to get DN parent here?
            dnParent = null,
            name = it.rdn,
            description = it.description,
            owners = it.owner.mapNotNull { e -> personUIDs[e] }.toSet(),
            ownerGroups = it.owner.mapNotNull { e -> groupUIDs[e] }.toSet(),
            userMembers = it.member.mapNotNull { e -> personUIDs[e] }.toSet(),
            groupMembers = it.member.mapNotNull { e -> groupUIDs[e] }.toSet(),
            parentGroups = memberGroups.toSet(),
            ownedGroups = ownedGroups.toSet()
        )
    }

    override fun getGroupByName(groupname: String): Group? {
        return getGroups().firstOrNull() { it.name == groupname }
    }

    override fun getGroups(): Collection<Group> = getUsersAndGroups().second

    override fun createGroup(group: Group): Group {
        val personFilter =
            (group.userMembers + group.owners).joinToString(
                separator = ")(${ldap.uuidType}=",
                prefix = "(${ldap.uuidType}=",
                postfix = ")"
            )
        val personUIDs = ldap.searchEntries(
            dn = ldap.baseDnUsers,
            filter = "(&(objectClass=inetOrgPerson)(|$personFilter))"
        ).associate { e -> e.getUUID() to e.dn.toString() }

        val groupFilter =
            (group.groupMembers + group.ownerGroups).joinToString(
                separator = ")(${ldap.uuidType}=",
                prefix = "(${ldap.uuidType}=",
                postfix = ")"
            )
        val groupUIDs = ldap.searchEntries(
            dn = ldap.baseDnGroups,
            filter = "(&(objectClass=$groupObjectClass)(|$groupFilter))"
        ).associate { e -> e.getUUID() to e.dn.toString() }

        val userMembers = (group.userMembers.mapNotNull { personUIDs[it] } + group.owners.mapNotNull { personUIDs[it] } + ldap.dummyAccountDn).toSet()
        val groupMembers = (group.groupMembers.mapNotNull { groupUIDs[it] } + group.ownerGroups.mapNotNull { groupUIDs[it] }).toSet()
        val userOwners = (group.owners.mapNotNull { personUIDs[it] } + ldap.dummyAccountDn).toSet()
        val groupOwners = group.ownerGroups.mapNotNull { groupUIDs[it] }.toSet()
        val created = ldap.create(
            GroupOfNames(
                dn = "${ldap.rdnGroup}=${group.name},${ldap.baseDnGroups}",
                rdn = group.name,
                member = userMembers + groupMembers,
                owner = userOwners + groupOwners,
                description = group.description,
                entryUUID = ""
            ).toLdapEntry(ldap)
        )

        (group.parentGroups - group.ownedGroups).mapNotNull { getGroup(it) }.forEach {
            // add this group as member to that other group
            updateGroup(it.copy(groupMembers = it.groupMembers + created.getUUID()))
        }

        group.ownedGroups.mapNotNull { getGroup(it) }.forEach {
            // add this group as owner to that other group
            updateGroup(it.copy(ownerGroups = it.ownerGroups + created.getUUID()))
        }

        return group.copy(
            id = created.getUUID(),
            userMembers = group.userMembers + group.owners,
            groupMembers = group.groupMembers + group.ownerGroups,
            parentGroups = group.parentGroups + group.ownedGroups
        )
    }

    override fun deleteGroup(group: Group) {
        ldap.getByUUID(group.id, ldap.baseDnGroups)?.apply {
            ldap.delete(this)
        }
    }

    override fun updateGroup(group: Group): Group? {
        // Special case, group (no longer) existing, ignoring update request
        val savedGroup = getGroup(group.id) ?: return null

        // Special case rdn modification
        if (group.name != savedGroup.name) {
            ldap.rename("${ldap.rdnGroup}=${savedGroup.name},${ldap.baseDnGroups}", "${ldap.rdnGroup}=${group.name}")
        }

        val personFilter =
            (group.userMembers + group.owners).joinToString(
                separator = ")(${ldap.uuidType}=",
                prefix = "(${ldap.uuidType}=",
                postfix = ")"
            )
        val personUIDs = ldap.searchEntries(
            dn = ldap.baseDnUsers,
            filter = "(&(objectClass=inetOrgPerson)(|$personFilter))"
        ).associate { e -> e.getUUID() to e.dn.toString() }

        val groupFilter =
            (group.groupMembers + group.ownerGroups).joinToString(
                separator = ")(${ldap.uuidType}=",
                prefix = "(${ldap.uuidType}=",
                postfix = ")"
            )
        val groupUIDs = ldap.searchEntries(
            dn = ldap.baseDnGroups,
            filter = "(&(objectClass=$groupObjectClass)(|$groupFilter))"
        ).associate { e -> e.getUUID() to e.dn.toString() }

        val modifications = mutableListOf<Modification>()
        attributeModificationOperation(SchemaConstants.DESCRIPTION_AT, savedGroup.description, group.description)
            ?.also { modifications.add(it) }

        if (((group.userMembers + group.owners) != savedGroup.userMembers) ||
            ((group.groupMembers + group.ownerGroups) != savedGroup.groupMembers)
        ) {
            val uniqueMembers = (
                group.userMembers.mapNotNull { personUIDs[it] } +
                    group.owners.mapNotNull { personUIDs[it] } +
                    group.groupMembers.mapNotNull { groupUIDs[it] } +
                    group.ownerGroups.mapNotNull { groupUIDs[it] } +
                    ldap.dummyAccountDn
                ).toSet()
            modifications.add(
                DefaultModification(
                    ModificationOperation.REPLACE_ATTRIBUTE,
                    groupMemberAttribute,
                    *uniqueMembers.toTypedArray()
                )
            )
        }

        if ((group.owners != savedGroup.owners) ||
            (group.ownerGroups != savedGroup.ownerGroups)
        ) {
            val uniqueOwners = (
                group.owners.mapNotNull { personUIDs[it] } +
                    group.ownerGroups.mapNotNull { groupUIDs[it] } +
                    ldap.dummyAccountDn
                ).toSet()
            modifications.add(
                DefaultModification(
                    ModificationOperation.REPLACE_ATTRIBUTE,
                    SchemaConstants.OWNER_AT,
                    *uniqueOwners.toTypedArray()
                )
            )
        }

        if (modifications.isNotEmpty()) {
            ldap.modify("${ldap.rdnGroup}=${group.name},${ldap.baseDnGroups}", modifications)
        }

        if (group.parentGroups != savedGroup.parentGroups) {
            group.parentGroups.plus(savedGroup.parentGroups).mapNotNull { getGroup(it) }.forEach {
                if (group.parentGroups.contains(it.id) && !savedGroup.parentGroups.contains(it.id)) {
                    // add this group as member to that other group
                    updateGroup(it.copy(groupMembers = it.groupMembers + group.id))
                } else if (!group.parentGroups.contains(it.id) && savedGroup.parentGroups.contains(it.id)) {
                    // remove this group as member from that other group
                    updateGroup(it.copy(groupMembers = it.groupMembers - group.id))
                }
            }
        }

        if (group.ownedGroups != savedGroup.ownedGroups) {
            group.ownedGroups.plus(savedGroup.ownedGroups).mapNotNull { getGroup(it) }.forEach {
                if (group.ownedGroups.contains(it.id) && !savedGroup.ownedGroups.contains(it.id)) {
                    // add this group as owner to that other group
                    updateGroup(it.copy(ownerGroups = it.ownerGroups + group.id))
                } else if (!group.ownedGroups.contains(it.id) && savedGroup.ownedGroups.contains(it.id)) {
                    // remove this group as owner from that other group
                    updateGroup(it.copy(ownerGroups = it.ownerGroups - group.id))
                }
            }
        }

        return group.copy(
            userMembers = group.userMembers + group.owners,
            groupMembers = group.groupMembers + group.ownerGroups,
            parentGroups = group.parentGroups + group.ownedGroups
        )
    }

    private fun attributeModificationOperation(attribute: String, oldValue: String?, newValue: String?): Modification? =
        when {
            oldValue == newValue -> null
            oldValue == null && newValue != null -> DefaultModification(
                ModificationOperation.ADD_ATTRIBUTE,
                attribute,
                newValue
            )
            oldValue != null && newValue == null -> DefaultModification(
                ModificationOperation.REMOVE_ATTRIBUTE,
                attribute
            )
            else -> DefaultModification(ModificationOperation.REPLACE_ATTRIBUTE, attribute, newValue)
        }

    override fun getUsersAndGroups(): Pair<Collection<User>, Collection<Group>> {
        val ldapUsers = ldap.searchEntries(
            dn = ldap.baseDnUsers,
            scope = SearchScope.ONELEVEL,
            filter = "(objectClass=inetOrgPerson)"
        ).map { it.toEntity<InetOrgPerson>(ldap) }
        val ldapGroups = ldap.searchEntries(
            dn = ldap.baseDnGroups,
            filter = "(objectClass=$groupObjectClass)"
        ).filter { it.dn.toString() != ldap.baseDnGroups }
            .map { it.toEntity<GroupOfNames>(ldap) }
        val ldapPermissions =
            ldap.searchEntries(dn = ldap.baseDnPermissions).filter { it.dn.toString() != ldap.baseDnPermissions }
                .map { it.toEntity<GroupOfNames>(ldap) }

        val cachedUserDNs: Map<String, String> =
            ldapUsers.associate { it.dn to it.entryUUID }
        val cachedGroupDNs: Map<String, String> =
            ldapGroups.associate { it.dn to it.entryUUID }
        /*val cachedPermissionDNs: Map<String, String> =
            ldapPermissions.associate { it.dn to it.entryUUID }*/

        val permissions: Map<Permission, Set<String>> = ldapPermissions.filter {
            enumContains<Permission>(it.rdn.uppercase(Locale.getDefault()))
        }.associate {
            Permission.valueOf(it.rdn.uppercase(Locale.getDefault())) to it.member
        }

        val disabled = ldap.searchEntries(
            dn = ldap.baseDnUsers,
            scope = SearchScope.ONELEVEL,
            filter = "(objectClass=inetOrgPerson)",
            attributeList = arrayOf(ldap.uuidType, "nsAccountLock")
        ).associate {
            it.getUUID() to (it.get("nsAccountLock")?.string == "TRUE")
        }

        val users = ldapUsers.map {
            User(
                id = it.entryUUID,
                username = it.rdn,
                firstname = it.givenName ?: "",
                surname = it.sn,
                mail = it.mail?.firstOrNull() ?: "",
                displayName = it.displayName,
                registeredSince = it.createTimestamp,
                permissions = permissions.filter { permission -> permission.value.contains(it.dn) }.keys,
                groups = ldapGroups.filter { group -> group.member.contains(it.dn) }
                    .map { group -> group.entryUUID }.toSet(),
                ownedGroups = ldapGroups.filter { group -> group.owner.contains(it.dn) }
                    .map { group -> group.entryUUID }.toSet(),
                disabled = disabled[it.entryUUID] ?: false
            )
        }

        val groups = ldapGroups.map {
            Group(
                id = it.entryUUID,
                name = it.rdn,
                description = it.description,
                dnParent = Dn(it.dn).parent.toString()
                    .let { parent -> if (parent == ldap.baseDnGroups) null else cachedGroupDNs[parent] },
                // TODO: allow recursive permissions
                // permissions = permissions.filter { permission -> permission.value.contains(it.dn) }.keys,
                userMembers = it.member.mapNotNull { member -> cachedUserDNs[member] }.toSet(),
                groupMembers = it.member.mapNotNull { member -> cachedGroupDNs[member] }.toSet(),
                owners = it.owner.mapNotNull { owner -> cachedUserDNs[owner] }.toSet(),
                ownerGroups = it.owner.mapNotNull { owner -> cachedGroupDNs[owner] }.toSet(),
                parentGroups = ldapGroups.filter {
                    otherGroup ->
                    otherGroup.member.contains(it.dn)
                }.map { otherGroup -> otherGroup.entryUUID }.toSet(),
                ownedGroups = ldapGroups.filter {
                    otherGroup ->
                    otherGroup.owner.contains(it.dn)
                }.map { otherGroup -> otherGroup.entryUUID }.toSet()
            )
        }

        return users to groups
    }

    override fun close() {
        ldap.close()
    }
}
