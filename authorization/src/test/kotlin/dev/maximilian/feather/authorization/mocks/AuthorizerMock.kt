/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.authorization.mocks

import dev.maximilian.feather.ICredentialAuthorizer
import dev.maximilian.feather.Permission
import dev.maximilian.feather.Session
import dev.maximilian.feather.User
import dev.maximilian.feather.authorization.EMPTY_UUID
import java.time.Instant
import java.util.Random

class AuthorizerMock : ICredentialAuthorizer {
    override val id = "credential"

    override fun authorize(username: String, password: String): Session {
        val randomNumber = Random().nextInt(100000)
        val user = User(
            randomNumber.toString(),
            "user$randomNumber",
            "Vis",
            "HarryFirst$randomNumber",
            "Last$randomNumber",
            "$randomNumber@maximilian.dev",
            setOf("1"),
            Instant.now().minusSeconds(1000),
            setOf(),
            setOf(Permission.ADMIN),
            false
        )
        return Session(
            EMPTY_UUID, Instant.now().plusSeconds(1000), user, this, null, null
        )
    }

    override fun refresh(session: Session): Session = session.copy(validUntil = session.validUntil.plusSeconds(100))

    override fun afterLogoutURL(): String = "/login"
}
