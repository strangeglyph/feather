/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

plugins {
    kotlin("plugin.serialization") version kotlinVersion
}

dependencies {
    // Library
    implementation(project(":lib"))

    // Logging
    implementation("org.slf4j", "slf4j-simple", slf4jVersion)
    // implementation("org.slf4j", "slf4j-api", slf4jVersion)

    // Database ORM Framework
    implementation("org.jetbrains.exposed", "exposed-core", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-dao", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-jdbc", exposedVersion)
    implementation("org.jetbrains.exposed", "exposed-java-time", exposedVersion)

    // Rest framework
    implementation("io.javalin", "javalin", javalinVersion)
    implementation("io.javalin", "javalin-openapi", javalinVersion)

    // Rest Client
    implementation("com.konghq", "unirest-java", unirestVersion)
    implementation("com.konghq", "unirest-objectmapper-jackson", unirestVersion)

    // OIDC driver
    implementation("com.nimbusds", "oauth2-oidc-sdk", "8.1")

    // Redis driver
    implementation("redis.clients", "jedis", "3.2.0")

    // LDAP driver
    implementation("org.apache.directory.api", "api-all", directoryApiVersion)

    // Text template compiler
    implementation("com.github.spullara.mustache.java", "compiler", "0.9.6")

    // Im memory database
    testImplementation("com.h2database", "h2", "1.4.200")
}

kotlin {
}
