/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

const val kotlinVersion = "1.5.31"
const val gitPluginVersion = "0.12.3"

const val microUtilsVersion = "2.0.11"
const val exposedVersion = "0.35.1"
const val postgresVersion = "42.2.24"
const val jacksonVersion = "2.13.0"
const val unirestVersion = "3.13.0"
const val javalinVersion = "3.13.12"
const val directoryApiVersion = "2.1.0"
const val slf4jVersion = "1.7.32"

const val junitVersion = "5.8.1"

const val ktorVersion = "1.6.5"
const val coroutinesVersion = "1.5.2"

const val jsoupVersion = "1.14.3"
