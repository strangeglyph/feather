/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud

import com.github.sardine.SardineFactory
import dev.maximilian.feather.getEnv
import dev.maximilian.feather.nextcloud.ocs.GroupAPI
import dev.maximilian.feather.nextcloud.ocs.NextcloudShare
import dev.maximilian.feather.nextcloud.ocs.ShareAPI
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import dev.maximilian.feather.nextcloud.webdavAPI.WebdavAPI
import dev.maximilian.feather.nextcloud.webdavAPI.webdavIntern.WebdavReal
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.UUID

class ShareAPITest {
    companion object {
        private val shareAPI = ShareAPI(TestUtil.baseUrl, TestUtil.rest)
        private val webdavURL = getEnv(
            "NEXTCLOUD_WEBDAV",
            TestUtil.baseUrl + "/remote.php/dav/files/" + TestUtil.user + "/"
        )

        private val webdavAPI = WebdavAPI(
            webdavURL,
            WebdavReal(SardineFactory.begin(TestUtil.user, TestUtil.password))
        )

        @BeforeAll
        @JvmStatic
        fun setup() {
            val groupAPI = GroupAPI(TestUtil.baseUrl, TestUtil.rest)
            groupAPI.addGroup(TestUtil.groupToShareWith)
        }
    }

    @Test
    fun `Test getAllShares and getSpecificShareEntity`() {
        var shares = shareAPI.getAllShares()

        if (shares.isEmpty()) {
            // test at least once with a share
            createShareForFolder(generateTestShare())

            shares = shareAPI.getAllShares()
        }

        assert(!shares.isEmpty())

        shares.forEach {
            check(it.id != 0)
            check(!it.path.isEmpty())
        }

        val t = shareAPI.getSpecificShareEntity(shares.first().id)
        check(t.id != 0)
        check(!t.path.isEmpty())
    }

    @Test
    fun `Test createDeleteShare`() {
        val shareUrlID = createShareForFolder(generateTestShare())

        checkNotNull(shareUrlID > 0)

        val share = shareAPI.getSpecificShareEntity(shareUrlID)
        checkNotNull(share)

        val shares = shareAPI.getAllShares()
        check(shares.filter { it.id == shareUrlID }.count() == 1)

        shareAPI.deleteShare(shareUrlID)

        assertThrows<java.lang.Exception> {
            shareAPI.getSpecificShareEntity(shareUrlID)
        }

        val shares2 = shareAPI.getAllShares()
        check(shares2.filter { it.id == shareUrlID }.count() == 0)
    }

    @Test
    fun `Test createShareForInvalidFolderFails`() {
        val testShare = generateTestShare()

        val folders = webdavAPI.listFolders("")
        // the folder of the test share should not exist
        check(!folders.contains(testShare.path.substring(1)))

        assertThrows<java.lang.Exception> {
            shareAPI.createShare(testShare)
        }
    }

    @Test
    fun `Test createShareForInvalidUserFails`() {
        // generate share for fake user ID
        val testShare = generateTestShare(
            userOrGroupIDtoShareWith = UUID.randomUUID().toString().replace("-", "")
        )

        // call createFolder without the leading "/"
        webdavAPI.createFolder(testShare.path.substring(1))

        assertThrows<java.lang.Exception> {
            shareAPI.createShare(testShare)
        }
    }

    @Test
    fun `Test updateShare`() {
        val shareUrlID = createShareForFolder(generateTestShare())

        checkNotNull(shareUrlID > 0)

        val share = shareAPI.getSpecificShareEntity(shareUrlID)
        checkNotNull(share)
        check(share.permissions == PermissionType.all.intValue)

        shareAPI.updateShare(shareUrlID, false, PermissionType.read)

        val share2 = shareAPI.getSpecificShareEntity(shareUrlID)
        checkNotNull(share2)
        check(share2.permissions == PermissionType.read.intValue)
    }

    @Test
    fun `Test createPublicShare`() {
        // generate share with public link
        var testShare = generateTestShare(
            type = ShareType.publicLink,
            publicUpload = false,
            permissions = PermissionType.create
        )

        val shareUrlID = createShareForFolder(testShare)
        checkNotNull(shareUrlID > 0)

        val share = shareAPI.getSpecificShareEntity(shareUrlID)
        checkNotNull(share)

        // when creating a 'publicLink', the permissions from the request will be ignored
        assertEquals(17, share.permissions) // or 31 if publicUpload == true
        check(share.can_edit)
        checkNotNull(share.token)
        checkNotNull(share.url)

        shareAPI.updateShare(shareUrlID, false, PermissionType.create)

        // only after the update, the permissions are right
        val share2 = shareAPI.getSpecificShareEntity(shareUrlID)
        assertEquals(ShareType.publicLink, share2.share_type)
        assertEquals(PermissionType.create.intValue, share2.permissions)
    }

    private fun createShareForFolder(share: NextcloudShare): Int {
        // call createFolder without the leading "/"
        webdavAPI.createFolder(share.path.substring(1))
        val shareID = shareAPI.createShare(share).id
        return shareID
    }
}
