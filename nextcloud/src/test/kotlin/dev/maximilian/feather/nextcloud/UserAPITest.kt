/*
 *    Copyright [2020-2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud

import dev.maximilian.feather.nextcloud.ocs.UserAPI
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class UserAPITest() {
    companion object {
        private val userAPI = UserAPI(TestUtil.baseUrl, TestUtil.rest)
    }

    @Test
    fun `Test createUser`() {
        val userData = generateTestUser()
        val result = userAPI.createUser(userData)
        check(result)

        val user = userAPI.getUserInfo(userData.userid)
        checkNotNull(user)
        assertEquals(user.displayname, userData.displayName)
        assertEquals(user.email, userData.email)
    }

    @Test
    fun `Test changeMailAddress`() {
        val userData = generateTestUser()
        val result = userAPI.createUser(userData)
        check(result)

        val newMail = generateMailAddress()
        userAPI.changeMailAddress(userData.userid, newMail)

        val user = userAPI.getUserInfo(userData.userid)
        checkNotNull(user)
        assertEquals(user.displayname, userData.displayName)
        assertEquals(user.email, newMail)
    }

    @Test
    fun `Test getAllUserID`() {
        val userList = userAPI.getAllUsers()
        checkNotNull(userList)
        check(userList.contains(TestUtil.user))
    }

    @Test
    fun `Test getUserInfo Quota correct`() {
        val user = userAPI.getUserInfo(TestUtil.user)
        // for the default admin user
        check(user.quota.quota == -3)
    }

    @Test
    fun `Test getUserInfo email correct`() {
        val user = userAPI.getUserInfo(TestUtil.user)
        // for the default admin user
        check(user.email == null)
    }

    @Test
    fun `Test enableUser and disableUser`() {
        val userData = generateTestUser()
        val result = userAPI.createUser(userData)
        check(result)

        val user = userAPI.getUserInfo(userData.userid)
        checkNotNull(user)
        assert(user.enabled)

        userAPI.disableUser(userData.userid)

        val user2 = userAPI.getUserInfo(userData.userid)
        checkNotNull(user2)
        assert(!user2.enabled)

        userAPI.enableUser(userData.userid)

        val user3 = userAPI.getUserInfo(userData.userid)
        checkNotNull(user3)
        assert(user3.enabled)
    }
}
