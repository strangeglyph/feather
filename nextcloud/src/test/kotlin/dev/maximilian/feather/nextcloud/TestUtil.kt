/*
 * Copyright [2021] Feather development team, see AUTHORS.md
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.maximilian.feather.nextcloud

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import dev.maximilian.feather.getEnv
import dev.maximilian.feather.nextcloud.ocs.NextcloudShare
import dev.maximilian.feather.nextcloud.ocs.NextcloudUser
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import kong.unirest.Config
import kong.unirest.UnirestInstance
import kong.unirest.jackson.JacksonObjectMapper
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.Locale
import java.util.UUID

object TestUtil {
    val baseUrl = getEnv("NEXTCLOUD_BASE_URL", "http://127.0.0.1:8082")
    val user = getEnv("NEXTCLOUD_USER", "ncadmin")
    val password = getEnv("NEXTCLOUD_PWD", "ncadminsecret")
    val rest = UnirestInstance(
        Config().setObjectMapper(
            JacksonObjectMapper(
                jacksonObjectMapper()
                    .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                    .configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
            )
        )
            .addDefaultHeader("OCS-APIRequest", "true")
            .addDefaultHeader("Accept", "application/json")
            .setDefaultBasicAuth(
                user,
                password
            )
    )

    const val groupToShareWith: String = "testgroup"

    val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("YYYY-MM-dd")
        .withLocale(Locale.US)
        .withZone(ZoneId.systemDefault())
}

internal fun generateMailAddress() = "test+${UUID.randomUUID()}@maximilian.dev"

internal fun generateTestUser(
    displayName: String = UUID.randomUUID().toString().replace("-", "").substring(4),
    email: String = generateMailAddress(),
    userid: String = displayName.lowercase(Locale.getDefault()),
    password: String = UUID.randomUUID().toString().replace("-", ""),
    groups: List<String> = emptyList<String>(),
    subadmin: List<String> = emptyList<String>(),
    quota: String = "-3",
    language: String = ""
) = NextcloudUser(
    displayName, email, userid, password,
    groups, subadmin, quota, language
)

internal fun generateTestPath(parentPath: String = "/"): Pair<String, String> {
    val subpath = UUID.randomUUID().toString()
    val fullpath = if (parentPath.endsWith("/")) {
        parentPath + subpath
    } else {
        parentPath + "/" + subpath
    }
    return Pair<String, String>(subpath, fullpath)
}

internal fun generateTestShare(
    path: String = "/" + UUID.randomUUID().toString(),
    type: ShareType = ShareType.group,
    userOrGroupIDtoShareWith: String = TestUtil.groupToShareWith,
    publicUpload: Boolean = false,
    permissions: PermissionType = PermissionType.all,
    expireDate: Instant = Instant.now().plus(3, ChronoUnit.DAYS)
) = NextcloudShare(
    path, type, userOrGroupIDtoShareWith,
    publicUpload, permissions, TestUtil.dateTimeFormatter.format(expireDate)
)
