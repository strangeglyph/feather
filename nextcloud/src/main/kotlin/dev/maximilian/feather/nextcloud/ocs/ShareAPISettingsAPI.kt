/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs

import dev.maximilian.feather.asObject
import dev.maximilian.feather.nextcloud.ocs.entities.general.ocs
import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.MetaEntity
import kong.unirest.UnirestInstance

internal data class StringResponseData(
    val data: String
)

internal data class EnforceExpireDateResponseEntity(
    val meta: MetaEntity,
    val data: StringResponseData
)

internal data class NumberResponseData(
    val data: Number
)

internal data class ExpireAfterNDaysResponseEntity(
    val meta: MetaEntity,
    val data: NumberResponseData
)

internal data class SetCoreSettingsResponseEntity(
    val meta: MetaEntity,
    val data: List<String> // will always be an empty list
)

internal class ShareAPISettingsAPI(baseURL: String, private val restAPI: UnirestInstance) : IShareAPISettingsAPI {
    private val basePath = "ocs/v2.php/apps/provisioning_api/api/v1"
    private val coreSettingsPath = "$baseURL/$basePath/config/apps/core"

    // default expire date

    override fun getDefaultExpireDate(): Boolean {
        val reply = restAPI.get("$coreSettingsPath/shareapi_default_expire_date")
            .asObject<ocs<EnforceExpireDateResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::getDefaultExpireDate Response Body is null")

        return reply.body.ocs.data.data == "yes"
    }

    override fun setDefaultExpireDate(
        defaultExpireDate: Boolean
    ): Boolean {
        val body: String = if (defaultExpireDate) "value=yes" else "value=no"
        val reply = restAPI.post("$coreSettingsPath/shareapi_default_expire_date")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .asObject<ocs<SetCoreSettingsResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::setDefaultExpireDate Response Body is null")

        return reply.body.ocs.meta.statuscode == 200
    }

    // default internal expire date

    override fun getDefaultInternalExpireDate(): Boolean {
        val reply = restAPI.get("$coreSettingsPath/shareapi_default_internal_expire_date")
            .asObject<ocs<EnforceExpireDateResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::getDefaultInternalExpireDate Response Body is null")

        return reply.body.ocs.data.data == "yes"
    }

    override fun setDefaultInternalExpireDate(
        defaultExpireDate: Boolean
    ): Boolean {
        val body: String = if (defaultExpireDate) "value=yes" else "value=no"
        val reply = restAPI.post("$coreSettingsPath/shareapi_default_internal_expire_date")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .asObject<ocs<SetCoreSettingsResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::setDefaultInternalExpireDate Response Body is null")

        return reply.body.ocs.meta.statuscode == 200
    }

    // expire after n days

    override fun getExpireAfterNDays(): Number {
        val reply = restAPI.get("$coreSettingsPath/shareapi_expire_after_n_days")
            .asObject<ocs<ExpireAfterNDaysResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::getDefaultExpireDate Response Body is null")

        return reply.body.ocs.data.data
    }

    override fun setExpireAfterNDays(
        expireAfterNDays: Number
    ): Boolean {
        val body = "value=$expireAfterNDays"
        val reply = restAPI.post("$coreSettingsPath/shareapi_expire_after_n_days")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .asObject<ocs<SetCoreSettingsResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::setExpireAfterNDays Response Body is null")

        return reply.body.ocs.meta.statuscode == 200
    }

    // internal expire after n days

    override fun getInternalExpireAfterNDays(): Number {
        val reply = restAPI.get("$coreSettingsPath/shareapi_internal_expire_after_n_days")
            .asObject<ocs<ExpireAfterNDaysResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::getInternalExpireAfterNDays Response Body is null")

        return reply.body.ocs.data.data
    }

    override fun setInternalExpireAfterNDays(
        expireAfterNDays: Number
    ): Boolean {
        val body = "value=$expireAfterNDays"
        val reply = restAPI.post("$coreSettingsPath/shareapi_internal_expire_after_n_days")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .asObject<ocs<SetCoreSettingsResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::setInternalExpireAfterNDays Response Body is null")

        return reply.body.ocs.meta.statuscode == 200
    }

    // enforce expire date

    override fun getEnforceExpireDate(): Boolean {
        val reply = restAPI.get("$coreSettingsPath/shareapi_enforce_expire_date")
            .asObject<ocs<EnforceExpireDateResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::getEnforceExpireDate Response Body is null")

        return reply.body.ocs.data.data == "yes"
    }

    override fun setEnforceExpireDate(
        enforceExpireDate: Boolean
    ): Boolean {
        val body: String = if (enforceExpireDate) "value=yes" else "value=no"
        val reply = restAPI.post("$coreSettingsPath/shareapi_enforce_expire_date")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .asObject<ocs<SetCoreSettingsResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::setEnforceExpireDate Response Body is null")

        return reply.body.ocs.meta.statuscode == 200
    }

    // enforce internal expire date

    override fun getEnforceInternalExpireDate(): Boolean {
        val reply = restAPI.get("$coreSettingsPath/shareapi_enforce_internal_expire_date")
            .asObject<ocs<EnforceExpireDateResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::getEnforceInternalExpireDate Response Body is null")

        return reply.body.ocs.data.data == "yes"
    }

    override fun setEnforceInternalExpireDate(
        enforceExpireDate: Boolean
    ): Boolean {
        val body: String = if (enforceExpireDate) "value=yes" else "value=no"
        val reply = restAPI.post("$coreSettingsPath/shareapi_enforce_internal_expire_date")
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body(body)
            .asObject<ocs<SetCoreSettingsResponseEntity>>()

        reply.throwOnErrorForShare()
            ?: throw IllegalStateException("::setEnforceInternalExpireDate Response Body is null")

        return reply.body.ocs.meta.statuscode == 200
    }
}
