/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud

import dev.maximilian.feather.nextcloud.groupfolders.GroupFoldersAPI
import dev.maximilian.feather.nextcloud.groupfolders.GroupFoldersGroupsAPI
import dev.maximilian.feather.nextcloud.groupfolders.entities.GroupFolderDetails
import dev.maximilian.feather.nextcloud.ocs.GroupAPI
import dev.maximilian.feather.nextcloud.ocs.ShareAPI
import dev.maximilian.feather.nextcloud.ocs.ShareAPISettingsAPI
import dev.maximilian.feather.nextcloud.ocs.UserAPI
import dev.maximilian.feather.nextcloud.ocs.entities.general.ShareDetailEntity
import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.NextcloudUserDataEntity
import dev.maximilian.feather.nextcloud.ocs.general.PermissionType
import dev.maximilian.feather.nextcloud.ocs.general.ShareType
import dev.maximilian.feather.nextcloud.webdavAPI.WebdavAPI
import dev.maximilian.feather.nextcloud.webdavAPI.webdavIntern.IWebdav
import kong.unirest.UnirestInstance
import mu.KLogging

public interface INextcloud {
    public fun listFolders(path: String): List<String>
    public fun listFiles(path: String): List<String>
    public fun createFolder(path: String)
    public fun copy(source: String, destination: String)
    public fun delete(path: String)
    public fun createFile(path: String, content: String)
    public fun createShare(
        path: String,
        type: ShareType,
        userOrGroupIDtoShareWith: String,
        publicUpload: Boolean,
        permissions: PermissionType,
        expireDate: String? = null
    ): Int
    public fun deleteShare(id: Int)

    public fun getAllShares(): List<ShareDetailEntity>

    public fun getSpecificShare(id: Int): ShareDetailEntity
    public fun getAllUser(): List<String>
    public fun getUserInfo(id: String): NextcloudUserDataEntity

    public fun getAllGroups(): List<String>
    public fun changeMailAddress(userID: String, newMailAddress: String)
    public fun enableUser(userID: String)
    public fun disableUser(userID: String)
    public fun deleteUser(userID: String)
    public fun listAllGroupFolders(): Map<String, GroupFolderDetails>
    public fun findGroupFolder(mountpoint: String): GroupFolderDetails?
    public fun createGroupFolder(
        mountpoint: String,
        quota: Int,
        group: String,
        permissionType: PermissionType = PermissionType.read
    )
    public fun addGroupToGroupFolder(
        groupfolder: GroupFolderDetails,
        group: String,
        permissionType: PermissionType
    )
    public fun removeGroupFromGroupFolder(
        groupfolder: GroupFolderDetails,
        group: String
    )
    public fun removeGroupFolder(groupfolder: GroupFolderDetails)
}

public class Nextcloud(
    webdavURL: String,
    webdavInterface: IWebdav,
    private val restAPI: UnirestInstance,
    private val restURL: String
) : INextcloud {
    private companion object : KLogging()

    private val webdavAPI = WebdavAPI(webdavURL, webdavInterface)
    private val shareAPI = ShareAPI(restURL, restAPI)
    private val shareAPISettingsAPI = ShareAPISettingsAPI(restURL, restAPI)
    private val userAPI = UserAPI(restURL, restAPI)
    private val groupAPI = GroupAPI(restURL, restAPI)
    private val groupFoldersAPI = GroupFoldersAPI(restURL, restAPI)

    public override fun listFolders(path: String): List<String> {
        logger.debug { "::listFolders of $path" }
        return webdavAPI.listFolders(path).also {
            logger.debug { "::listFolders ${it.size} folders found" }
        }
    }

    public override fun listFiles(path: String): List<String> {
        logger.debug { "::listFiles of $path" }
        return webdavAPI.listFiles(path).also {
            logger.debug { "::listFiles ${it.size} files found" }
        }
    }

    public override fun createFolder(path: String) {
        logger.debug { "::createFolder of $path" }
        webdavAPI.createFolder(path)
        logger.debug { "::createFolder done" }
    }

    public override fun copy(source: String, destination: String) {
        logger.debug { "::copy of $source to $destination" }
        webdavAPI.copy(source, destination)
        logger.debug { "::copy done" }
    }

    public override fun delete(path: String) {
        logger.debug { "::delete of $path" }
        webdavAPI.delete(path)
        logger.debug { "::delete done" }
    }

    public override fun createFile(path: String, content: String) {
        logger.debug { "::createFile $path" }
        webdavAPI.createFile(path, content)
        logger.debug { "::createFile finished" }
    }

    public override fun createShare(
        path: String,
        type: ShareType,
        userOrGroupIDtoShareWith: String,
        publicUpload: Boolean,
        permissions: PermissionType,
        expireDate: String?
    ): Int {
        logger.debug { "::createShare for $path" }
        val result = shareAPI.createShare(
            path,
            type,
            userOrGroupIDtoShareWith,
            publicUpload,
            permissions
        )
        logger.debug { "::createShare done, share id : ${result.id}" }
        if (type == ShareType.publicLink || expireDate != null) {
            /* Expirate date: if not null (i.e. use default value from Nextcloud settings),
             * enforce the expiration date with an update. */
            // public links: we need to fix the permissions with an update
            val enforcingExpiration = (
                shareAPISettingsAPI.getDefaultExpireDate() &&
                    shareAPISettingsAPI.getEnforceExpireDate()
                )
            var undoEnforcementDeactivation = false
            if (expireDate != null && expireDate == "" && enforcingExpiration) {
                // deactivate enforcement
                shareAPISettingsAPI.setEnforceExpireDate(false)
                undoEnforcementDeactivation = true
            }
            shareAPI.updateShare(result.id, publicUpload, permissions, expireDate)
            if (undoEnforcementDeactivation) {
                // activate enforcement
                shareAPISettingsAPI.setEnforceExpireDate(true)
            }
        }
        return result.id
    }

    public override fun deleteShare(id: Int) {
        logger.debug { "::deleteShare with ID: $id" }
        return shareAPI.deleteShare(id).also {
            logger.debug { "::deleteShare done" }
        }
    }

    public override fun getAllShares(): List<ShareDetailEntity> {
        logger.debug { "::getAllShares" }
        return shareAPI.getAllShares().also {
            logger.debug { "::getAllShares done: ${it.count()} found" }
        }
    }

    public override fun getSpecificShare(id: Int): ShareDetailEntity {
        logger.debug { "::getSpecificShare with ID: $id" }
        return shareAPI.getSpecificShareEntity(id).also {
            logger.debug { "::getSpecificShare done" }
        }
    }

    public override fun getAllUser(): List<String> {
        logger.debug { "::getAllUser" }
        return userAPI.getAllUsers().also {
            logger.debug { "::getAllUser done: ${it.count()} found" }
        }
    }

    public override fun getUserInfo(id: String): NextcloudUserDataEntity {
        logger.debug { "::getUserInfo with ID: $id" }
        return userAPI.getUserInfo(id).also {
            logger.debug { "::getUserInfo done" }
        }
    }

    public override fun getAllGroups(): List<String> {
        logger.debug { "::getAllGroups" }
        return groupAPI.getAllGroups().also {
            logger.debug { "::getAllGroups done" }
        }
    }

    public override fun changeMailAddress(userID: String, newMailAddress: String) {
        logger.debug { "::changeMailAddress" }
        return userAPI.changeMailAddress(userID, newMailAddress).also {
            logger.debug { "::changeMailAddress done" }
        }
    }

    public override fun enableUser(userID: String) {
        logger.debug { "::enableUser" }
        return userAPI.enableUser(userID).also {
            logger.debug { "::enableUser done" }
        }
    }

    public override fun disableUser(userID: String) {
        logger.debug { "::disableUser" }
        return userAPI.disableUser(userID).also {
            logger.debug { "::disableUser done" }
        }
    }

    public override fun deleteUser(userID: String) {
        logger.debug { "::deleteUser" }
        return userAPI.deleteUser(userID).also {
            logger.debug { "::deleteUse done" }
        }
    }

    public override fun listAllGroupFolders(): Map<String, GroupFolderDetails> {
        logger.debug { "::listAllGroupFolders" }
        return groupFoldersAPI.getAllGroupFolders().also {
            logger.debug { "::listAllGroupFolders ${it.size} folders found" }
        }
    }

    public override fun findGroupFolder(mountpoint: String): GroupFolderDetails? {
        logger.debug { "::findGroupFolder" }
        val folder = groupFoldersAPI.getAllGroupFolders()
            .filterValues { it.mount_point == mountpoint }
            .values.firstOrNull()
        return folder?.also {
            logger.debug { "::findGroupFolder folder $mountpoint found" }
        }
    }

    public override fun createGroupFolder(
        mountpoint: String,
        quota: Int,
        group: String,
        permissionType: PermissionType
    ) {
        logger.debug { "::createGroupFolder" }
        val newId = groupFoldersAPI.createGroupFolder(mountpoint)
        groupFoldersAPI.changeQuota(newId, quota.toString())
        val groupFoldersGroupsAPI = GroupFoldersGroupsAPI(restURL, newId, restAPI)
        groupFoldersGroupsAPI.addGroup(group)
        if (permissionType != PermissionType.all) {
            groupFoldersGroupsAPI.changePermissions(group, permissionType.intValue)
        }
        logger.debug { "::createGroupFolder created folder $mountpoint for $group with size $quota" }
    }

    public override fun addGroupToGroupFolder(
        groupfolder: GroupFolderDetails,
        group: String,
        permissionType: PermissionType
    ) {
        logger.debug { "::addGroupToGroupFolder" }
        val groupFolderDetails = groupFoldersAPI.getGroupFolder(groupfolder.id)
        val groupFoldersGroupsAPI = GroupFoldersGroupsAPI(restURL, groupfolder.id, restAPI)
        if (groupFolderDetails.groups == null || !groupFolderDetails.groups.containsKey(group)) {
            groupFoldersGroupsAPI.addGroup(group)
            if (permissionType != PermissionType.all) {
                groupFoldersGroupsAPI.changePermissions(group, permissionType.intValue)
            }
        } else if (groupFolderDetails.groups.get(group)!! != permissionType.intValue) {
            groupFoldersGroupsAPI.changePermissions(group, permissionType.intValue)
        }
        logger.debug { "::addGroupToGroupFolder added $group to folder ${groupfolder.mount_point}" }
    }

    public override fun removeGroupFromGroupFolder(
        groupfolder: GroupFolderDetails,
        group: String
    ) {
        logger.debug { "::removeGroupFromGroupFolder" }
        val groupFoldersGroupsAPI = GroupFoldersGroupsAPI(restURL, groupfolder.id, restAPI)
        groupFoldersGroupsAPI.deleteGroup(group)
        logger.debug { "::removeGroupFromGroupFolder removed $group from folder ${groupfolder.mount_point}" }
    }

    public override fun removeGroupFolder(groupfolder: GroupFolderDetails) {
        logger.debug { "::removeGroupFolder" }
        groupFoldersAPI.deleteGroupFolder(groupfolder.id)
        logger.debug { "::removeGroupFolder remvoed folder ${groupfolder.mount_point} entirely" }
    }
}
