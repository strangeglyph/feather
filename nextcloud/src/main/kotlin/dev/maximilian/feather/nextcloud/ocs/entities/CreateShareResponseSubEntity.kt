/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.nextcloud.ocs.entities

import dev.maximilian.feather.nextcloud.ocs.general.ShareType

internal data class CreateShareResponseSubEntityRaw(
    val id: Int,
    val share_type: Int,
    val displayname_file_owner: String?,
    val displayname_owner: String?
)

public data class CreateShareResponseSubEntity(
    public val id: Int,
    public val share_type: ShareType,
    public val displayname_file_owner: String?,
    public val displayname_owner: String?
) {
    internal companion object {
        fun fromRaw(value: CreateShareResponseSubEntityRaw) = CreateShareResponseSubEntity(
            value.id, ShareType.findByValue(value.share_type)!!,
            value.displayname_file_owner, value.displayname_owner
        )
    }
}
