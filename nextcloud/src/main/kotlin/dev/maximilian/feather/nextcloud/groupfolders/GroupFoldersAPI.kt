/*
 *
 *  *    Copyright [2021] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.groupfolders

import dev.maximilian.feather.asObject
import dev.maximilian.feather.nextcloud.groupfolders.entities.GroupFolderDetails
import dev.maximilian.feather.nextcloud.ocs.entities.general.ocs
import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.MetaEntity
import kong.unirest.HttpResponse
import kong.unirest.UnirestInstance
import java.net.HttpURLConnection

internal data class NewGroupFolderRequest(
    val mountpoint: String
)

internal data class NewGroupFolderId(
    val id: Int
)

internal data class NewGroupFolderResponseEntity(
    val meta: MetaEntity,
    val data: NewGroupFolderId
)

internal data class GetAllGroupFoldersResponseEntity(
    val meta: MetaEntity,
    val data: Map<String, GroupFolderDetails>?
)

internal data class GetGroupFoldersResponseEntity(
    val meta: MetaEntity,
    val data: GroupFolderDetails
)

internal data class ChangeQuotaRequest(
    val quota: String
)

internal data class SuccessResponse(
    val success: Boolean
)

internal data class ChangeQuotaResponseEntity(
    val meta: MetaEntity,
    val data: SuccessResponse
)

internal data class ChangeMointPointRequest(
    val mountpoint: String
)

internal data class ChangeMointPointResponseEntity(
    val meta: MetaEntity,
    val data: SuccessResponse
)

internal data class ChangeAclFlagRequest(
    val acl: Boolean
)

internal data class ChangeAclFlagResponseEntity(
    val meta: MetaEntity,
    val data: SuccessResponse
)

internal data class ChangeAclManagerRequest(
    val mappingType: String,
    val mappingId: String,
    val manageAcl: Boolean
)

internal data class ChangeAclManagerResponseEntity(
    val meta: MetaEntity,
    val data: SuccessResponse
)

internal class GroupFoldersAPI(baseURL: String, private val restAPI: UnirestInstance) : IGroupFoldersAPI {
    private val basePath = "apps/groupfolders"
    private val groupfoldersPath = "$baseURL/$basePath/folders"

    override fun getAllGroupFolders(): Map<String, GroupFolderDetails> {
        val reply = restAPI.get(groupfoldersPath)
            .asObject<ocs<GetAllGroupFoldersResponseEntity>>()

        reply.throwOnErrorForGroupFolders("getAll")
            ?: throw IllegalStateException("::getAll Response Body is null")

        return reply.body.ocs.data ?: emptyMap()
    }

    override fun getGroupFolder(id: Int): GroupFolderDetails {
        val reply = restAPI.get("$groupfoldersPath/$id")
            .asObject<ocs<GetGroupFoldersResponseEntity>>()

        reply.throwOnErrorForGroupFolders("get")
            ?: throw IllegalStateException("::get Response Body is null")

        return reply.body.ocs.data
    }

    override fun createGroupFolder(mountpoint: String): Int {
        val newGroupFolderRequest = NewGroupFolderRequest(mountpoint)
        val reply = restAPI.post(groupfoldersPath)
            .header("Content-Type", "application/json")
            .body(newGroupFolderRequest)
            .asObject<ocs<NewGroupFolderResponseEntity>>()

        reply.throwOnErrorForGroupFolders("create")
            ?: throw IllegalStateException("::create Response Body is null")

        return reply.body.ocs.data.id
    }

    // newQuota is as string encoding the number of bytes or "-3" for no quota
    override fun changeQuota(id: Int, newQuota: String): Boolean {
        val changeQuotaRequest = ChangeQuotaRequest(newQuota)
        val reply = restAPI.post("$groupfoldersPath/$id/quota")
            .header("Content-Type", "application/json")
            .body(changeQuotaRequest)
            .asObject<ocs<ChangeQuotaResponseEntity>>()

        reply.throwOnErrorForGroupFolders("changeQuotaRequest")
            ?: throw IllegalStateException("::changeQuotaRequest Response Body is null")

        return reply.body.ocs.data.success
    }

    override fun changeMointpoint(id: Int, newMountpoint: String): Boolean {
        val changeMointPointRequest = ChangeMointPointRequest(newMountpoint)
        val reply = restAPI.post("$groupfoldersPath/$id/mountpoint")
            .header("Content-Type", "application/json")
            .body(changeMointPointRequest)
            .asObject<ocs<ChangeMointPointResponseEntity>>()

        reply.throwOnErrorForGroupFolders("changeMointpoint")
            ?: throw IllegalStateException("::changeMointpoint Response Body is null")

        return reply.body.ocs.data.success
    }

    override fun changeAclFlag(id: Int, aclFlag: Boolean): Boolean {
        val changeAclFlagRequest = ChangeAclFlagRequest(aclFlag)
        val reply = restAPI.post("$groupfoldersPath/$id/acl")
            .header("Content-Type", "application/json")
            .body(changeAclFlagRequest)
            .asObject<ocs<ChangeAclFlagResponseEntity>>()

        reply.throwOnErrorForGroupFolders("changeAclFlag")
            ?: throw IllegalStateException("::changeAclFlag Response Body is null")

        return reply.body.ocs.data.success
    }

    // mappingType is either 'user' or 'group' and mappingId the id of the user or group
    override fun changeAclManager(
        id: Int,
        mappingType: String,
        mappingId: String,
        manageAcl: Boolean
    ): Boolean {
        val changeAclManagerRequest = ChangeAclManagerRequest(mappingType, mappingId, manageAcl)
        val reply = restAPI.post("$groupfoldersPath/$id/manageACL")
            .header("Content-Type", "application/json")
            .body(changeAclManagerRequest)
            .asObject<ocs<ChangeAclManagerResponseEntity>>()

        reply.throwOnErrorForGroupFolders("changeAclManager")
            ?: throw IllegalStateException("::changeAclManager Response Body is null")

        return reply.body.ocs.data.success
    }

    override fun deleteGroupFolder(id: Int) {
        val reply = restAPI.delete("$groupfoldersPath/$id")
            .asEmpty()

        reply.throwOnErrorForGroupFolders("delete")
    }
}

internal fun <T> HttpResponse<T>.throwOnErrorForGroupFolders(cmd: String): T? {
    when (status) {
        HttpURLConnection.HTTP_OK -> {
        }
        HttpURLConnection.HTTP_CREATED -> {
        }
        HttpURLConnection.HTTP_ACCEPTED -> {
        }
        HttpURLConnection.HTTP_NOT_ACCEPTABLE -> throw IllegalStateException("::$cmd groups not accepted. ")

        HttpURLConnection.HTTP_UNAUTHORIZED -> throw IllegalStateException("::$cmd Unauthorized to read groups. Bad credentials?")
        HttpURLConnection.HTTP_FORBIDDEN -> throw IllegalStateException("::$cmd Insufficient rights to $cmd groups")
        HttpURLConnection.HTTP_NOT_FOUND -> throw IllegalStateException("::$cmd groups does not exist!")
        HttpURLConnection.HTTP_BAD_REQUEST -> throw IllegalStateException("::$cmd Invalid JSON content for groups")
        // No constant in HttpUrlConnection for this :(
        422 -> throw IllegalArgumentException("::$cmd Requirements not met. $body")
        else -> throw IllegalStateException("::$cmd Unexpected return code $status for groups")
    }
    parsingError.ifPresent {
        throw IllegalStateException(
            "::$cmd Could not parse answer from Nextcloud",
            it
        )
    }
    return body
}
