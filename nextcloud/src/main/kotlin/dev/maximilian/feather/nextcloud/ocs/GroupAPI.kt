/*
 *
 *  *    Copyright [2020] Feather development team, see AUTHORS.md
 *  *
 *  *    Licensed under the Apache License, Version 2.0 (the "License");
 *  *    you may not use this file except in compliance with the License.
 *  *    You may obtain a copy of the License at
 *  *
 *  *        http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  *    Unless required by applicable law or agreed to in writing, software
 *  *    distributed under the License is distributed on an "AS IS" BASIS,
 *  *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  *    See the License for the specific language governing permissions and
 *  *    limitations under the License.
 *
 */

package dev.maximilian.feather.nextcloud.ocs

import dev.maximilian.feather.asObject
import dev.maximilian.feather.nextcloud.ocs.entities.GetAllGroupsResponseEntity
import dev.maximilian.feather.nextcloud.ocs.entities.general.ocs
import dev.maximilian.feather.nextcloud.ocs.entities.get.user.subentities.MetaEntity
import kong.unirest.HttpResponse
import kong.unirest.UnirestInstance
import java.net.HttpURLConnection

internal data class AddGroupsResponseEntity(
    val meta: MetaEntity,
    val data: List<String> // will always be an empty array
)

internal class GroupAPI(baseURL: String, private val restAPI: UnirestInstance) : IGroupAPI {
    private val basePath = "ocs/v1.php/cloud"
    private val groupsPath = "$baseURL/$basePath/groups"

    override fun addGroup(groupid: String): Boolean {
        // val createGroupEntity = CreateGroupEntity(groupid)
        val reply = restAPI.post(groupsPath)
            /*.header("Content-Type", "application/json")
            .body(createGroupEntity)*/
            .header("Content-Type", "application/x-www-form-urlencoded")
            .body("groupid=$groupid")
            .asObject<ocs<AddGroupsResponseEntity>>()

        /* 100 - successful
         * 101 - invalid input data
         * 102 - group already exists
         * 103 - failed to add the group */

        reply.throwOnErrorForGroups("post")
            ?: throw IllegalStateException("::addGroup Response Body is null")

        return reply.body.ocs.meta.statuscode == 100 ||
            reply.body.ocs.meta.statuscode == 102
    }

    override fun getAllGroups(): List<String> {
        val reply = restAPI.get(groupsPath)
            .asObject<ocs<GetAllGroupsResponseEntity>>()

        reply.throwOnErrorForGroups("get")
            ?: throw IllegalStateException("::getAllGroups Response Body is null")

        return reply.body.ocs.data.groups
    }
}

internal fun <T> HttpResponse<T>.throwOnErrorForGroups(cmd: String): T? {
    when (status) {
        HttpURLConnection.HTTP_OK -> {
        }
        HttpURLConnection.HTTP_CREATED -> {
        }
        HttpURLConnection.HTTP_ACCEPTED -> {
        }
        HttpURLConnection.HTTP_NOT_ACCEPTABLE -> throw IllegalStateException("::$cmd groups not accepted. ")

        HttpURLConnection.HTTP_UNAUTHORIZED -> throw IllegalStateException("::$cmd Unauthorized to read groups. Bad credentials?")
        HttpURLConnection.HTTP_FORBIDDEN -> throw IllegalStateException("::$cmd Insufficient rights to $cmd groups")
        HttpURLConnection.HTTP_NOT_FOUND -> throw IllegalStateException("::$cmd groups does not exist!")
        HttpURLConnection.HTTP_BAD_REQUEST -> throw IllegalStateException("::$cmd Invalid JSON content for groups")
        // No constant in HttpUrlConnection for this :(
        422 -> throw IllegalArgumentException("::$cmd Requirements not met. $body")
        else -> throw IllegalStateException("::$cmd Unexpected return code $status for groups")
    }
    parsingError.ifPresent {
        throw IllegalStateException(
            "::$cmd Could not parse answer from Nextcloud",
            it
        )
    }
    return body
}
