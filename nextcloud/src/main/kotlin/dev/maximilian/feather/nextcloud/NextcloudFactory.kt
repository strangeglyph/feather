package dev.maximilian.feather.nextcloud

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.github.sardine.SardineFactory
import dev.maximilian.feather.nextcloud.webdavAPI.webdavIntern.WebdavReal
import kong.unirest.Config
import kong.unirest.UnirestInstance
import kong.unirest.jackson.JacksonObjectMapper
import mu.KLogging

public class NextcloudFactory {
    private companion object : KLogging()

    public fun create(baseUrl: String, username: String, password: String): Nextcloud {
        require(baseUrl.isNotBlank()) { "Nextcloud baseUrl may not be empty" }
        require(username.isNotBlank()) { "Nextcloud user may not be empty" }
        require(password.isNotBlank()) { "Nextcloud password may not be empty" }

        val webdavURL = baseUrl.removeSuffix("/") + "/remote.php/dav/files/$username/"
        val realWebdav = WebdavReal(SardineFactory.begin(username, password))

        logger.info { "Creating Nextcloud WebDav client for user $username (password length: ${password.length})" }

        val restAPI = UnirestInstance(
            Config().setObjectMapper(
                JacksonObjectMapper(
                    jacksonObjectMapper()
                        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
                        .configure(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT, true)
                )
            )
                .addDefaultHeader("OCS-APIRequest", "true")
                .addDefaultHeader("Accept", "application/json")
                .setDefaultBasicAuth(username, password)
        )
        return Nextcloud(webdavURL, realWebdav, restAPI, baseUrl)
    }
}
