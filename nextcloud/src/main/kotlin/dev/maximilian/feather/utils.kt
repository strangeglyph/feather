package dev.maximilian.feather

import kong.unirest.GenericType
import kong.unirest.HttpRequest
import kong.unirest.HttpResponse

internal inline fun <reified B> HttpRequest<*>.asObject(): HttpResponse<B> = this.asObject(object : GenericType<B>() {})

public fun getEnv(name: String, default: String? = null): String =
    System.getenv(name) ?: default ?: throw IllegalArgumentException("Environment variable $name not found")
