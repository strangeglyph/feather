/*
 *    Copyright [2021] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.openshift

import com.shopify.promises.Promise
import dev.maximilian.feather.bindings.openshift.Openshift
import mu.KotlinLogging

public class OpenshiftOpenProjectLDAPSync(private val openshift: Openshift) {
    private val logger = KotlinLogging.logger {}

    public fun synchronizeOpenProjectGroupsWithLDAP(): Promise<CommandOutput, RuntimeException> {
        // NOTE: this will take more than 10 seconds, maybe even up to a minute
        return Promise {
            val innerPromise = try {
                executeShellCommand()
            } catch (e: Exception) {
                try {
                    executeShellCommand()
                } catch (e: Exception) {
                    reject(RuntimeException(e))
                    return@Promise
                }
            }

            onCancel {
                innerPromise.cancel()
            }

            innerPromise.whenComplete { result ->
                when (result) {
                    is Promise.Result.Success -> {
                        logger.debug("Open project synchronisation output: ${result.value.messages}")
                        logger.info("Open project synchronisation success:  ${!result.value.failure}")
                        resolve(result.value)
                    }
                    is Promise.Result.Error -> {
                        logger.info("Open project synchronisation error: ${result.error.message}")
                        reject(RuntimeException(result.error))
                    }
                }
            }
        }
    }

    private fun executeShellCommand(): Promise<CommandOutput, RuntimeException> {
        val commands = arrayOf("bundle", "exec", "rails", "ldap_groups:synchronize")
        return openshift.runCommand("openproject", "community", "community-worker", commands)
    }
}
