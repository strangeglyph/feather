/*
 *    Copyright [2020] Feather development team, see AUTHORS.md
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package dev.maximilian.feather.bindings.openshift

import com.openshift.restclient.ClientBuilder
import com.openshift.restclient.IClient
import com.openshift.restclient.ResourceKind
import com.openshift.restclient.api.capabilities.IPodExec
import com.openshift.restclient.capability.CapabilityVisitor
import com.openshift.restclient.capability.IStoppable
import com.openshift.restclient.model.IDeploymentConfig
import com.openshift.restclient.model.IPod
import com.shopify.promises.Promise
import dev.maximilian.feather.openshift.CommandOutput
import dev.maximilian.feather.openshift.OpenShiftConfig
import mu.KotlinLogging
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean

private data class OpenShiftNamespaceBinding(
    val namespace: String,
    val labels: Map<String, String>
)

private data class OpenShiftDeploymentStatus(
    val deploymentConfig: IDeploymentConfig,
    val pods: List<IPod>
)

private data class OpenShiftNamespaceStatus(
    val namespace: String,
    val deploymentConfigs: List<OpenShiftDeploymentStatus>
)

// For more information, see
// https://github.com/openshift/openshift-restclient-java/
public class Openshift(private val config: OpenShiftConfig) {
    private val logger = KotlinLogging.logger {}

    private var client: IClient? = null

    private val namespaceBindings = mutableMapOf<String, OpenShiftNamespaceBinding>()

    public val enabled: Boolean = config.baseUrl.isNotBlank() && config.username.isNotBlank() && config.password.isNotBlank()

    init {
        if (enabled) {
            val configuredNamespaces = config.propertyMap.getOrPut("openshift.namespaces") { "" }
            logger.info { "Openshift binding enabled for $configuredNamespaces at ${this.config.baseUrl}" }
            configuredNamespaces.split(",").forEach {
                val configuredLabels = config.propertyMap.getOrPut("openshift.$it.labels") { "app=$it" }.split(",")
                val labelsAsMap = configuredLabels.associate { label ->
                    label.split("=").let { (name, value) -> name to value }
                }
                namespaceBindings[it] = OpenShiftNamespaceBinding(it, labelsAsMap)
            }
        } else {
            logger.info { "Openshift binding disabled" }
        }
    }

    private fun getClient(): IClient {
        val curClient = client
        return if (curClient == null) {
            val newClient = ClientBuilder(config.baseUrl)
                .withUserName(config.username)
                .withPassword(config.password)
                .build()
            client = newClient
            newClient
        } else {
            curClient
        }
    }

    private fun getDeploymentConfigList(namespace: String, labels: Map<String, String>):
        List<IDeploymentConfig> {
        logger.info(
            "Retrieving list of deployment configs in namespace $namespace with labels " +
                labels.entries.joinToString(",") { it.key + "=" + it.value }
        )
        val result: List<IDeploymentConfig>
        try {
            result = getClient().list(
                ResourceKind.DEPLOYMENT_CONFIG,
                namespace,
                labels
            )
        } catch (e: Exception) {
            // throw away this failed client (assuming the failure to be critical)
            client = null
            throw e
        }
        return result
    }

    private fun getPODList(namespace: String, labels: Map<String, String>):
        List<IPod> {
        logger.info(
            "Retrieving list of PODS in namespace $namespace with labels " +
                labels.entries.joinToString(",") { it.key + "=" + it.value }
        )
        val result: List<IPod>
        try {
            result = getClient().list(
                ResourceKind.POD,
                namespace,
                labels
            )
        } catch (e: Exception) {
            // throw away this failed client (assuming the failure to be critical)
            client = null
            throw e
        }
        return result
    }

    private fun getDeploymentStatus(namespace: OpenShiftNamespaceBinding): OpenShiftNamespaceStatus {
        val dcs = getDeploymentConfigList(namespace.namespace, namespace.labels)
        return OpenShiftNamespaceStatus(
            namespace.namespace,
            dcs.map {
                val labelsForThisDC = arrayOf(it.name).associateBy { "deploymentconfig" }
                OpenShiftDeploymentStatus(it, getPODList(namespace.namespace, labelsForThisDC))
            }
        )
    }

    internal fun runCommand(
        namespace: String,
        subService: String,
        containerName: String,
        commands: Array<String>,
        timeoutInSeconds: Long = 60
    ): Promise<CommandOutput, RuntimeException> = doIfEnabled {
        if (!namespaceBindings.contains(namespace)) {
            throw IllegalStateException("Invalid namespace $namespace")
        }

        val dc = getDeploymentStatus(namespaceBindings[namespace]!!).deploymentConfigs.firstOrNull {
            it.deploymentConfig.name == subService
        }

        requireNotNull(dc) { "Invalid sub service $subService" }

        val pod = dc.pods.firstOrNull { it.isReady }

        requireNotNull(pod) { "No running PODs for sub service $subService" }

        val container = pod.containers.firstOrNull { it.name == containerName }

        requireNotNull(container) { "Invalid container $containerName" }

        val options = IPodExec.Options()
        options.container(containerName)

        logger.info(
            "Running command " + commands.joinToString(" ") +
                " in container $containerName for service $namespace-$subService"
        )

        Promise {
            val listener = CommandExecListener()

            val visitor = CommandExecCapabilityVisitor(listener, options, commands)

            val stoppable = pod.accept(visitor, null)

            onCancel {
                stoppable.stop()
            }

            try {
                listener.commandDone.await(timeoutInSeconds, TimeUnit.SECONDS)
            } catch (e: Exception) {
                reject(RuntimeException(e))
                return@Promise
            }

            val failure = listener.failureCalled.get() || listener.execErrCalled.get()

            logger.info(
                "Command " + commands.joinToString(" ") + " in container $containerName " +
                    (if (failure) "failed" else "was successful")
            )

            resolve(CommandOutput(failure, listener.messages))
        }
    }

    private fun <T> doIfEnabled(block: () -> T): T {
        if (enabled) return block()
        else throw IllegalStateException("Tried to use openshift binding, but is not enabled")
    }
}

private class CommandExecCapabilityVisitor(
    private val listener: CommandExecListener,
    private val options: IPodExec.Options,
    private val commands: Array<String>
) : CapabilityVisitor<IPodExec, IStoppable>() {

    override fun visit(capability: IPodExec): IStoppable {
        return capability.start(listener, options, *commands)
    }
}

private class CommandExecListener : IPodExec.IPodExecOutputListener {

    private val logger = KotlinLogging.logger {}

    var commandDone = CountDownLatch(1)
    var openCalled = AtomicBoolean(false)
    var closeCalled = AtomicBoolean(false)
    var failureCalled = AtomicBoolean(false)
    var execErrCalled = AtomicBoolean(false)
    val messages = mutableListOf<String>()

    override fun onOpen() {
        openCalled.set(true)
    }

    override fun onStdOut(message: String) {
        message.trim().let {
            if (it.isNotEmpty()) {
                messages.add(it)
            }
        }
    }

    override fun onStdErr(message: String) {
        message.trim().let {
            if (it.isNotEmpty()) {
                logger.warn(it)
            }
        }
    }

    override fun onExecErr(message: String) {
        execErrCalled.set(true)
        message.trim().let {
            if (it.isNotEmpty()) {
                logger.error(it)
            }
        }
    }

    override fun onClose(code: Int, reason: String) {
        closeCalled.set(true)
        commandDone.countDown()
    }

    override fun onFailure(t: Throwable) {
        failureCalled.set(true)
        logger.error("Potentially expected error occurred", t)
        commandDone.countDown()
    }
}
